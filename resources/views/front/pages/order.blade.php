@extends('front.layouts.master')
<?php
use App\helper\Helpers\Helper;
$helpers = new Helper();
?>

@section('title',$helpers->titles()->where('page',1)->first())
@section('content')
	<main class="col px-5 pl-md-2 pt-2 main">
		<!--main-area-->

		<div class="container p-4">
			<div class="container">
				<div class="stepwizard">
					<div class="stepwizard-row">
						<div class="stepwizard-step">
							<button type="button" class="btn btn-primary btn-circle" >1</button>
							<p>Sigin in/Register</p>
						</div>
						<div class="stepwizard-step">
							<button type="button" class="btn btn-default btn-circle" disabled="disabled">2</button>
							<p>Location Confermation</p>
						</div>
						<div class="stepwizard-step">
							<button type="button" class="btn btn-default btn-circle" disabled="disabled">3</button>
							<p>Order Overview</p>
						</div>
						<div class="stepwizard-step">
							<button type="button" class="btn btn-default btn-circle" disabled="disabled">4</button>
							<p>Payment Option</p>
						</div>
						<div class="stepwizard-step">
							<button type="button" class="btn btn-default btn-circle" disabled="disabled">5</button>
							<p>Order Placed</p>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6  text-center offset-md-3">
					<h1 class="p-4">At First You Have to Sign In</h1>
					<div class="row">
						<div class="col-sm-11">
							<a href="#" class="btn btn-block m-2 btn-primary"><span class="text-light"><i class="fab fa-facebook"></i></span>&nbsp; SIgn In With Facebook</a>
						</div>
						<div class="col-sm-11">
							<a href="#" class="btn btn-block m-2 btn-danger"><span class="text-light"><i class="fas fa-envelope"></i></span>&nbsp; SIgn In With Gmail</a>

						</div>

						<div class="col-sm-12">
							<form action="/action_page.php" class=" p-4 needs-validation" novalidate>
								<div class="form-row">
									<div class="col-md-11 m-2">
										<input type="tel" class="form-control" id="mobile" placeholder="Enter Your Phone Number e.g +880123456789" name="mobile" pattern="[0-9]{11}"required>
										<div class="valid-feedback">Valid.</div>
										<div class="invalid-feedback">Please fill out this field.</div>
									</div>
									<div class="col-md-11 m-2">
										<input type="password" class="form-control" id="pwd" placeholder="Enter password" name="pswd" required>
										<div class="valid-feedback">Valid.</div>
										<div class="invalid-feedback">Please fill out this field.</div>
									</div>
								</div>
								<div class="col-md-11 m-2">
									<a type="submit" class="btn btn-lg text-light m-2 btn-primary" href="{{ route
									('login')
									}}">Sign
                                        In</a>
								</div>
								<div class="col-md-11 m-2">
									<a type="submit" class="btn btn-lg text-light m-2 btn-danger" href="{{ route
									('register')

									}}">Register</a>
								</div>


							</form>
						</div>
					</div>
				</div>

			</div>

@stop
