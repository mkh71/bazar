@extends('front.layouts.master')
<?php
use App\helper\Helpers\Helper;
$helpers = new Helper();
?>

@section('title',$helpers->titles()->where('page',1)->first())
@section('content')
	<main class="col px-5 pl-md-2 pt-2 main">
		<!--main-area-->
		<div class="container p-4">
			<div class="container">
				<div class="stepwizard">
					<div class="stepwizard-row">
						<div class="stepwizard-step">
							<button type="button" class="btn btn-danger btn-circle" >1</button>
							<p>Sigin in/Register</p>
						</div>
						<div class="stepwizard-step">
							<button type="button" class="btn btn-danger btn-circle" disabled="disabled">2</button>
							<p>Location Confermation</p>
						</div>
						<div class="stepwizard-step">
							<button type="button" class="btn btn-primary btn-circle" disabled="disabled">3</button>
							<p>Order Overview</p>
						</div>
						<div class="stepwizard-step">
							<button type="button" class="btn btn-default btn-circle" disabled="disabled">4</button>
							<p>Payment Option</p>
						</div>
						<div class="stepwizard-step">
							<button type="button" class="btn btn-default btn-circle" disabled="disabled">5</button>
							<p>Order Placed</p>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6  text-center offset-md-3">
					<h1 class="p-4">Confirm Your Order</h1>
					<div class="row">

						<div class="col-sm-12">
							<form action="/action_page.php" class=" p-4 needs-validation" novalidate>
								<div class="form-row">
									<div class="col-md-11 m-2">
										<table class="show-cart table">

										</table>
										<div class="font-weight-bold">Total price: ৳<span class="total-cart text-danger"></span>/-</div>

									</div>
								</div>
								<div class="col-md-11 m-2">
									<a type="submit" class="btn btn-lg text-light m-2 btn-danger" href="order-4.html">Confirm</a>
								</div>


							</form>
						</div>
					</div>
				</div>

			</div>

@stop