@extends('front.layouts.master')
<?php
use App\helper\Helpers\Helper;
$helpers = new Helper();
?>

@section('title',$helpers->titles()->where('page',1)->first())
@section('content')
			<main class="col px-5 pl-md-2 pt-2 main">
				<!--main-area-->
				<div class="container p-4">
					<div class="container">
						<div class="stepwizard">
							<div class="stepwizard-row">
								<div class="stepwizard-step">
									<button type="button" class="btn btn-danger btn-circle" >1</button>
									<p>Sigin in/Register</p>
								</div>
								<div class="stepwizard-step">
									<button type="button" class="btn btn-danger btn-circle" disabled="disabled">2</button>
									<p>Location Confermation</p>
								</div>
								<div class="stepwizard-step">
									<button type="button" class="btn btn-danger btn-circle" disabled="disabled">3</button>
									<p>Order Overview</p>
								</div>
								<div class="stepwizard-step">
									<button type="button" class="btn btn-primary btn-circle" disabled="disabled">4</button>
									<p>Payment Option</p>
								</div>
								<div class="stepwizard-step">
									<button type="button" class="btn btn-default btn-circle" disabled="disabled">5</button>
									<p>Order Placed</p>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6  text-center offset-md-3">
							<h1 class="p-4">Confirm Your Order</h1>
							<div class="row">

								<div class="col-sm-12">
									<form action="{{ route('cart.complete-order') }}" method="post" class=" p-4
									needs-validation"
                                          novalidate>
                                        @csrf
										<div class="form-row">
											<div class="col-md-11 m-2">
												<div class="form-check">
													<label class="form-check-label form-inline">
														<input type="radio" value="1" checked class="form-check-input"
                                                               name="payment">Cash On Delivery &nbsp;&nbsp; <span><img src="img/png/cash-on-delivery.png" alt=""></span>
													</label>
												</div>
												<div class="form-check">
													<label class="form-check-label form-inline">
														<input type="radio" value="2" class="form-check-input"
                                                               name="payment">Bkash &nbsp;&nbsp; <span><img src="img/png/bkash.png" alt=""></span>
													</label>
												</div>
											</div>
										</div>
										<div class="col-md-11 m-2">
											<input
                                                type="submit"
                                                class="btn btn-lg text-light m-2 btn-danger"
                                                value="Confirm" />
										</div>


									</form>
								</div>
							</div>
						</div>

					</div>
@stop
