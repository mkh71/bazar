@extends('front.layouts.master')
<?php
use App\helper\Helpers\Helper;
$helpers = new Helper();
?>

@section('title',$helpers->titles()->where('page',1)->first())
@section('content')
	<main class="col px-5 pl-md-2 pt-2 main">
		<!--main-area-->
		<div class="container p-4">
			<div class="container">
				<div class="stepwizard">
					<div class="stepwizard-row">
						<div class="stepwizard-step">
							<button type="button" class="btn btn-danger btn-circle" >1</button>
							<p>Sigin in/Register</p>
						</div>
						<div class="stepwizard-step">
							<button type="button" class="btn btn-danger btn-circle" disabled="disabled">2</button>
							<p>Location Confermation</p>
						</div>
						<div class="stepwizard-step">
							<button type="button" class="btn btn-danger btn-circle" disabled="disabled">3</button>
							<p>Order Overview</p>
						</div>
						<div class="stepwizard-step">
							<button type="button" class="btn btn-danger btn-circle" disabled="disabled">4</button>
							<p>Payment Option</p>
						</div>
						<div class="stepwizard-step">
							<button type="button" class="btn btn-success btn-circle" disabled="disabled">5</button>
							<p>Order Placed</p>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6  text-center offset-md-3 p-4">
					<h1 class="p-4">Your Order Has been Confirmed</h1>
					<h4 class="p-4 text-secondery">Your Order your order will be placed within 1 hour</h4>
					<div class="container">
						<span class="check mark bg-danger text-light rounded-circle "><i class="fas fa-check"></i></span>
					</div>
				</div>

			</div>

@stop