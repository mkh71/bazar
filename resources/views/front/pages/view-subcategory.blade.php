@extends('front.layouts.master')
<?php
    use App\helper\Helpers\Helper;
    $helpers = new Helper();
?>
@section('title',$helpers->titles()->where('page',1)->first())
@section('content')
    <main class="col px-5 pl-md-2 pt-2 main">
        <!--main-area-->
        <div class="container-nai">
            <!-- main-section-->
            <div class="main-pading-area">
                <!--crousel area-->
                <div class="1st-offer p-4">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-6"><img class="img-fluid" src="img/ofr-2.jpg" alt=""></div>

                            <div class="col-md-6"><img class="img-fluid" src="img/ofr-1.jpg" alt=""></div>
                        </div>
                    </div>
                </div>
                <!--crousel area-->
                <!--body area-->
                <div class="locator-section">
                    <div class="container">
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item active"><a href="#">{{$category->name}}</a></li>

                        </ul>
                    </div>
                </div>

                <div class="category-wise-select text-center p-4 mx-auto">
                    <h1 class="text-dark p-4 border-bottom">{{$category->name}}</h1>
                    <div class="container">
                        <div class="catagory-wise-area">
                            <div class="row">
                                @foreach($sub_cats as $subcat)
                                    <div class="col-md-4 col-lg-3">
                                        <a href="{{route('visit.product',$category->id)}}">
                                            <div class="card">
                                                <img class="card-img-top img-fluid mx-auto"
                                                     src="{{asset('public/admin/subcategory/'.$subcat->image)}}" alt="">
                                                <div class="card-body">
                                                    <div class="card-body">
                                                        <span class="text-dark font-weight-bold">{{$subcat->name}}</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>

                <!--body area-->
            </div>

@stop