@extends('front.layouts.master')
<?php
use App\helper\Helpers\Helper;
$helpers = new Helper();
?>

@section('title',$helpers->titles()->where('page',1)->first())
@section('content')
	<main class="col px-5 pl-md-2 pt-2 main">
		<!--main-area-->

		<div class="container p-4">
			<div class="container">
				<div class="stepwizard">
					<div class="stepwizard-row">
						<div class="stepwizard-step">
							<button type="button" class="btn btn-danger btn-circle" >1</button>
							<p>Sigin in/Register</p>
						</div>
						<div class="stepwizard-step">
							<button type="button" class="btn btn-primary btn-circle" disabled="disabled">2</button>
							<p>Location Confermation</p>
						</div>
						<div class="stepwizard-step">
							<button type="button" class="btn btn-default btn-circle" disabled="disabled">3</button>
							<p>Order Overview</p>
						</div>
						<div class="stepwizard-step">
							<button type="button" class="btn btn-default btn-circle" disabled="disabled">4</button>
							<p>Payment Option</p>
						</div>
						<div class="stepwizard-step">
							<button type="button" class="btn btn-default btn-circle" disabled="disabled">5</button>
							<p>Order Placed</p>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
						<div class="col-md-6  text-center offset-md-3">
							<h1 class="p-4">You Have to Confirm Your Location</h1>
							<div class="row">

								<div class="col-sm-12">
									<form action="{{ route('cart.orderLocation') }}" method="post" class=" p-4
									needs-validation" novalidate>
                                        @csrf
										<div class="form-row">
											<div class="col-md-11 m-2">
												<label for="mobile" class="font-weight-bold">Your Phone No.</label>
												<input
                                                    type="tel"
                                                    class="form-control"
                                                    id="mobile"
                                                    placeholder="Enter Your Phone Number e.g +880123456789"
                                                    name="mobile"
                                                    pattern="[0-9]{11}"
                                                    required>
												<div class="valid-feedback">Valid.</div>
												<div class="invalid-feedback">Please fill out this field.</div>
											</div>
											<div class="col-md-11 m-2">
												<label for="district" class="font-weight-bold">Select District:</label>
												<select name="district" class="form-control" id="district" required>
													<option>--Select District--</option>
													<option>Chittagong</option>
													<option>Dhaka</option>
												</select>
												<div class="valid-feedback">Valid.</div>
												<div class="invalid-feedback">Please fill out this field.</div>
											</div>
											<div class="col-md-11 m-2">
												<label for="thana" class="font-weight-bold">Select Thana:</label>
												<select class="form-control" name="thana" id="thana" required>
													<option>--Select Thana--</option>
													<option>Chadgaon</option>
													<option>Bakalia</option>
												</select>
												<div class="valid-feedback" class="font-weight-bold">Valid.</div>
												<div class="invalid-feedback">Please fill out this field.</div>
											</div>
											<div class="col-md-11 m-2">
												<label for="adress" class="font-weight-bold">Your Adress</label>
												<textarea
                                                    class="form-control"
                                                    name="address"
                                                    id="address"
                                                    placeholder="Enter address"
                                                    required></textarea>
												<div class="valid-feedback">Valid.</div>
												<div class="invalid-feedback">Please fill out this field.</div>
											</div>
										</div>
										<div class="col-md-11 m-2">
											<input type="submit" class="btn btn-lg text-light m-2 btn-danger"
                                               value="Confirm" />
										</div>


									</form>
								</div>
							</div>
						</div>

					</div>

@stop
