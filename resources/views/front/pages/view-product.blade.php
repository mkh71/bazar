@extends('front.layouts.master')
<?php
	use App\helper\Helpers\Helper;
	$helpers = new Helper();
?>

@section('title',$helpers->titles()->where('page',1)->first())
@section('content')
	<main class="col px-5 pl-md-2 pt-2 main">
		<div class="main-pading-area">
				<!--main-area-->
				<div class="1st-offer img-fluid p-4">
					<div class="container">
						<div class="row">
							<div class="col-md-6 col-xs-12"><img class="img-fluid" src="img/ofr-2.jpg" alt=""></div>

							<div class="col-md-6 col-xs-12"><img class="img-fluid" src="img/ofr-1.jpg" alt=""></div>
						</div>
					</div>
				</div>

				<!--body area-->
				<div class="locator-section">
					<div class="container">
						<ul class="breadcrumb">
							<li class="breadcrumb-item"><a href="baby_care.html">{{$category->name}} /</a></li>

						</ul>
					</div>
				</div>

				<div class="product-page product text-center m-4">
					<h1 class="text-dark p-4 border-bottom">{{$category->name}}</h1>
					<div class="container">
						<div class="row">
							@foreach($products as $product)
							<div class="col-md-3 col-sm-4 col-6">
								<div class="card shopping-card">
									<div class="card-body">
										@php
											$image = \App\ImageProduct::query()->where('product_id', $product->id)->first();
										@endphp

										<img class="card-img-top img-fluid " src="{{asset('public/admin/product/'.$image->image)}}">
										<h4 class="card-title">{{$product->name}}</h4>
										{{--<small class="card-text">200 &nbsp;gm</small>--}}
										<h6 class="card-text font-weight-bold">{{$product->price}}&nbsp;৳</h6>
										<div class="h-a">

											<button type="button" class="btn-block align-middle btn btn-light mt-5 details" data-toggle="modal" data-target="#myModalDetails" data-id="{{$product->id}}">Details <span class="mx-auto"><i class="fas fa-angle-double-right"></i></span>
											</button>
										</div>
									</div>
									<div class="add-rmv-area  justify-content-center">
										<form class="form-inline d-j d-block" id="d-b">
											<button
													type="button"
													data-name="{{$product->name}}"
                                                    data-price="{{$product->price}}"
													data-id="{{ $product->id }}"
													class="
													    btn btn-warning btn-lg  p-0
													    cartMinus
													    inputValues
                                                        details-add-rmv-btn
                                                    ">
												<i class="fas fa-minus"></i>
                                            </button>

											<input
                                                type="text"
                                                class="
                                                    inputValues midValue
                                                    form-control qty{{$product->id}}
                                                    bg-warning
                                                    text-light
                                                    details-add-rmv-btn
                                                "
												   value="0 in bag"
												   data-id="{{$product->id}}"
                                                   data-qty="0"
												   readonly>
											<button
													data-id="{{ $product->id }}"
													type="button"
													data-name="{{$product->name}}"
                                                    data-price="{{$product->price}}"
													class="btn details-add-rmv-btn inputValues btn-warning btn-lg details-add-rmv-btn p-0
													cartPlus">
												<i class="fas fa-plus"></i>
                                            </button>
										</form>
										{{--<a href="#" data-name="{{$product->name}}"
                                           data-price="{{$product->price}}"
                                           class="add-to-cart cart-b add-btn btn btn-light text-danger font-weight-bold"
                                            data-id="{{$product->id}}"
                                        >
                                            Add to bag
                                        </a>--}}

									</div>
								</div>
							</div>
							@endforeach
						</div>
					</div>
				</div>
@endsection
@section('js')

{{--
    <script>
        $(document).ready(function(){
            sessionStorage.clear();
            carts();
        });

        $(document).on("click",".cartMinus",function(){
            var product_id = $(this).attr('data-id');

            const qty = $(".qty"+product_id).attr("data-qty");

            var incQty = (qty>0 ? Number(qty)-1 : 0);

            $(".qty"+product_id).attr({"data-qty":incQty});

            $(".qty"+product_id).val(incQty+ " in bag");

        });

        $(document).on("click",".cartPlus",function(){
            var product_id = $(this).attr('data-id');

            const qty = $(".qty"+product_id).attr("data-qty");

            var incQty = (qty>=0 ? Number(qty)+1 : 0);

            $(".qty"+product_id).attr({"data-qty":incQty});

            $(".qty"+product_id).val(incQty+ " in bag");

            if(incQty >0){
                storeToCart(product_id,incQty);
            }

            console.log("Plus : ",product_id+" Qty is : "+incQty);
        });

        function storeToCart(product_id,qty){
            $.ajax({
                method:"post",
                url : "{{ route('cart.store') }}",
                data:{product_id:product_id,qty:qty,_token:"{{csrf_token()}}"},
                dataType:"json",
                success:function(res){

                    console.log("success");
                    carts();
                }
            })
        }

        function carts(){
            sessionStorage.clear();

            $.ajax({
                method:"get",
                url : "{{ route('cart.items') }}",
                dataType:"json",
                success:function(res){
                    console.log(res);
                    $(".show-cart").html(res.items);
                    $(".total-cart").text(res.totalAmount);
                    $(".total-item").text(res.totalItem);

                }
            })
        };


        /* remove item from carts */

            $(document).on("click",".destroyItem",function(){
                var product_id = $(this).attr('data-id');

                $.ajax({
                    method:"post",
                    url :"{{ route('cart.destroy') }}",
                    data : {product_id:product_id,_token:'{{ csrf_token() }}'},
                    dataType:'json',
                    success:function(res){
                        carts();
                        console.log(res);
                    }
                })
        });


    </script>
--}}

@endsection
