<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!--/Required meta tags -->
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- /Bootstrap CSS -->
    <!------Owl Crousel------>
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">
    <!------/Owl Crousel------>
    <!------Fontawesome------>
    <link href="css/all.css" rel="stylesheet">
    <link href="css/fontawesome.css" rel="stylesheet">
    <link href="css/brands.css" rel="stylesheet">
    <link href="css/solid.css" rel="stylesheet">
    <!------/Fontawesome------>
    <!--<link rel="stylesheet" type="text/css" href="css/all.css">-->
    <link rel="stylesheet" href="css/zoomy.css">
    <link rel="stylesheet" type="text/css" href="css/w3.css">
    <link rel="stylesheet" href="css/animate.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="stylesheet" type="text/css" href="css/responsive.css">
    <!--<link rel="stylesheet" type="text/css" href="css/all.css">-->
</head>
<body class="bg-light img-fluid">
<!------------------------------------------Strat From Here----------------------------------------------->

<!--w3-collapse-navbar-->

<nav class="navbar navbar-expand-md bg-danger sticky-top text-center navbar-dark">
    <div class="container-fluid">
        <!-- Brand -->
        <ul class="navbar-nav">
            <li class="nav-item mx-auto">
                <a href="#" data-target=".sidebarb" class="text-light" data-toggle="collapse"><i class="fas fa-bars fa-lg py-2 p-1"></i></a>
            </li>
        </ul>
        <a class="navbar-brand px-4" href="index.html">
            <span class="logo-name text-light"><img src="img/logo.png" alt="" class="img-fluid logo"></span>
        </a>
        <form class="form-inline mx-auto" action="/action_page.php">
            <input class="form-control mr-sm-2" type="text" width="500px" placeholder="Search for products (e.g. eggs,milk,alu)">
            <button class="btn btn-danger search-btn" type="submit"><span><i class="fas fa-search"></i></span></button>
        </form>
        <!-- Toggler/collapsibe Button -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
            <span><i class="fas fa-ellipsis-v"></i></span>
        </button>

        <!-- Navbar links -->
        <div class="collapse navbar-collapse" id="collapsibleNavbar">

            <ul class="navbar-nav">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-4">
                            <li class="nav-item mx-auto p-2 active">
                                <a class="btn btn-warning p-2 nav-link "href="#">NEED HELP <span><i class="fas fa-question"></i></span></a>
                            </li>
                        </div>
                        <div class="col-xs-4">
                            <li class="nav-item mx-auto p-2 active">
                                <a class="nav-link p-2 btn btn-info" type="button" data-toggle="modal" data-target="#signIn">SIGN IN / SIGN UP</a>
                            </li>
                        </div>
                    </div>
                </div>
            </ul>
        </div>
    </div>
</nav>
<div id="main">
    <div class="container-fluid">
        <div class="row flex-nowrap wrapper">
            <div class="col-md-3 col-lg-2 col-5 pl-0 pr-0 s-b-c collapse show width border-right sidebarb vh-100">
                <div class="sidebar-d">
                    <div class="list-group border-0 card text-center text-md-left" id="sidebar">
                        <a href="index.html" class="list-group-item w3-bar-item border-right-0 d-inline-block" data-parent="#sidebar"><span class="text-primary">Home</span> </a>
                        <a href="baby_care.html" class="list-group-item w3-bar-item border-right-0 d-inline-block" data-parent="#sidebar"><span >Offers</span> </a>
                        <a href="baby_care.html" class="list-group-item w3-bar-item border-right-0 d-inline-block" data-parent="#sidebar"><span >Recipes</span></a>
                        <a href="baby_care.html" class="list-group-item w3-bar-item border-right-0 d-inline-block" data-parent="#sidebar"><span >Diccounts</span></a>
                        <a href="baby_care.html" class="list-group-item w3-bar-item border-right-0 d-inline-block" data-parent="#sidebar"><span class="text-warning">Product Request</span></a>
                        <hr>
                        <a href="baby_care.html" class="list-group-item w3-bar-item border-right-0 d-inline-block" data-parent="#sidebar"><span >Baby Care <span class="float-right"><i class="fas fa-angle-right"></i></span></span></a>
                        <a href="baby_care.html" class="list-group-item w3-bar-item border-right-0 d-inline-block" data-parent="#sidebar"><span >Pet Care <span class="float-right"><i class="fas fa-angle-right"></i></span></span></a>
                        <a href="baby_care.html" class="list-group-item w3-bar-item border-right-0 d-inline-block" data-parent="#sidebar"><span >Populer<span class="float-right"><i class="fas fa-angle-right"></i></span></span></a>
                    </div>
                </div>
            </div>

            <!--details modal-->
            <div class="modal fade" id="myModalDetails">
                <div class="modal-dialog">
                    <div class="modal-content">

                        <!-- Modal Header -->
                        <div class="modal-header">
                            <h4 class="modal-title">Product Details</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>

                        <!-- Modal body -->
                        <div class="modal-body mx-auto">
                            <div class="details-section">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="container">
                                                <div id="myGallery"><img src="img/baby-wipes.jpg" alt="" class="img-fluid"></div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <h1>Clariss Baby Wipes (Sensitive)</h1>
                                            <h6 class="text-secondary">120pic</h6>
                                            <h5 class="text-dark">৳২০০</h5>
                                            <br>
                                            <a href="#" data-name="Lemon" data-price="200" class="add-to-cart btn btn-danger btn-block">Add to cart</a>
                                            <p class="text-dark">
                                                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                                            </p>


                                        </div>
                                        <div class="col-md-12 d-f">
                                            <div class="footer p-4 bg-muted">
                                                <div class="container">
                                                    <div class="row">
                                                        <div class="col-md-8 f-l">
                                                            <div class="container">
                                                                <div class="row r-f">
                                                                    <div class="col-md-12">
                                                                        <div class="topbar">
                                                                            <div class="f-1-a float-left">
                                                                                <p><span>1 Hour Delivery &nbsp;&nbsp;<img src="img/png/1-hour.png" alt=""></span>
                                                                                    <span>Cash On Delivery &nbsp;&nbsp;<img src="img/png/cash-on-delivery.png" alt=""></span></p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-12"><h1><span class="text-danger">HD EXPRESS</span></h1>
                                                                        <p class="text-muted p-2">
                                                                            HD EXPRESS is an online shop in Dhaka, Bangladesh. We believe time is valuable to our fellow Dhaka residents, and that they should not have to waste hours in traffic, brave bad weather and wait in line just to buy basic necessities like eggs! This is why Chaldal delivers everything you need right at your door-step and at no additional cost.
                                                                        </p>
                                                                    </div>
                                                                    <div class="col-md-12 text-center">
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <ul>
                                                                                    <li class="text-dark">Customer Service</li>
                                                                                    <a href="contact-us.html"><li class="text-muted">Contact Us</li></a>
                                                                                    <a href="contact-us.html"><li class="text-muted">FAQ</li></a>
                                                                                </ul>
                                                                            </div>
                                                                            <div class="col-md-4">
                                                                                <ul>
                                                                                    <li class="text-dark">About HD EXPRESS</li>
                                                                                    <a href="privacy-policy.html"><li class="text-muted">Privacy Policy</li></a>
                                                                                    <a href="term-of-use.html"><li class="text-muted">Terms of Use</li></a>
                                                                                </ul>
                                                                            </div>
                                                                            <div class="col-md-4">
                                                                                <ul>
                                                                                    <li class="text-dark">For Business</li>
                                                                                    <a href="corporate.html"><li class="text-muted">Corporate</li></a>
                                                                                </ul>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </div>
                                                        <div class="col-md-4 f-r">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="topbar">
                                                                        <div class="f-2-a float-right">
                                                                            <p>
                                                                            <p>Pay With &nbsp; &nbsp;
                                                                                <span><img src="img/png/bkash.png" alt=""></span>
                                                                                <span><img src="img/png/cod.png" alt=""></span>
                                                                            </p>
                                                                            </p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-12 download-area">
                                                                    <div class="row">
                                                                        <div class="col-md-6"> <a href="#"><img class="img-fluid p-d" src="img/png/google-play.png" alt=""></a> </div>
                                                                        <div class="col-md-6"> <a href="#"><img class="img-fluid p-d" src="img/png/app-store.png" alt=""></a> </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-12">
                                                                    <p class="float-right"><span><img src="img/png/phone_icon.png" alt=""></span>&nbsp;&nbsp;&nbsp;01-xxxxxxxx</p>
                                                                    <p class="float-right"><span class="float-right">Or Support email us </span><span class="float-right text-success">hdexpress@email.com</span></p>
                                                                </div>
                                                                <div class="col-md-3"><a href="#"><img src="img/png/facebook.png" alt=""></a></div>
                                                                <div class="col-md-3"><a href="#"><img src="img/png/youtube.png" alt=""></a></div>
                                                                <div class="col-md-3"><a href="#"><img src="img/png/twitter.png" alt=""></a></div>
                                                                <div class="col-md-3"><a href="#"><img src="img/png/instagram.png" alt=""></a></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!--details-section-->
                        </div>

                        <!-- Modal footer -->
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                        </div>

                    </div>
                </div>
            </div>
            <!--details modal-->
            <main class="col px-5 pl-md-2 pt-2 main">
                <!--main-area-->
                <div class="container-nai">
                    <!-- main-section-->
                    <div class="main-pading-area">
                        <!--crousel area-->
                        <div class="1st-offer p-4">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-6"><img class="img-fluid" src="img/ofr-2.jpg" alt=""></div>

                                    <div class="col-md-6"><img class="img-fluid" src="img/ofr-1.jpg" alt=""></div>
                                </div>
                            </div>
                        </div>
                        <!--crousel area-->
                        <!--body area-->
                        <div class="locator-section">
                            <div class="container">
                                <ul class="breadcrumb">
                                    <li class="breadcrumb-item active"><a href="#">Baby Care</a></li>

                                </ul>
                            </div>
                        </div>

                        <div class="category-wise-select text-center p-4 mx-auto">
                            <h1 class="text-dark p-4 border-bottom">Baby Care</h1>
                            <div class="container">
                                <div class="catagory-wise-area">
                                    <div class="row">
                                        <div class="col-md-3 col-sm-4 col-6">
                                            <a href="wipes.html">
                                                <div class="card">
                                                    <img class="card-img-top img-fluid mx-auto" src="img/png/wipes.png" alt="">
                                                    <div class="card-body">
                                                        <a href="wipes.html" class="text-dark font-weight-bold">Wipes</a>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="col-md-3 col-sm-4 col-6">
                                            <a href="diapers.html">
                                                <div class="card">
                                                    <img class="card-img-top img-fluid mx-auto" src="img/png/diapers.png" alt="">
                                                    <div class="card-body">
                                                        <a href="diapers.html" class="text-dark font-weight-bold">Diapers</a>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="col-md-3 col-sm-4 col-6">
                                            <a href="diapers.html">
                                                <div class="card">
                                                    <img class="card-img-top img-fluid mx-auto" src="img/png/diapers.png" alt="">
                                                    <div class="card-body">
                                                        <a href="diapers.html" class="text-dark font-weight-bold">Diapers</a>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="col-md-3 col-sm-4 col-6">
                                            <a href="diapers.html">
                                                <div class="card">
                                                    <img class="card-img-top img-fluid mx-auto" src="img/png/diapers.png" alt="">

                                                    <div class="card-body">
                                                        <a href="diapers.html" class="text-dark font-weight-bold">Diapers</a>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="col-md-3 col-sm-4 col-6">
                                            <a href="wipes.html">
                                                <div class="card">
                                                    <img class="card-img-top img-fluid mx-auto" src="img/png/diapers.png" alt="">
                                                    <div class="card-body">
                                                        <a href="fooding.html" class="text-dark font-weight-bold">Fooding</a>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="col-md-3 col-sm-4 col-6">
                                            <a href="wipes.html">
                                                <div class="card">
                                                    <img class="card-img-top img-fluid mx-auto" src="img/png/diapers.png" alt="">
                                                    <div class="card-body">
                                                        <a href="fooding.html" class="text-dark font-weight-bold">Fooding</a>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="col-md-3 col-sm-4 col-6">
                                            <a href="wipes.html">
                                                <div class="card">
                                                    <img class="card-img-top img-fluid mx-auto" src="img/png/diapers.png" alt="">
                                                    <div class="card-body">
                                                        <a href="fooding.html" class="text-dark font-weight-bold">Fooding</a>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!--body area-->
                    </div>

                    <!--main-area-->
                    <!--Footer-->
                    <footer>
                        <div class="footer p-4 bg-muted">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-8 f-l">
                                        <div class="container">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="topbar">
                                                        <div class="f-1-a float-left">
                                                            <p><span>1 Hour Delivery &nbsp;&nbsp;<img src="img/png/1-hour.png" alt=""></span>
                                                                <span>Cash On Delivery &nbsp;&nbsp;<img src="img/png/cash-on-delivery.png" alt=""></span></p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12"><h1><span class="text-danger">HD EXPRESS</span></h1>
                                                    <p class="text-muted p-2">
                                                        HD EXPRESS is an online shop in Dhaka, Bangladesh. We believe time is valuable to our fellow Dhaka residents, and that they should not have to waste hours in traffic, brave bad weather and wait in line just to buy basic necessities like eggs! This is why Chaldal delivers everything you need right at your door-step and at no additional cost.
                                                    </p>
                                                </div>
                                                <div class="col-md-12 text-center">
                                                    <div class="row">
                                                        <div class="col-md-4 col-12 col-sm-12">
                                                            <ul>
                                                                <li class="text-dark">Customer Service</li>
                                                                <a href="contact-us.html"><li class="text-muted">Contact Us</li></a>
                                                                <a href="contact-us.html"><li class="text-muted">FAQ</li></a>
                                                            </ul>
                                                        </div>
                                                        <div class="col-md-4 col-12 col-sm-12">
                                                            <ul>
                                                                <li class="text-dark">About HD EXPRESS</li>
                                                                <a href="privacy-policy.html"><li class="text-muted">Privacy Policy</li></a>
                                                                <a href="term-of-use.html"><li class="text-muted">Terms of Use</li></a>
                                                            </ul>
                                                        </div>
                                                        <div class="col-md-4 col-12 col-sm-12">
                                                            <ul>
                                                                <li class="text-dark">For Business</li>
                                                                <a href="corporate.html"><li class="text-muted">Corporate</li></a>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="col-md-4 f-r">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="topbar">
                                                    <div class="f-2-a float-right">
                                                        <p>
                                                        <p>Pay With &nbsp; &nbsp;
                                                            <span><img src="img/png/bkash.png" alt=""></span>
                                                            <span><img src="img/png/cod.png" alt=""></span>
                                                        </p>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12 download-area">
                                                <div class="row">
                                                    <div class="col-md-6"> <a href="#"><img class="img-fluid" src="img/png/google-play.png" alt=""></a> </div>
                                                    <div class="col-md-6"> <a href="#"><img class="img-fluid" src="img/png/app-store.png" alt=""></a> </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <p class="float-right"><span><img src="img/png/phone_icon.png" alt=""></span>&nbsp;&nbsp;&nbsp;01-xxxxxxxx</p>
                                                <p class="float-right"><span class="float-right">Or Support email us </span><span class="float-right text-success">hdexpress@email.com</span></p>
                                            </div>
                                            <div class="col-md-3 col-3"><a href="#"><img class="img-fluid " src="img/png/facebook.png" alt=""></a></div>
                                            <div class="col-md-3 col-3"><a href="#"><img class="img-fluid " src="img/png/youtube.png" alt=""></a></div>
                                            <div class="col-md-3 col-3"><a href="#"><img class="img-fluid " src="img/png/twitter.png" alt=""></a></div>
                                            <div class="col-md-3 col-3"><a href="#"><img class="img-fluid " src="img/png/instagram.png" alt=""></a></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </footer>


            </main>


            <!--cart-area-->

            <!--cart-area-->
            <div class="w3-sidebar col-12 w3-bar-block w3-card w3-animate-right" style="display:none;right:0" id="mySidebar">
                <div class="cart-ds px-4 d-block"><h4 class="text-danger float-left font-weight-bold">ITEMS <span class="text-success float-right">0</span></h4><button class="btn btn-lg float-right rounded-0 btn-info"
                                                                                                                                                                        onclick="w3_close()" id="cartClose">Close</button></div>
                <div class="cart-top"><h6 class="price-area   p-4 bg-secondary text-light">Total price: ৳<span class="total-cart"></span><a href="order.html" class="btn btn-sm bg-light text-danger float-right font-weight-bold">Checkout</a href="order.html"></h6></div><div class="cart-bag py-4">
                    <table class="show-cart">

                    </table>


                </div>
            </div>

        </div>
        <!--body area-->

        <!-- main-section-->

        <!------------------------------------------/End Here----------------------------------------------->
        <div class="cart-bag sticky-right float-right rounded-right" id="cartBag">
            <div class="card text-light w3-button"  onclick="w3_open()"  id="openNav">
                <div class="card-body"><span class="total-item">0</span><br><span>items</span><br>৳<span class="total-cart"></span>
                </div>
            </div>
        </div>
    </div>
    <script src="js/jquery-3.3.1.slim.min.js" type="text/javascript"></script>
    <script src="js/jquery-3.4.1.min.js" type="text/javascript"></script>
    <script src="js/zoomy.js"></script>
    <script src="js/popper.min.js" type="text/javascript"></script>
    <script src="js/bootstrap.min.js" type="text/javascript"></script>
    <script src="js/owl.carousel.min.js" type="text/javascript"></script>
    <script src="js/app.js" type="text/javascript"></script>
</body>
</html>