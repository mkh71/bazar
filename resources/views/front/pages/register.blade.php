@extends('front.layouts.master')
<?php
use App\helper\Helpers\Helper;
$helpers = new Helper();
?>

@section('title',$helpers->titles()->where('page',1)->first())
@section('content')
			<main class="col px-5 pl-md-2 pt-2 main">
				<div class="container">
					<!--main-area-->
					<!-- main-section-->
					<div class="reg-form p-4 bg-light">
						<div class="container">
							<h1 class="text-center text-success font-weight-bold">Registration</h1>
							<form action="/action_page.php" class=" p-4 needs-validation" novalidate>
								<div class="form-row">
									<div class="col-md-3 m-2">
										<input type="text" class="form-control" id="fName" placeholder="Enter First Name" name="fName" required>
										<div class="valid-feedback">Valid.</div>
										<div class="invalid-feedback">Please fill out this field.</div>
									</div>
									<div class="col-md-3 m-2">
										<input type="text" class="form-control" id="lName" placeholder="Enter Last Name" name="lName" required>
										<div class="valid-feedback">Valid.</div>
										<div class="invalid-feedback">Please fill out this field.</div>
									</div>
									<div class="col-md-4 m-2">
										<input type="text" class="form-control" id="uname" placeholder="Enter username" name="uname" required>
										<div class="valid-feedback">Valid.</div>
										<div class="invalid-feedback">Please fill out this field.</div>
									</div>
									<div class="col-md-5 m-2">
										<input type="email" class="form-control" id="email" placeholder="Enter Email" name="email" required>
										<div class="valid-feedback">Valid.</div>
										<div class="invalid-feedback">Please fill out this field.</div>
									</div>
									<div class="col-md-5 m-2">
										<input type="tel" class="form-control" id="mobile" placeholder="Enter Your Phone Number e.g +880123456789" name="mobile" pattern="[0-9]{11}"required>
										<div class="valid-feedback">Valid.</div>
										<div class="invalid-feedback">Please fill out this field.</div>
									</div>
									<div class="col-md-5 m-2">
										<input type="password" class="form-control" id="pwd" placeholder="Enter password" name="pwd" required>
										<div class="valid-feedback">Valid.</div>
										<div class="invalid-feedback">Please fill out this field.</div>
									</div>
									<div class="col-md-5 m-2">
										<input type="password" class="form-control" id="cpwd" placeholder="Confirm password" name="cpwd" required>
										<div class="valid-feedback">Valid.</div>
										<div class="invalid-feedback">Please fill out this field.</div>
									</div>
									<div class="col-md-4 m-2 form-inline">
										<label for="gender" class="font-weight-bold">Select&nbsp; Gender: &nbsp;&nbsp;</label>
										<select class="form-control" id="gender">
											<option>Select</option>
											<option>Male</option>
											<option>Female</option>
											<option>Others</option>
										</select>
									</div>
									<div class="col-md-3 m-2 form-inline">
										<label for="zila" class="font-weight-bold">Select&nbsp; Zila: &nbsp;&nbsp;</label>
										<select class="form-control" id="zila">
											<option>Select</option>
											<option>Dhaka</option>
											<option>Chattogram</option>
											<option>Shylet</option>
										</select>
									</div>
									<div class="col-md-3 m-2 form-inline">
										<label for="thana" class="font-weight-bold">Select&nbsp; Thana: &nbsp;&nbsp;</label>
										<select class="form-control" id="thana">
											<option>Select</option>
											<option>Anwara</option>
											<option>Chadgaon</option>
											<option>Bakalia</option>
											<option>Kotowali</option>
											<option>Khulshi</option>
										</select>
									</div>
									<div class="col-md-10 m-2">
										<textarea type="adress" class="form-control" id="adress" placeholder="Enter address" name="adress" required></textarea>
										<div class="valid-feedback">Valid.</div>
										<div class="invalid-feedback">Please fill out this field.</div>

									</div>
								</div>
								<div class="form-group form-check">
									<label class="form-check-label">
										<input class="form-check-input" type="checkbox" name="remember" required> I agree on this service by following all this terms & Condition
										<div class="valid-feedback">Valid.</div>
										<div class="invalid-feedback">Check this checkbox to continue.</div>
									</label>
								</div>
								<button type="submit" class="btn text-light btn-success">Submit</button>
							</form>
						</div>
					</div>

@stop