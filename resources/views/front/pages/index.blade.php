@extends('front.layouts.master')
<?php
use App\helper\Helpers\Helper;
    $helpers = new Helper();
?>

@section('title',$helpers->titles()->where('page',1)->first())
@section('content')
    <main class="col px-5 pl-md-2 pt-2 main">
        <div class="container">
            <div class="row">

                <div class="col-md-12 1st-crousel">
                    <div id="demo" class="carousel slide" data-ride="carousel">
                        {{--<!-- Indicators -->
                        <ul class="carousel-indicators">
                            <li data-target="#demo" data-slide-to="0" class="active"></li>
                            <li data-target="#demo" data-slide-to="1"></li>
                            <li data-target="#demo" data-slide-to="2"></li>
                        </ul>--}}
                        <!-- The slideshow -->
                        <div class="carousel-inner row w-100 mx-auto text-center" role="listbox">
                            @foreach($sliders as $slider)
                                <div class="carousel-item">
                                    <img class="img-fluid" src="{{asset('public/slider/'.$slider->image)}}" alt="Slider"
                                         width="1100" height="500">
                                </div>
                            @endforeach

                        </div>
                    </div>
                </div>

                <!--section-padding-2-product-categories-->
                <div class="col-md-12 text-center">
                    <div class=" section-padding-2 p-4">
                        <h2>Our Product Categories</h2>
                        <div class="row">

                            @foreach($helpers->categories() as $category)

                                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-4">
                                    @if(count($category->subcategories) > 0)
                                        <a href="{{route('visit.category',$category->id)}}">
                                            @else
                                                <a href="{{route('visit.product',$category->id)}}">
                                                    @endif
                                                    <div class="card m-2">
                                                        <div class="card-body">
                                                            <p>
                                                                <strong class="text text-uppercase">	{{$category->name}}</strong>
                                                                <span class="float-right">
                                                                    <img
                                                                            class="img-fluid"
                                                                            src="{{asset('public/admin/category/'.$category->image)}}" height="20px" width="30px"
                                                                            alt=""
                                                                    />
                                                                </span>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </a>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                <!--section-padding-2-product-categories-->

                <!--section-padding-3-more-offers-->
                <div class="col-md-12">
                    <div class=" section-padding-3 m-4 text-center">
                        <div class="container">
                            <div class="row">
                                @foreach($helpers->offers() as $offer)
                                @php
                                    $i = 1;
                                    $i++;
                                    if($i == 3){
                                        break;
                                    }
                                @endphp
                                <div class="col-md-6"><img class="img-fluid" src="{{asset('public/admin/offer/offer_product/'.$offer->image)}}img/ofr-2.jpg" alt="">
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
                <!--why chaldal-->
                <div class="col-md-12">
                    <div class="container">
                        <div class="why-hd">
                            <div><img class="img-fluid mx-auto" src="{{asset('public/admin/img/tutorial-1.jpg')}}" alt=""></div>
                            <div><img class="img-fluid mx-auto" src="{{asset('public/admin/img/tutorial-2.jpg')}}" alt=""></div>
                            <div><img class="img-fluid mx-auto" src="{{asset('public/admin/img/tutorial-3.jpg')}}" alt=""></div>
                        </div>
                    </div>
                </div>
                <!--why chaldal-->
                <!--section-padding-3-more-offers-->
                <div class="col-md-12">
                    <div class="container">
                        <div class="special-offer-product product text-center mx-4">
                            <h1 class="text-secondary">Special offers</h1>
                            <div class="main-card-area">
                                <div class="crousel-sec-2 mx-auto">
                                    <div class="speacial-product">
                                        <div>
                                            <div class="card shopping-card">
                                                <div class="card-body">
                                                    <img class="card-img-top img-fluid "
                                                         src="img/baby-wipes.jpg">
                                                    <h4 class="card-title">diaper</h4>
                                                    <small class="card-text">200 &nbsp;gm</small>
                                                    <h6 class="card-text font-weight-bold">165
                                                        &nbsp;৳</h6>
                                                    <div class="h-a">

                                                        <button type="button"
                                                                class="btn-block align-middle btn btn-light mt-5 details"
                                                                data-toggle="modal"
                                                                data-target="#myModalDetails">Details
                                                            <span class="mx-auto"><i
                                                                        class="fas fa-angle-double-right"></i></span>
                                                        </button>
                                                    </div>
                                                </div>
                                                <div class="add-rmv-area  justify-content-center">
                                                    <form class="form-inline d-j d-none" id="d-b">
                                                        <button type="button" data-name="Diaper"
                                                                data-price="165"
                                                                class="minus-item btn btn-warning btn-lg details-add-rmv-btn p-0 minus">
                                                            <i class="fas fa-minus"></i></button>
                                                        <input type="text"
                                                               class="form-control  input-details bg-warning text-light"
                                                               value="1 in bag" readonly>
                                                        <button type="button" data-name="Diaper"
                                                                data-price="165"
                                                                class="plus-item btn btn-warning btn-lg details-add-rmv-btn p-0 plus">
                                                            <i class="fas fa-plus"></i></button>
                                                    </form>
                                                    <a
                                                        href="#"
                                                        data-name="Diaper"
                                                        data-price="165"
                                                       class="add-to-cart cart-b add-btn btn btn-light text-danger font-weight-bold">
                                                        Add to bag
                                                    </a>

                                                </div>
                                            </div>
                                        </div>
                                        <div>
                                            <div class="card shopping-card">
                                                <div class="card-body">
                                                    <img class="card-img-top img-fluid "
                                                         src="img/baby-wipes.jpg">
                                                    <h4 class="card-title">diaper</h4>
                                                    <small class="card-text">200 &nbsp;gm</small>
                                                    <h6 class="card-text font-weight-bold">165
                                                        &nbsp;৳</h6>
                                                    <div class="h-a">

                                                        <button type="button"
                                                                class="btn-block align-middle btn btn-light mt-5 details"
                                                                data-toggle="modal"
                                                                data-target="#myModalDetails">Details
                                                            <span class="mx-auto"><i
                                                                        class="fas fa-angle-double-right"></i></span>
                                                        </button>
                                                    </div>
                                                </div>
                                                <div class="add-rmv-area  justify-content-center">
                                                    <form class="form-inline d-j d-none" id="d-b">
                                                        <button type="button" data-name="Diaper"
                                                                data-price="165"
                                                                class="minus-item btn btn-warning btn-lg details-add-rmv-btn p-0 minus">
                                                            <i class="fas fa-minus"></i></button>
                                                        <input type="text"
                                                               class="form-control  input-details bg-warning text-light"
                                                               value="1 in bag" readonly>
                                                        <button type="button" data-name="Diaper"
                                                                data-price="165"
                                                                class="plus-item btn btn-warning btn-lg details-add-rmv-btn p-0 plus">
                                                            <i class="fas fa-plus"></i></button>
                                                    </form>
                                                    <a href="#" data-name="Diaper" data-price="165"
                                                       class="add-to-cart cart-b add-btn btn btn-light text-danger font-weight-bold">Add
                                                        to bag</a>

                                                </div>
                                            </div>
                                        </div>
                                        <div>
                                            <div class="card shopping-card">
                                                <div class="card-body">
                                                    <img class="card-img-top img-fluid "
                                                         src="img/baby-wipes.jpg">
                                                    <h4 class="card-title">diaper</h4>
                                                    <small class="card-text">200 &nbsp;gm</small>
                                                    <h6 class="card-text font-weight-bold">165
                                                        &nbsp;৳</h6>
                                                    <div class="h-a">

                                                        <button type="button"
                                                                class="btn-block align-middle btn btn-light mt-5 details"
                                                                data-toggle="modal"
                                                                data-target="#myModalDetails">Details
                                                            <span class="mx-auto"><i
                                                                        class="fas fa-angle-double-right"></i></span>
                                                        </button>
                                                    </div>
                                                </div>
                                                <div class="add-rmv-area  justify-content-center">
                                                    <form class="form-inline d-j d-none" id="d-b">
                                                        <button type="button" data-name="Diaper"
                                                                data-price="165"
                                                                class="minus-item btn btn-warning btn-lg details-add-rmv-btn p-0 minus">
                                                            <i class="fas fa-minus"></i></button>
                                                        <input type="text"
                                                               class="form-control  input-details bg-warning text-light"
                                                               value="1 in bag" readonly>
                                                        <button type="button" data-name="Diaper"
                                                                data-price="165"
                                                                class="plus-item btn btn-warning btn-lg details-add-rmv-btn p-0 plus">
                                                            <i class="fas fa-plus"></i></button>
                                                    </form>
                                                    <a href="#" data-name="Diaper" data-price="165"
                                                       class="add-to-cart cart-b add-btn btn btn-light text-danger font-weight-bold">Add
                                                        to bag</a>

                                                </div>
                                            </div>
                                        </div>
                                        <div>
                                            <div class="card shopping-card">
                                                <div class="card-body">
                                                    <img class="card-img-top img-fluid "
                                                         src="img/baby-wipes.jpg">
                                                    <h4 class="card-title">diaper</h4>
                                                    <small class="card-text">200 &nbsp;gm</small>
                                                    <h6 class="card-text font-weight-bold">165
                                                        &nbsp;৳</h6>
                                                    <div class="h-a">

                                                        <button type="button"
                                                                class="btn-block align-middle btn btn-light mt-5 details"
                                                                data-toggle="modal"
                                                                data-target="#myModalDetails">Details
                                                            <span class="mx-auto"><i
                                                                        class="fas fa-angle-double-right"></i></span>
                                                        </button>
                                                    </div>
                                                </div>
                                                <div class="add-rmv-area  justify-content-center">
                                                    <form class="form-inline d-j d-none" id="d-b">
                                                        <button type="button" data-name="Diaper"
                                                                data-price="165"
                                                                class="minus-item btn btn-warning btn-lg details-add-rmv-btn p-0 minus">
                                                            <i class="fas fa-minus"></i></button>
                                                        <input type="text"
                                                               class="form-control  input-details bg-warning text-light"
                                                               value="1 in bag" readonly>
                                                        <button type="button" data-name="Diaper"
                                                                data-price="165"
                                                                class="plus-item btn btn-warning btn-lg details-add-rmv-btn p-0 plus">
                                                            <i class="fas fa-plus"></i></button>
                                                    </form>
                                                    <a href="#" data-name="Diaper" data-price="165"
                                                       class="add-to-cart cart-b add-btn btn btn-light text-danger font-weight-bold">Add
                                                        to bag</a>

                                                </div>
                                            </div>
                                        </div>
                                        <div>
                                            <div class="card shopping-card">
                                                <div class="card-body">
                                                    <img class="card-img-top img-fluid "
                                                         src="img/baby-wipes.jpg">
                                                    <h4 class="card-title">diaper</h4>
                                                    <small class="card-text">200 &nbsp;gm</small>
                                                    <h6 class="card-text font-weight-bold">165
                                                        &nbsp;৳</h6>
                                                    <div class="h-a">

                                                        <button type="button"
                                                                class="btn-block align-middle btn btn-light mt-5 details"
                                                                data-toggle="modal"
                                                                data-target="#myModalDetails">Details
                                                            <span class="mx-auto"><i
                                                                        class="fas fa-angle-double-right"></i></span>
                                                        </button>
                                                    </div>
                                                </div>
                                                <div class="add-rmv-area  justify-content-center">
                                                    <form class="form-inline d-j d-none" id="d-b">
                                                        <button type="button" data-name="Diaper"
                                                                data-price="165"
                                                                class="minus-item btn btn-warning btn-lg details-add-rmv-btn p-0 minus">
                                                            <i class="fas fa-minus"></i></button>
                                                        <input type="text"
                                                               class="form-control  input-details bg-warning text-light"
                                                               value="1 in bag" readonly>
                                                        <button type="button" data-name="Diaper"
                                                                data-price="165"
                                                                class="plus-item btn btn-warning btn-lg details-add-rmv-btn p-0 plus">
                                                            <i class="fas fa-plus"></i></button>
                                                    </form>
                                                    <a href="#" data-name="Diaper" data-price="165"
                                                       class="add-to-cart cart-b add-btn btn btn-light text-danger font-weight-bold">Add
                                                        to bag</a>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

@endsection
