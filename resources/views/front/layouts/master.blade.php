<?php
    use App\helper\Helpers\Helper;
    $helpers = new Helper();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

   <title>  @yield('title') </title>
  {{--  <title>Industrial</title>--}}
    <meta name="author" content="HD Express">

    <meta name="viewport" content="width=device-width; initial-scale=1.0">

    <link rel="stylesheet" type="text/css" href="{{ asset('public/front/css/hover.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('public/front/font-awesome/css/font-awesome.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('public/front/css/style.css')}}"/>
    <link rel="shortcut icon" href="{{ asset('public/favicon.png') }}">
    <link rel="stylesheet" href="{{ asset('public/front/css/bootstrap.min.css')}}">
    <!-- /Bootstrap CSS -->
    <!------Owl Crousel------>
    <link rel="stylesheet" href="{{ asset('public/front/css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{ asset('public/front/css/owl.theme.default.min.css')}}">
    <!------/Owl Crousel------>
    <!------Fontawesome------>
    <link href="{{ asset('public/front/css/all.css')}}" rel="stylesheet">
    <link href="{{ asset('public/front/css/slick.css')}}" rel="stylesheet">
    <link href="{{ asset('public/front/css/slick-theme.css')}}" rel="stylesheet">
    <link href="{{ asset('public/front/css/fontawesome.css')}}" rel="stylesheet">
    <link href="{{ asset('public/front/css/brands.css')}}" rel="stylesheet">
    <link href="{{ asset('public/front/css/solid.css')}}" rel="stylesheet">
    <!------/Fontawesome------>
    <link rel="stylesheet" href="{{ asset('public/front/css/zoomy.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('public/front/css/w3.css')}}">
    <link rel="stylesheet" href="{{ asset('public/front/css/animate.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('public/front/css/style.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('public/front/css/responsive.css')}}">
    <style>
        .inputValues{

        }

        .midValue{

        }
    </style>
    @yield('css')
</head>

<body class="bg-light img-fluid">
@include('front.include.header')
@include('front.include.sidebar')
@yield('content')

@include('front.include.whychose')
@include('front.include.footer')



<!--details modal-->
<div class="modal fade" id="myModalDetails">
    <div class="modal-dialog ">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title text-center text-info">Product Details</h4>
                <button type="button" class="close rounded-circle" data-dismiss="modal"><i class="fas fa-times-circle"></i></button>
            </div>

            <!-- Modal body -->
            <div class="modal-body mx-auto">
                <div class="details-section">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-6 col-12 col-md-6 modt">
                                <div class="container">
                                    <div id="myGallery"><img src="img/baby-wipes.jpg" alt="" class="img-fluid"></div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-12 col-md-6">
                                <h1>Clariss Baby Wipes (Sensitive)</h1>
                                <h6 class="text-secondary">120pic</h6>
                                <h5 class="text-dark">৳২০০</h5>
                                <br>
                                <form class="form-inline" id="d-f">
                                    <button type="button" class="btn btn-danger btn-lg details-add-rmv-btn minus"><i class="fas fa-minus"></i></button>
                                    <input type="text" class="form-control input-details" value="0" readonly>
                                    <button type="button" class="btn btn-danger btn-lg details-add-rmv-btn plus"><i class="fas fa-plus"></i></button>
                                    <a href="#" data-name="Lemon" data-price="200" data-weight="200" class="float-right ml-5 add-to-cart btn btn-danger btn-lg"  data-dismiss="modal"  onclick="w3_open()">ALL ADD TO BAG</a>
                                </form>
                                <p class="text-dark">
                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                                </p>

                            </div>
                        </div>
                    </div>
                </div>

                <!--details-section-->
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>

        </div>
    </div>
</div>
<!--details modal-->


<a class="scroll-top" href="#"><i class="fa fa-angle-up"></i> </a>
<!-- Scripts -->
    <script src="{{ asset('public/front/js/jquery-3.3.1.slim.min.js')}}"></script>
    <script src="{{ asset('public/front/js/jquery-3.4.1.min.js')}}"></script>
    <script src="{{ asset('public/front/js/bootstrap.min.js')}}"></script>
    <script src="{{ asset('public/front/js/slick.min.js')}}"></script>
    <script src="{{ asset('public/front/js/zoomy.js')}}"></script>
    <script src="{{ asset('public/front/js/popper.min.js')}}"></script>
    <script src="{{ asset('public/front/js/app.js')}}"></script>
    <script src="{{ asset('public/front/plugins/owl.carousel.min.js')}}"></script>
    <script src="{{ asset('public/front/OwlCarousel/dist/owl.carousel.min.html')}}"></script>
    <script src="{{ asset('public/front/plugins/waypoints.min.js')}}"></script>
    <script src="{{ asset('public/front/plugins/jquery.counterup.min.js')}}"></script>
    <script src="{{ asset('public/front/plugins/theme.js')}}"></script>
    @yield('script')
    @yield('js')

<script>
    var current_productId = null;
    var previews_productId = null;

    $(document).ready(function(){
        sessionStorage.clear();
        carts();
    });

    $(document).on("click",".cartMinus",function(){
        var product_id = $(this).attr('data-id');

        const qty = $(".qty"+product_id).attr("data-qty");

        var incQty = (qty>0 ? Number(qty)-1 : 0);
        //var incQty = qty; //(qty>0 ? Number(qty)-1 : 0);
        $(".qty"+product_id).attr({"data-qty":incQty});

        $(".qty"+product_id).val(incQty+ " in bag");
        if(qty>0){
            updateToCart(product_id);
        }
    });

    function updateToCart(product_id){

        $.ajax({
            method:"post",
            url : "{{ route('cart.update') }}",
            data:{product_id:product_id,_token:"{{csrf_token()}}"},
            dataType:"json",
            success:function(res){
                carts();
            }
        });

    }

    $(document).on("click",".cartPlus",function(){
        var product_id = $(this).attr('data-id');

        const qty = $(".qty"+product_id).attr("data-qty");

        var incQty = (qty>=0 ? Number(qty)+1 : 0);

        $(".qty"+product_id).attr({"data-qty":incQty});

        $(".qty"+product_id).val(incQty+ " in bag");

        if(incQty >0){
            storeToCart(product_id,incQty);
        }

        console.log("Plus : ",product_id+" Qty is : "+incQty);
    });

    function storeToCart(product_id,qty){

        $.ajax({
            method:"post",
            url : "{{ route('cart.store') }}",
            data:{product_id:product_id,qty:qty,_token:"{{csrf_token()}}"},
            dataType:"json",
            success:function(res){

                console.log("success");
                carts();
            }
        })
    }

    function carts(){
        sessionStorage.clear();

        $.ajax({
            method:"get",
            url : "{{ route('cart.items') }}",
            dataType:"json",
            success:function(res){

                $(".show-cart").html(res.items);
                $(".total-cart").text(res.totalAmount);
                $(".total-item").text(res.totalItem);

                /* product qty assigning */
                /* after increment or decrement you have to show individual product qty amount which you or user added
                 in  cart*/
                if(res.isEmpty !=true){
                    (res.products).forEach((data,index)=>{
                        const cur_qty = res.qtys[index].qty;
                        const id = data.product_id;
                        $(".qty"+id).attr({'data-qty':cur_qty});
                        $(".qty"+id).val(cur_qty+" in bag");

                        console.log(data.product_id); // Product Id;
                        console.log('Product Qty : ',res.qtys[index].qty);
                    })
                }else{
                    $('.qty'+current_productId).attr({'data-qty':0});
                    $('.qty'+current_productId).val('0 in bag');
                }
            }
        })
    };


    /* remove item from carts */

    $(document).on("click",".destroyItem",function(){
        var product_id = $(this).attr('data-id');

        $(".qty"+product_id).attr({"data-qty":0});
        $(".qty"+product_id).val("0 in bag");

        $.ajax({
            method:"post",
            url :"{{ route('cart.destroy') }}",
            data : {product_id:product_id,_token:'{{ csrf_token() }}'},
            dataType:'json',
            success:function(res){
                carts();
                console.log(res);
            }
        })
    });


</script>


</body>

</html>
