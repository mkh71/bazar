<div id="main">
    <div class="container-fluid">
        <div class="row flex-nowrap wrapper">
            <div class="col-md-3 col-lg-2 col-5 pl-0 pr-0 s-b-c collapse show width border-right sidebarb vh-100">
                <div class="sidebar-d">
                    <div class="list-group border-0 card text-center text-md-left" id="sidebar">
                        <a href="{{route('/')}}"
                           class="list-group-item w3-bar-item border-right-0 d-inline-block"
                           data-parent="#sidebar"><span class="text-primary">Home</span> </a>
                        <a href="baby_care.blade.php"
                           class="list-group-item w3-bar-item border-right-0 d-inline-block"
                           data-parent="#sidebar"><span>Offers</span> </a>
                        <a href="baby_care.blade.php"
                           class="list-group-item w3-bar-item border-right-0 d-inline-block"
                           data-parent="#sidebar"><span>Recipes</span></a>
                        <a href="baby_care.blade.php"
                           class="list-group-item w3-bar-item border-right-0 d-inline-block"
                           data-parent="#sidebar"><span>Diccounts</span></a>
                        <a href="baby_care.blade.php"
                           class="list-group-item w3-bar-item border-right-0 d-inline-block"
                           data-parent="#sidebar"><span class="text-warning">Product Request</span></a>
                        <hr>
                        @foreach($helpers->categories() as $category)
                            @if(count($category->subcategories) > 0)
                                <a href="{{route('visit.category',$category->id)}}"
                                   class="list-group-item w3-bar-item border-right-0 d-inline-block"
                                   data-parent="#sidebar">
                                <span>{{$category->name}}
                                    <span class="float-right">
                                        <i class="fas fa-angle-right"></i>
                                    </span>
                                </span>
                                </a>
                            @else
                                <a href="{{route('visit.product',$category->id)}}"
                                   class="list-group-item w3-bar-item border-right-0 d-inline-block"
                                   data-parent="#sidebar">
                                    <span>{{$category->name}}</span>
                                </a>
                            @endif
                        @endforeach
                    </div>
                </div>
            </div>

