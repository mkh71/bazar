
<section class="testimonials-two">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="titles">
                    <h2>Testimonials</h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div id="testimonials-three2" class="owl-carousel owl-theme">
                <div class="item">
                    <blockquote>
                        Fusce non faucibus lorem. Cras eu velit id diam cursus tincidunt in ut dui. Quisque quis augue placerat, pulvinar dui aliquam, convallis sapien. Orci varius natoque penatibus et magnis dis parturient montes.
                    </blockquote>


                    <div class="testimonials-author">
                        <div class="author-img">
                            <img class="img-responsive" src="{{ asset('public/front/img/author.jpg')}}" />
                        </div>
                        <h4>George Simons<span> - Lawyer</span></h4>
                        <ul>
                            <li><i class="fa fa-star" aria-hidden="true"></i></li>
                            <li><i class="fa fa-star" aria-hidden="true"></i></li>
                            <li><i class="fa fa-star" aria-hidden="true"></i></li>
                            <li><i class="fa fa-star" aria-hidden="true"></i></li>
                            <li><i class="fa fa-star" aria-hidden="true"></i></li>
                        </ul>
                    </div>
                </div>
                <div class="item">
                    <blockquote>
                        Fusce non faucibus lorem. Cras eu velit id diam cursus tincidunt in ut dui. Quisque quis augue placerat, pulvinar dui aliquam, convallis sapien. Orci varius natoque penatibus et magnis dis parturient montes.
                    </blockquote>
                    <div class="testimonials-author">
                        <div class="author-img">
                            <img class="img-responsive" src="{{ asset('public/front/img/author.jpg')}}" />
                        </div>
                        <h4>George Simons<span> - Lawyer</span></h4>
                        <ul>
                            <li><i class="fa fa-star" aria-hidden="true"></i></li>
                            <li><i class="fa fa-star" aria-hidden="true"></i></li>
                            <li><i class="fa fa-star" aria-hidden="true"></i></li>
                            <li><i class="fa fa-star" aria-hidden="true"></i></li>
                            <li><i class="fa fa-star" aria-hidden="true"></i></li>
                        </ul>
                    </div>

                </div>
                <div class="item">
                    <blockquote>
                        Fusce non faucibus lorem. Cras eu velit id diam cursus tincidunt in ut dui. Quisque quis augue placerat, pulvinar dui aliquam, convallis sapien. Orci varius natoque penatibus et magnis dis parturient montes.
                    </blockquote>
                    <div class="testimonials-author">
                        <div class="author-img">
                            <img class="img-responsive" src="{{ asset('public/front/img/author.jpg')}}" />
                        </div>
                        <h4>George Simons<span> - Lawyer</span></h4>
                        <ul>
                            <li><i class="fa fa-star" aria-hidden="true"></i></li>
                            <li><i class="fa fa-star" aria-hidden="true"></i></li>
                            <li><i class="fa fa-star" aria-hidden="true"></i></li>
                            <li><i class="fa fa-star" aria-hidden="true"></i></li>
                            <li><i class="fa fa-star" aria-hidden="true"></i></li>
                        </ul>
                    </div>

                </div>
            </div>
        </div>
</section>
