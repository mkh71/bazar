
    </div> {{--End of After main DIV--}}
        <footer>
    <div class="footer p-4 bg-muted">
        <div class="container">
            <div class="row">
                <div class="col-md-8 f-l">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="topbar">
                                    <div class="f-1-a float-left">
                                        <p><span>1 Hour Delivery &nbsp;&nbsp;<img
                                                        src="{{asset('public/admin/png/1-hour.png')}}" alt=""></span>
                                            <span>Cash On Delivery &nbsp;&nbsp;<img
                                                        src="{{asset('public/admin/png/cash-on-delivery.png')}}"
                                                        alt=""></span></p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12"><h1><span
                                            class="text-danger">HD EXPRESS</span></h1>
                                <p class="text-muted p-2">
                                    HD EXPRESS is an online shop in Dhaka, Bangladesh. We
                                    believe time is valuable to our fellow Dhaka residents, and
                                    that they should not have to waste hours in traffic, brave
                                    bad weather and wait in line just to buy basic necessities
                                    like eggs! This is why Chaldal delivers everything you need
                                    right at your door-step and at no additional cost.
                                </p>
                            </div>
                            <div class="col-md-12 text-center">
                                <div class="row">
                                    <div class="col-md-4 col-12 col-sm-12">
                                        <ul>
                                            <li class="text-dark">Customer Service</li>
                                            <a href="contact-us.html">
                                                <li class="text-muted">Contact Us</li>
                                            </a>
                                            <a href="contact-us.html">
                                                <li class="text-muted">FAQ</li>
                                            </a>
                                        </ul>
                                    </div>
                                    <div class="col-md-4 col-12 col-sm-12">
                                        <ul>
                                            <li class="text-dark">About HD EXPRESS</li>
                                            <a href="privacy-policy.html">
                                                <li class="text-muted">Privacy Policy</li>
                                            </a>
                                            <a href="term-of-use.html">
                                                <li class="text-muted">Terms of Use</li>
                                            </a>
                                        </ul>
                                    </div>
                                    <div class="col-md-4 col-12 col-sm-12">
                                        <ul>
                                            <li class="text-dark">For Business</li>
                                            <a href="corporate.html">
                                                <li class="text-muted">Corporate</li>
                                            </a>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="col-md-4 f-r">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="topbar">
                                <div class="f-2-a float-right">
                                    <p>
                                    <p>Pay With &nbsp; &nbsp;
                                        <span><img src="{{asset('public/admin/png/bkash.png')}}" alt=""></span>
                                        <span><img src="{{asset('public/admin/png/cod.png')}}" alt=""></span>
                                    </p>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 download-area">
                            <div class="row">
                                <div class="col-md-6"><a href="#"><img class="img-fluid"
                                                                       src="{{asset('public/admin/png/google-play.png')}}"
                                                                       alt=""></a></div>
                                <div class="col-md-6"><a href="#"><img class="img-fluid"
                                                                       src="{{asset('public/admin/png/app-store.png')}}"
                                                                       alt=""></a></div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <p class="float-right"><span><img src="{{asset('public/admin/png/phone_icon.png')}}"
                                                              alt=""></span>&nbsp;&nbsp;&nbsp;01-xxxxxxxx
                            </p>
                            <p class="float-right"><span
                                        class="float-right">Or Support email us </span><span
                                        class="float-right text-success">hdexpress@email.com</span>
                            </p>
                        </div>
                        <div class="col-md-3 col-3"><a href="#"><img class="img-fluid "
                                                                     src="{{asset('public/admin/png/facebook.png')}}"
                                                                     alt=""></a></div>
                        <div class="col-md-3 col-3"><a href="#"><img class="img-fluid "
                                                                     src="{{asset('public/admin/png/youtube.png')}}"
                                                                     alt=""></a></div>
                        <div class="col-md-3 col-3"><a href="#"><img class="img-fluid "
                                                                     src="{{asset('public/admin/png/twitter.png')}}"
                                                                     alt=""></a></div>
                        <div class="col-md-3 col-3"><a href="#"><img class="img-fluid "
                                                                     src="{{asset('public/admin/png/instagram.png')}}"
                                                                     alt=""></a></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
        <section class="footer-bottom">
            <div class="container">
                <div class="row">
                    <div class="pull-left col-3">
                        <p>@Copyright : HD Express | {{ Date("Y") }}</p>
                    </div>
                    <div class="col-offset-6 col-3">
                        <p>Developed By : SpinnerTech</p>
                    </div>
                </div>
            </div>
        </section>
</main>
        <!--Main area End here-->

        <!--cart-area-->
        <div class="w3-sidebar col-12 w3-bar-block w3-card w3-animate-right" style="display:none;right:0"
             id="mySidebar">
            <div class="cart-ds px-4 d-block"><h4 class="text-danger float-left font-weight-bold">ITEMS <span
                            class="text-success float-right total-item">0</span></h4>
                <button class="btn btn-lg float-right rounded-0 btn-info"
                        onclick="w3_close()" id="cartClose">Close
                </button>
            </div>
            <div class="cart-top">
                <h6 class="price-area p-4 bg-secondary text-light">Total price: ৳<span
                            class="total-cart"></span>
                    <a href="{{route('order.product')}}"
                       class="btn btn-sm bg-light text-danger float-right font-weight-bold">Checkout</a>
                </h6></div>
            <div class="cart-bag py-4">
                <table class="show-cart">

                </table>
            </div>
        </div>
        <!--body area-->
        <div class="cart-bag sticky-right float-right rounded-right" id="cartBag">
            <div class="card text-light w3-button" onclick="w3_open()" id="openNav">
                <div class="card-body">
                    <span class="total-item">0</span>
                    <br>
                    <span>items</span>
                    <br>৳
                    <span class="total-cart"></span>
                </div>;
            </div>
        </div>


    </div>
</div>
