
<section class="our-team">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="team-text">
                    <div class="title">
                        <h2>Our Experts</h2>
                    </div>
                    <p> {!! $helpers->company()->about !!} </p>
                </div>
            </div>
            <div class="col-md-8">

                <div class="members">

                   @if($helpers->teams() !=null )
                        @foreach($helpers->teams() as $team)

                            <div class="member">

                                <figure>
                                    <img style="height: 100%" src="{{ $team->image !=null ? asset('public/admin/team/'.$team->image) : asset('public/front/img/member1.jpg')}}" alt="No Image" title="{{ $team->name }}"/>
                                    <div class="overlay">
                                        <div class="inner-box">
                                            <ul class="social">
                                                <li><a href="{{ $team->facebook }}"><i class="fa fa-facebook" aria-hidden="true"></i> </a></li>
                                                <li><a href="{{ $team->twitter }}"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                                <li><a href="{{ $team->instagram }}"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </figure>

                                <div class="author-info " style="overflow: hidden">
                                    <h4>{{ $team->name }}</h4>
                                    <p> {{ $team->designation }}</p>
                                    <ul>
                                        <li>Tel: {{ $helpers->company()->phone}}</li>
                                        <li style="text-align: justify;">Mail: {{ $helpers->company()->gmail}} </li>
                                    </ul>
                                </div>

                            </div>
                        @endforeach
                    @endif

                    {{--

                    <div class="member">
                        <figure>
                            <img src="{{ asset('public/front/img/member2.jpg')}}" alt="awsome image"/>
                            <div class="overlay">
                                <div class="inner-box">
                                    <ul class="social">
                                        <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i> </a></li>
                                        <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                                        <li><a href="#"><i class="fa fa-skype" aria-hidden="true"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </figure>
                        <div class="author-info">
                            <h4>Lisa Jones </h4>
                            <p>Adviser &amp; Business Developer</p>
                            <ul>
                                <li>Tel: 800-700-6202</li>
                                <li>Mail: simons@industrial.com</li>
                            </ul>
                        </div>
                    </div>

                    <div class="member">
                        <figure>
                            <img src="{{ asset('public/front/img/member3.jpg')}}" alt="awsome image"/>
                            <div class="overlay">
                                <div class="inner-box">
                                    <ul class="social">
                                        <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i> </a></li>
                                        <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                                        <li><a href="#"><i class="fa fa-skype" aria-hidden="true"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </figure>
                        <div class="author-info">
                            <h4>George Simons</h4>
                            <p>Cheif Engineer</p>
                            <ul>
                                <li>Tel: 800-700-6202</li>
                                <li>Mail: simons@industrial.com</li>
                            </ul>
                        </div>
                    </div>--}}


                </div>
            </div>
        </div>
    </div>
</section>
