@php($services = \App\About::query()->where('type', 4)->get())
        <!--why-people love hd-ex-->
        <div class="col-md-12">
            <div class="why-love p-4 text-center" id="whd">
                <h3 class="text-muted p-4">Why people <span class="text-danger"><i class="fas fa-heart"></i></span> HD EXPRESS</h3>
                <div class="container">
                    <div class="row">
                        @foreach($services as $service)
                        <div class="col-md-12 col-lg-4 col-sm-12 col-12">
                            <div class="op-padding">
                                <img class="img-fluid" src="{{asset('public/admin/service/'.$service->image)}}" alt="" style="max-height: 250px" width="100%">

                                <div class="img-header  w-l-1">
                                    <div class="container">
                                        <div class="h"><h2 class="text-dark">Convenient & Quick</h2></div>
                                        <div class="text"><p class="text-muted">No waiting in traffic, no haggling, no worries carrying groceries, they're delivered right at your door.</p></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>