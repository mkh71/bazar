
<div id="minimal-bootstrap-carousel" data-ride="carousel" class="carousel slide carousel-fade shop-slider slider-two">
    <!-- Wrapper for slides-->
    <div role="listbox" class="carousel-inner carousel-inner-two">

        @foreach($sliders as $slider)
            <div style="background-image: url({{ asset('public/slider/'.$slider->image)}});background-position: center right;" class="item {{$loop->iteration == 1 ? 'active' : ''}} slide-1">
                <div class="carousel-caption">
                    <div class="thm-container">
                        <div class="box valign-bottom">
                            <div class="content text-center">
                                <h2 data-animation="animated fadeInUp" style=" background : rgba(0,0,0,0.6)!important; padding:5px 10px!important;"><span>{{
                                $slider->title }}</span></h2>
                                <p data-animation="animated fadeInDown">{{ $slider->description }}</p>
                                <div class="buttons">
                                    <a data-animation="animated fadeInLeft" href="#" class="thm-button hvr-bounce-to-right">Learn more </a>
                                    <a data-animation="animated fadeInRight" href="#" class="thm-button inverse hvr-bounce-to-right2">View services</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach

        {{--

        <div style="background-image: url({{ asset('public/front/img/slide2two.jpg')}});background-position: center right;" class="item slide-2">
            <div class="carousel-caption">
                <div class="thm-container">
                    <div class="box valign-bottom">
                        <div class="content text-center">
                            <h2 data-animation="animated fadeInUp"><span>Leader</span> in Power<br>and Automotion</h2>
                            <p data-animation="animated fadeInDown">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam fringilla <br>mauris pellentesque, vestibulum quam vel, bibendum lectus. </p>
                            <div class="buttons">
                                <a data-animation="animated fadeInLeft" href="#" class="thm-button hvr-bounce-to-right">Learn more</a>
                                <a data-animation="animated fadeInRight" href="#" class="thm-button inverse hvr-bounce-to-right2">View services</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div style="background-image: url({{ asset('public/front/img/slide3tthree.jpg')}});background-position: center right;" class="item slide-3">
            <div class="carousel-caption">
                <div class="thm-container">
                    <div class="box valign-bottom">
                        <div class="content text-center pull-right">
                            <h2 class="gray" data-animation="animated fadeInUp"><span>Welcome</span> to all <br>Industrial Business</h2>
                            <p class="gray" data-animation="animated fadeInDown">You will be always aware of all business and financial news and stay informed with investment tips, market predictions, business advice and guides. </p>
                            <div class="buttons">
                                <a data-animation="animated fadeInLeft" href="#" class="thm-button hvr-bounce-to-right">Learn more </a>
                                <a data-animation="animated fadeInRight" href="#" class="thm-button inverse hvr-bounce-to-right2">View services</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        --}}

    </div>
    <!-- Controls--><a href="#minimal-bootstrap-carousel" role="button" data-slide="prev" class="left carousel-control"><i class="fa fa-angle-left"></i><span class="sr-only">Previous</span></a><a href="#minimal-bootstrap-carousel" role="button" data-slide="next" class="right carousel-control"><i class="fa fa-angle-right"></i><span class="sr-only">Next</span></a>
</div>
