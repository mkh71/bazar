
<section class="partners">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>Our partners</h2>
            </div>
            <div class="col-md-12">
                <div id="about-us-our-partners" class="owl-carousel owl-theme">
                    {{ dd("Hello") }}
                    @foreach($helpers->client() as $client)
                    <div class="item-partner">
                        <div class="image">
                            <img class="img-responsive" src="{{ asset('public/front/img/partner%20logos/logo1.png')}}" alt="Nutrition Photo">
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</section>
