
<section class="our-projects-two">
    <div class="container">
        <div class="row head">
            <div class="col-md-12">
                <div class="title">
                    <h2>Our Services</h2>
                </div>
            </div>
        </div>

    </div>
    <div class="full-width-slider">
        <div id="our-projects" class="owl-carousel owl-theme owl-responsive-700">
            <div class="item">
                <div class="image">
                    <img class="img-responsive" src="{{ asset('public/front/img/project-two1.jpg')}}" alt="Nutrition Photo">
                    <div class="overlay">
                        <div class="button"><a class="btn" href="#">SEE MORE</a></div>
                        <div class="text">

                            <h4>Power and Energy</h4>

                        </div>
                    </div>
                </div>
            </div>z
            <div class="item">
                <div class="image">
                    <img class="img-responsive" src="{{ asset('public/front/img/project-two2.jpg')}}" alt="Nutrition Photo">
                    <div class="overlay">
                        <div class="button"><a class="btn" href="#">SEE MORE</a></div>
                        <div class="text">
                            <h4>Power and Energy</h4>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="image">
                    <img class="img-responsive" src="{{ asset('public/front/img/project-two3.jpg')}}" alt="Nutrition Photo">
                    <div class="overlay">
                        <div class="button"><a class="btn" href="#">SEE MORE</a></div>
                        <div class="text">
                            <h4>Power and Energy</h4>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="image">
                    <img class="img-responsive" src="{{ asset('public/front/img/project-two4.jpg')}}" alt="Nutrition Photo">
                    <div class="overlay">
                        <div class="button"><a class="btn" href="#">SEE MORE</a></div>
                        <div class="text">
                            <h4>Power and Energy</h4>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="image">
                    <img class="img-responsive" src="{{ asset('public/front/img/project-two5.jpg')}}" alt="Nutrition Photo">
                    <div class="overlay">
                        <div class="button"><a class="btn" href="#">SEE MORE</a></div>
                        <div class="text">
                            <h4>Power and Energy</h4>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="image">
                    <img class="img-responsive" src="{{ asset('public/front/img/project-two5.jpg')}}" alt="Nutrition Photo">
                    <div class="overlay">
                        <div class="button"><a class="btn" href="#">SEE MORE</a></div>
                        <div class="text">
                            <h4>Power and Energy</h4>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="image">
                    <img class="img-responsive" src="{{ asset('public/front/img/project-two5.jpg')}}" alt="Nutrition Photo">
                    <div class="overlay">
                        <div class="button"><a class="btn" href="#">SEE MORE</a></div>
                        <div class="text">
                            <h4>Power and Energy</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
