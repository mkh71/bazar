<!--w3-collapse-navbar-->
<nav class="navbar navbar-expand-md bg-danger sticky-top text-center navbar-dark">
    <div class="container-fluid">
        <!-- Brand -->
        <ul class="navbar-nav">
            <li class="nav-item mx-auto">
                <a href="#" data-target=".sidebarb" class="text-light" data-toggle="collapse" ><i class="fas fa-bars fa-lg py-2 p-1"></i></a>
            </li>
        </ul>
        <a class="navbar-brand px-4" href="index.html">
            <span class="logo-name text-light"><img src="{{asset('public/admin/company/'.$helpers->company()->logo)}}" alt="" class="img-fluid logo"></span>
        </a>
        <form class="form-inline mx-auto" action="/action_page.php">
            <input class="form-control mr-sm-2" type="text" width="500px" placeholder="Search for products (e.g. eggs,milk,alu)">
            <button class="btn btn-danger search-btn" type="submit"><span><i class="fas fa-search"></i></span></button>
        </form>
        <!-- Toggler/collapsibe Button -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
            <span><i class="fas fa-ellipsis-v"></i></span>
        </button>

        <!-- Navbar links -->
        <div class="collapse navbar-collapse" id="collapsibleNavbar">

            <ul class="navbar-nav">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-4">
                            <li class="nav-item mx-auto p-2 active">
                                <a class="btn btn-warning p-2 nav-link "href="#">NEED HELP <span><i class="fas fa-question"></i></span></a>
                            </li>
                        </div>
                        <div class="col-xs-4">
                            <li class="nav-item mx-auto p-2 active">
                                <a class="nav-link p-2 btn btn-info" type="button" data-toggle="modal" data-target="#signIn">SIGN IN / SIGN UP</a>
                            </li>
                        </div>
                    </div>
                </div>
            </ul>
        </div>
    </div>
</nav>


<!--Sign In Modal-->
<!-- The Modal -->
<div class="modal fade" id="signIn">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Sign In</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body text-center">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-11">
                            <a href="#" class="btn btn-block m-2 btn-primary"><span
                                        class="text-light"><i class="fab fa-facebook"></i></span>&nbsp;
                                SIgn In With Facebook</a>
                        </div>
                        <div class="col-sm-11">
                            <a href="#" class="btn btn-block m-2 btn-danger"><span
                                        class="text-light"><i class="fas fa-envelope"></i></span>&nbsp;
                                SIgn In With Gmail</a>

                        </div>

                        <div class="col-sm-12">
                            <form action="{{ route('login') }}" method="post" class=" p-4 needs-validation"
                                  novalidate>
                                @csrf
                                <div class="form-row">
                                    <div class="col-md-11 m-2">
                                        <input type="tel" class="form-control" id="mobile"
                                               placeholder="Enter Your Phone Number e.g +880123456789"
                                               name="email" pattern="[0-9]{11}" required>
                                        <div class="valid-feedback">Valid.</div>
                                        <div class="invalid-feedback">Please fill out this field.
                                        </div>
                                    </div>
                                    <div class="col-md-11 m-2">
                                        <input type="password" class="form-control" id="pwd"
                                               placeholder="Enter password" name="password" required>
                                        <div class="valid-feedback">Valid.</div>
                                        <div class="invalid-feedback">Please fill out this field.
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-11 m-2">
                                    <button
                                        type="submit"
                                        class="nav-link p-2 btn btn-info"
                                        >
                                        SIGN IN
                                    </button>
                                </div>
                                <div class="col-md-11 m-2">
                                    <a type="submit" class="btn btn-lg text-light m-2 btn-danger"
                                       href="register.blade.php">Register</a>
                                </div>


                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>

        </div>
    </div>
</div>
<!--Sign In Modal-->
