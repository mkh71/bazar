
<section class="about-us">
    <div class="container">
        <div class="row">
            <div class="title">
                <h2>About Us &amp; Faq</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 about">
                <div class="clearfix"></div>
                <h5>We are professional {{ $helpers->company()->name }} company</h5>
                <p> {!! $helpers->company()->about !!} </p>

                <div class="button">
                    <a class="hvr-bounce-to-right btn" href="#">SEE MORE</a>
                </div>
            </div>
            <div class="col-md-6">

                <div class="questions">

                    <div id="accordion" role="tablist">

                        @foreach($helpers->faqs() as $faq)
                            <div class="toggle">
                                <div class="toggle-heading" role="tab">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne"> <i class="fa fa-plus"></i> {{ $faq->title }}<i class="fa fa-minus"></i></a>
                                </div>
                                <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-expanded="false">
                                    <div class="toggle-body">
                                        <p>
                                           {!!  $faq->description  !!}
                                        </p>
                                    </div>
                                </div>
                            </div>
                        @endforeach

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
