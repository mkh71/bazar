@extends('finalfront.layouts.master')
@section('title',"404 | MTR Engineering Co.")
@section('content')
    <div class="error-404">
        <div class="box">
            <div class="content">
                <h3>404</h3>
                <p>Page is not found!</p>
                <br>
                <hr>
                <a href="{{ route("/") }}" class="btn btn-primary text-center" style="padding: 10px;font-weight: bold;text-transform: uppercase;font-size: 2rem;letter-spacing: 2px;width: 50%">Back to Home</a>
            </div><!-- /.content -->
        </div><!-- /.box -->
    </div><!-- /.error-404 -->
@endsection