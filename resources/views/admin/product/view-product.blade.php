@extends('admin.layouts.admin')
@section('title','PRODUCTS ')
@section('content')
    <div class="boxed">
        <div id="content-container">
            <div id="page-head">
                <div id="page-title">
                    <h1 class="page-header text-overflow"> View Product</h1>
                </div>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="demo-pli-home"></i></a></li>
                    <li><a href="#">Admin</a></li>
                    <li class="active">Product</li>
                </ol>
            </div>
            <div id="page-content">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel">
                            <div class="panel-heading">
                                <h3 class="panel-title"> Product</h3>
                            </div>
                            <div class="panel-body">
                                <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 table-responsive">
                                    <table class="table table-bordered table-striped" id="CategoryTable">
                                        <thead>
                                        <tr>
                                            <th>SL</th>
                                            <th>Name</th>
                                            <th>Code</th>
                                            <th>Price</th>
                                            <th>Category</th>
                                            <th>Sub Category</th>
                                            <th>Description</th>
                                            <th>Image</th>
                                            <th>Action</th>

                                        </tr>
                                        </thead>
                                        <tbody>
                                        @php $i=0; @endphp

                                       @foreach($products as $info)

                                        <tr>
                                            <td>{{++$i}}</td>
                                            <td>{{ $info->name}}</td>
                                            <td>{{ $info->code !=null ?  $info->code : "" }}</td>
                                            <td>{{ $info->price }}</td>
                                            <td>{{ $info->category->name}}</td>
                                            <td>{{ $info->subcategory !=null ? $info->subcategory->name : ''}}</td>
                                            <td>{!!  $info->description !!}</td>
                                            <td>

                                               @foreach($info->images as $image)
                                                    <img src="{{url('public/admin/product/'.$image->image)}}" alt="no img" height="50px" width="50px">
                                               @endforeach

                                            </td>
                                            <td>
                                                <a href="{{route('product.edit',$info->id)}}" class="btn btn-primary"><i class="fa fa-edit"></i></a>
                                                {{--<button type="button" data-id="{{ $info->id }}" data-url="{{ URL('ProductManagement/Product/erase') }}" class="btn btn-danger erase delete"><i class="fa fa-trash"></i></button>--}}
                                            </td>
                                        </tr>
                                       @endforeach

                                        </tbody>
                                    </table>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>

        $(function(){
            $('#CategoryTable').DataTable();

        });

        $(document).on('click','.activestatus',function(){
            var id = $(this).attr('data-id');
            $('#product_status').modal('show');
            $("#status_id").val(id);
            console.log("Fire Console : "+$(this).attr('data-id'));
        });

        /* UPDATE Category END */

    </script>

    <div id="product_status" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 id="modalTitle" class="modal-title"> Product Status Update </h4>
                </div>
                <div class="modal-body">
                    {{Form::open(['route'=>'product.statusupdate','method'=>'post'])}}
                    {{csrf_field()}}
                    <input type="hidden" name="id" id="status_id" >
                    <div class="panel-body">
                        <div class="form-group ">
                            @php $p =1; @endphp
                            {{Form::label('status','Product Status : ',['class'=>'label-control'])}}
                            {{Form::select('isactive',[
                                                        ''=>'SELECT PRODUCT STATUS',
                                                        '1'=>'ACTIVE',
                                                        '0'=>'IN-ACTIVE'
                                                        ],null,['class'=>'form-control','id'=>'di'])}}
                        </div>
                        <div class="panel-footer text-right">
                            <button class="btn btn-success" type="submit">UPDATE STATUS</button>
                        </div>
                        {{ Form::close() }}
                    </div>

                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->


@endsection
