@extends('admin.layouts.admin')
@section('title','EDIT-PRODUCT | HD Exp')
@section('content')
    <div class="boxed">
        <div id="content-container">
            <div id="page-head">
                <div id="page-title">
                    <h1 class="page-header text-overflow">Add / View Product</h1>
                </div>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="demo-pli-home"></i></a></li>
                    <li><a href="#">Admin</a></li>
                    <li class="active">Product</li>
                </ol>
            </div>
            <div id="page-content">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel">
                            <div class="panel-heading">
                                <h3 class="panel-title">Add New Product</h3>
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    {{ Form::model($product,['route'=>'product.update','method'=>'post','files'=>true]) }}
                                    {{-- row 1--}}
                                    <div class="row">
                                        <div class="col-lg-2 col-sm-2 col-xs-12 {{$errors->has('code') ? 'has-error' : ''}}">
                                            <span style="display: block;height: 10px;width: 100%;background: #fff;"></span>
                                            {{ Form::label('product','Product Code : ',['class'=>'control-label'])}}
                                            {{ Form::hidden('id',$product->id) }}
                                            {{ Form::text('code',old('code'),['class'=>'form-control','id'=>'prod_code','placeholder'=>'Ex: G102'])}}
                                            @if ($errors->has('code'))
                                                <span class="help-block">
                                                     <strong>{{ $errors->first('code') }}</strong>
                                                </span>
                                            @endif
                                        </div>

                                        <!--  PRODUCT NAME  -->
                                        <div class="col-lg-4 col-sm-4 col-xs-12 {{$errors->has('name') ? 'has-error' : ''}}">
                                            <span style="display: block;height: 10px;width: 100%;background: #fff;"></span>

                                            {{ Form::label('product','Product Name : ',['class'=>'control-label'])}}
                                            {{ Form::text('name',old('name'),['class'=>'form-control','placeholder'=>'Ex: Ring'])}}
                                            @if ($errors->has('name'))
                                                <span class="help-block">
                                                 <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                        <!-- / PRODUCT NAME  -->



                                        <!--  PRODUCT LOCAL NAME  -->
                                        <div class="col-lg-3 col-sm-3 col-xs-12 {{$errors->has('price') ? 'has-error' : ''}}">
                                            <span style="display: block;height: 10px;width: 100%;background: #fff;"></span>

                                            {{ Form::label('product','Price : ',['class'=>'control-label'])}}
                                            {{Form::text('price',old('price'),['class'=>'form-control','placeholder'=>'Ex: 5000 tk'])}}
                                            @if ($errors->has('price'))
                                                <span class="help-block">
                                                 <strong>{{ $errors->first('price') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                        <!-- / PRODUCT LOCAL NAME  -->
                                    </div>

                                    <div class="row">


                                        <!--  PRODUCT Brand NAME  -->
                                        <div class="col-lg-3 col-sm-3 col-md-3 col-xs-12 {{$errors->has('brand_id') ? 'has-error' : ''}}">
                                            <span style="display: block;height: 10px;width: 100%;background: #fff;"></span>

                                            {{ Form::label('brand','Brand : ',['class'=>'control-label'])}}

                                            {{ Form::select('brand_id', $brand,null,['class'=>'form-control']) }}
                                            @if($errors->has('brand_id'))
                                                <span class="help-block">
                                                <strong>{{$errors->first('brand_id')}}</strong>
                                            </span>
                                            @endif

                                        </div>
                                        <!-- / PRODUCT Brand NAME  -->

                                        <!-- categorie_id Select Start -->
                                        <div class="col-lg-3 col-sm-3 col-xs-12 {{$errors->has('category_id') ? 'has-error' : ''}}">
                                            <span style="display: block;height: 10px;width: 100%;background: #fff;"></span>
                                            {{ Form::label('Category','Category : ',['class'=>'control-label'])}}

                                            {{ Form::select('category_id', $category,true,['class'=>'form-control' ,'id'=>'category_id']) }}
                                            @if($errors->has('category_id'))
                                                <span class="help-block">
                                                <strong>{{$errors->first('category_id')}}</strong>
                                            </span>
                                            @endif

                                        </div>
                                        <!-- categorie_id Select End -->
                                    </div>
                                    <div class="row">


                                        <!-- sub_categorie_id Select Start -->
                                        <div class="col-lg-3 col-sm-3 col-xs-12 {{$errors->has('subcategory_id') ? 'has-error' : ''}}">
                                            <span style="display: block;height: 10px;width: 100%;background: #fff;"></span>
                                            {{ Form::label('Category','Sub-Category : ',['class'=>'control-label'])}}

                                            {{ Form::select('subcategory_id',[''],true,['class'=>'form-control','id'=>'sub_category_id']) }}
                                            @if($errors->has('sub_categorie_id'))
                                                <span class="help-block">
                                                <strong>{{$errors->first('subcategory_id')}}</strong>
                                            </span>
                                            @endif

                                        </div>

                                        <!-- sub_categorie_id Select End -->

                                        <!--  PRODUCT File NAME  -->
                                        <div class="col-lg-3 col-sm-3 col-xs-12 {{$errors->has('image') ? 'has-error' : ''}}">
                                            <span style="display: block;height: 10px;width: 100%;background: #fff;"></span>
                                            {{ Form::label('image','Product Image : ',['class'=>'control-label'])}}

                                            {{ Form::file('image[]', ['class'=>'form-control','multiple']) }}

                                            @if($errors->has('image'))
                                                <span class="help-block">
                                                <strong>{{$errors->first('image')}}</strong>
                                            </span>
                                            @endif
                                        </div>
                                        <!-- / PRODUCT File NAME  -->

                                    </div>
                                    <!--  PRODUCT details NAME  -->
                                    <div class="col-lg-12 col-sm-12 col-xs-12 {{$errors->has('description') ? 'has-error' : ''}}">
                                        <span style="display: block;height: 10px;width: 100%;background: #fff;"></span>
                                        {{ Form::label('image','Details : ',['class'=>'control-label'])}}

                                        {!! Form::textarea('description',old('description'),['class'=>'form-control', 'rows' => 6, 'cols' => 40,'placeholder'=>'Product Details.......','id'=>'demo-summernote']) !!}

                                        @if($errors->has('description'))
                                            <span class="help-block">
                                                <strong>{{$errors->first('description')}}</strong>
                                            </span>
                                        @endif

                                    </div>
                                    <div class="col-lg-6 col-sm-6 col-xs-12 {{$errors->has('image') ? 'has-error' : ''}}">
                                        {{ Form::label('old',"Previews Image") }}
                                        <table class="table-dark">
                                            <thead>
                                                <th>Image</th>
                                                <th>Check for Delete</th>
                                            </thead>
                                            <tbody>
                                            @foreach($product->images as $image)
                                                <tr>
                                                    <td>
                                                        <img src="{{ asset('public/admin/product/'.$image->image) }}" style="height: 80px;width: 80px;">
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" value="{{ $image->id }}" name="pro_image_delete[]" class="form-control">
                                                    </td>
                                                </tr>

                                            @endforeach
                                            </tbody>
                                        </table>

                                    </div>
                                    <!-- / PRODUCT details NAME  -->

                                    <div class="col-lg-12 col-sm-12 col-xs-12"><span style="display: block;height: 10px;width: 100%;background: #fff;"></span></div>

                                    <div class="col-lg-12 col-sm-12 col-xs-12">
                                        {{ Form::button('UPDATE PRODUCT',['type'=>'submit','id'=>'savekarat','class'=>'btn btn-primary']) }}
                                    </div>
                                    {{ Form::close() }}
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function(){
            var code ='';
            var key = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            for (var i = 0; i < 3; i++)
                code += key.charAt(Math.floor(Math.random() * key.length));
            $("#prod_code").val("P"+code);


            /* all select 2*/

            $(".origin_select2").select2({tags:true});
            $(".size_select2").select2();
            $(".color_select2").select2();
            subcatload();

        });

        /* select Category and load sub-category automatically start */
        $(document).on('change','#category_id',function(){
            var category_id = $(this).val();
            $.ajax({
                method:"post",
                url:"{{ route('purchase.subcategory') }}",
                data:{category_id:category_id,"_token":"{{ csrf_token() }}"},
                dataType:"html",
                success:function(response){
                    $("#sub_category_id").html(response);
                },
                error:function(err){
                    console.log(err);
                }
            });
        });

        /* select category and load sub-category automatically end */

        function subcatload(){
            var category_id = $("#category_id").val();
            $.ajax({
                method:"post",
                url:"{{ route('purchase.subcategory') }}",
                data:{category_id:category_id,"_token":"{{ csrf_token() }}"},
                dataType:"html",
                success:function(response){
                    $("#sub_category_id").html(response);
                },
                error:function(err){
                    console.log(err);
                }
            });
        }

    </script>
@endsection
