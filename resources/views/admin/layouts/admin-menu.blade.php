<!--MAIN NAVIGATION-->
<!--===================================================-->
<nav id="mainnav-container">
    <div id="mainnav">
        <!--================================-->
        <div id="mainnav-menu-wrap">
            <div class="nano">
                <div class="nano-content">
                    <!--Shortcut buttons-->
                    <!--================================-->
                    <div id="mainnav-shortcut" class="hidden">
                        <ul class="list-unstyled shortcut-wrap">
                            <li class="col-xs-3" data-content="My Profile">
                                <a class="shortcut-grid" href="#">
                                    <div class="icon-wrap icon-wrap-sm icon-circle bg-mint">
                                        <i class="demo-pli-male"></i>
                                    </div>
                                </a>
                            </li>
                            <li class="col-xs-3" data-content="Messages">
                                <a class="shortcut-grid" href="#">
                                    <div class="icon-wrap icon-wrap-sm icon-circle bg-warning">
                                        <i class="demo-pli-speech-bubble-3"></i>
                                    </div>
                                </a>
                            </li>
                            <li class="col-xs-3" data-content="Activity">
                                <a class="shortcut-grid" href="#">
                                    <div class="icon-wrap icon-wrap-sm icon-circle bg-success">
                                        <i class="demo-pli-thunder"></i>
                                    </div>
                                </a>
                            </li>
                            <li class="col-xs-3" data-content="Lock Screen">
                                <a class="shortcut-grid" href="#">
                                    <div class="icon-wrap icon-wrap-sm icon-circle bg-purple">
                                        <i class="demo-pli-lock-2"></i>
                                    </div>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <!--================================-->
                    <!--End shortcut buttons-->
                    @php
                         $prefix=Request::route()->getPrefix();
                         $route=Route::current()->getName();
                    @endphp

                    <ul id="mainnav-menu" class="list-group">
                        <!--Category name-->
                        <li class="list-header">Navigation</li>

                        <!--Menu list item-->
                        <li class="{{($route=='home')? 'active-sub active':''}}">
                            <a href="{{url('/home')}}"><i class="demo-pli-home"></i>
                                <span class="menu-title">Dashboard</span>
                            </a>
                        </li>

                        <li  class="{{($prefix=='/UserManagement')? 'active-sub active':''}}">
                            <a href="#">
                                <i class="fa fa-users"></i>
                                <span class="menu-title">User Management</span>
                                <i class="arrow"></i>
                            </a>
                            <!--Submenu-->
                            <ul class="collapse">
                                <li class="">
                                    <a href="{{route('user.create')}}"><i class="fa fa-user-circle"></i>Users</a>
                                </li>
                                {{--<li class="">--}}
                                {{--<a href="{{route('role.create')}}"><i class="fa fa-user-circle"></i>Roles</a>--}}
                                {{--</li>--}}
                                <li class="">
                                    <a href="{{route('role.view')}}"><i class="fa fa-user-circle"></i>Roles</a>
                                </li>
                            </ul>
                        </li>
                        <!-- User Management END -->


                        <!-- Class Menu Add -->

                        <li  class="{{($prefix=='/ProductManagement')? 'active-sub active':''}}">
                            <a href="#">
                                <i class="fa fa-apple"></i>
                                <span class="menu-title">Product Management</span>
                                <i class="arrow"></i>
                            </a>
                            <!--Submenu-->
                            <ul class="collapse">

                                <li class="">
                                    <a href="{{route('brand.add')}}"><i class="ion-merge"></i> Add / View Brand</a>
                                    <a href="{{route('category.add')}}"><i class="ion-merge"></i> Add / View Category</a>
                                </li>

                                <li class="">
                                    <a href="{{route('sub-category.add')}}"><i class="ion-merge"></i> Add / View SubCategory</a>
                                </li>
{{--
                                <li class="">
                                    <a href="{{route('size.add')}}"><i class="ion-merge"></i> Add / View Size</a>
                                </li>--}}

                                <li class="">
                                    <a href="{{route('product.add')}}"><i class="ion-merge"></i> Add New Product</a>
                                </li>

                                <li class="">
                                    <a href="{{route('product.view')}}"><i class="ion-merge"></i> View Products</a>
                                </li>

                            </ul>
                        </li>

                        <li  class="{{($prefix=='/Offer')? 'active-sub active':''}}">
                            <a href="#">
                                <i class="fa fa-globe"></i>
                                <span class="menu-title">Offer Management</span>
                                <i class="arrow"></i>
                            </a>
                            <!--Submenu-->
                            <ul class="collapse">

                                <li class="">
                                    <a href="{{route('offer.index')}}"><i class="ion-merge"></i> View / Add Offer</a>
                                    <a href="{{route('offer.manage')}}"><i class="ion-merge"></i>Add / View Product Offer</a>
                                    <a href="{{route('coupon')}}"><i class="ion-merge"></i> View / Add Coupon</a>
                                    <a href="{{route('service.add')}}"><i class="ion-merge"></i> Manage  New Service</a>
                                </li>

                            </ul>
                        </li>


                        <li  class="{{($prefix=='/WebManagement')? 'active-sub active':''}}">
                            <a href="#">
                                <i class="fa fa-globe"></i>
                                <span class="menu-title">Web-site Management</span>
                                <i class="arrow"></i>
                            </a>
                            <!--Submenu-->
                            <ul class="collapse">

                                <li class="">
                                    <a href="{{route('about.add')}}"><i class="ion-merge"></i> Manage  About/Mission/Vision</a>
                                    <a href="{{route('service.add')}}"><i class="ion-merge"></i> Manage  New Service</a>
                                </li>

                            </ul>
                        </li>


                        <li  class="{{($prefix=='/SliderManagement')? 'active-sub active':''}}">
                            <a href="#">
                                <i class="fa fa-image"></i>
                                <span class="menu-title">Slider Management</span>
                                <i class="arrow"></i>
                            </a>
                            <!--Submenu-->
                            <ul class="collapse">
                                <li class="">
                                    <a href="{{route('slider.add')}}"><i class="ion-merge"></i>Add / View Slider</a>
                                </li>
                            </ul>
                        </li>


                        <li  class="{{($prefix=='/Company')? 'active-sub active':''}}">
                            <a href="#">
                                <i class="fa fa-image"></i>
                                <span class="menu-title">Company Settings</span>
                                <i class="arrow"></i>
                            </a>
                            <!--Submenu-->
                            <ul class="collapse">
                                <li class="">
                                    <a href="{{route('company.edit',1)}}"><i class="ion-merge"></i>Update Company</a>
                                </li>

                                <li class="">
                                    <a href="{{route('faq.add')}}"><i class="ion-merge"></i>Manage Faq</a>
                                </li>
                            </ul>
                        </li>

                        <li  class="{{($route=='title.add') ? 'active-sub active':''}}">
                            <a href="{{  route('title.add') }}">
                                <i class="fa fa-globe"></i>
                                <span class="menu-title">Page Title</span>
                            </a>
                        </li>

                        <li  class="{{($prefix=='/CustomerRequest')? 'active-sub active':''}}">
                            <a href="#">
                                <i class="fa fa-globe"></i>
                                <span class="menu-title">Customer Messages</span>
                                <i class="arrow"></i>
                            </a>
                            <!--Submenu-->
                            <ul class="collapse">

                                <li class="">
                                    <a href="{{route('customer.message')}}"><i class="ion-merge"></i> View Messages</a>
                                </li>
                            </ul>
                        </li>
                    </ul>

                </div>
            </div>
        </div>
        <!--================================-->
        <!--End menu-->

    </div>
</nav>
<!--===================================================-->
<!--END MAIN NAVIGATION-->
