@extends('admin.layouts.admin')
@section('title','Add faq')
@section('content')
    <div class="boxed">
        <div id="content-container">
            <div id="page-head">
                <div id="page-title">
                    <h1 class="page-header text-overflow">New faq</h1>
                </div>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="demo-pli-home"></i></a></li>
                    <li><a href="#">Admin</a></li>
                    <li class="active">faq</li>
                </ol>
            </div>
            <div id="page-content">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel">
                            <div class="panel-heading">
                                <h3 class="panel-title">Update faq</h3>
                            </div>
                            <div class="panel-body">
                                <div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
                                    <div class="col-lg-12 col-sm-12 {{$errors->has('name') ? 'has-error' : ''}}">
                                        {{ Form::model($faq,['route'=>['faq.update',$faq->id],'method'=>'post','enctype'=>'multipart/form-data','id'=>'faqForm']) }}
                                        {{ Form::label('faq','faq Name : ',['class'=>'control-label'])}}
                                        {{Form::text('title',old('title'),['class'=>'form-control','placeholder'=>'Ex: Policy'])}}
                                        @if ($errors->has('title'))
                                            <span class="help-block">
                                                 <strong>{{ $errors->first('title') }}</strong>
                                            </span>
                                        @endif
                                        <br>
                                    </div>

                                    <div class="col-lg-12 col-sm-12 {{$errors->has('description') ? 'has-error' : ''}}">
                                        {{ Form::label('','Description : ',['class'=>'control-label'])}}
                                        {{ Form::textarea('description',null,['class'=>'form-control',"id"=>"demo-summernote"])}}
                                        @if ($errors->has('description'))
                                            <span class="help-block">
                                                 <strong>{{ $errors->first('description') }}</strong>
                                            </span>
                                        @endif
                                        <br>
                                    </div>


                                    <div class="col-md-12 col-xs-12">
                                        <br>
                                    </div>

                                    <div class="col-md-12 col-xs-12">
                                        {{ Form::button('Update Faq',['type'=>'submit','id'=>'savefaq','class'=>' btn btn-primary']) }}
                                    </div>
                                    {{ Form::close() }}
                                </div>

                                <div class="col-lg-8 col-sm-8 col-md-8 col-xs-12">
                                    <table class="table table-bordered table-striped" id="faqTable">
                                        <thead>
                                        <tr>
                                            <th>SL</th>
                                            <th>Faq</th>
                                            <th>Details</th>
                                            <th>Action </th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($faqs as $info)
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td>{{ $info->title}}</td>
                                                <td> {!!
$info->description !=null ? substr($info->description,0,150) : "no description "  !!}
                                                </td>
                                                <td>
                                                    <a href="{{route('faq.edit',$info->id)}}" class="btn btn-sm btn-info edit" ><i class="demo-pli-pen-5"></i></a> ||
                                                    <button class="btn btn-sm btn-danger erase" data-id="{{$info->id}}" data-url="{{ route('faq.destroy')}}"><i class="demo-pli-trash"></i></button>
                                                </td>
                                            </tr>
                                        @endforeach

                                        </tbody>
                                    </table>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>

        $(function(){
            $('#faqTable').DataTable();

        });

        /* UPDATE faq END */

    </script>



@endsection
