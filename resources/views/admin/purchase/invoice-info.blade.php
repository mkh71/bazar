<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Dashboard</title>
    <!--STYLESHEET-->
    <!--=================================================-->

    <!--Open Sans Font [ OPTIONAL ]-->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700' rel='stylesheet' type='text/css'>

    <!--Bootstrap Stylesheet [ REQUIRED ]-->
    <link href="{{url('public/admin/css/bootstrap.min.css')}}" rel="stylesheet">

    <!--Nifty Stylesheet [ REQUIRED ]-->
    <link href="{{url('public/admin/css/nifty.min.css')}}" rel="stylesheet">



    <!--Nifty Premium Icon [ DEMONSTRATION ]-->
    <link href="{{url('public/admin/css/demo/nifty-demo-icons.min.css')}}" rel="stylesheet">

    <!--Ion Icons [ OPTIONAL ]-->
    <link href="{{url('public/admin/plugins/ionicons/css/ionicons.min.css')}}" rel="stylesheet">


    <!--Font Awesome [ OPTIONAL ]-->
    <link href="{{url('public/admin/plugins/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">

    <!-- Select 2 js Start  -->

    <link rel="stylesheet" type="text/css" href="{{ url('public/admin/css/select2.css') }}">


    <!--Pace - Page Load Progress Par [OPTIONAL]-->
    <link href="{{url('public/admin/plugins/pace/pace.min.css')}}" rel="stylesheet">
    <script src="{{url('public/admin/plugins/pace/pace.min.js')}}"></script>

    <!--Demo [ DEMONSTRATION ]-->
    <link href="{{url('public/admin/css/demo/nifty-demo.min.css')}}" rel="stylesheet">
    <!--DataTable-->
    <link rel="stylesheet" href="{{url('public/admin/plugins/datatables/css/dataTables.bootstrap.min.css')}}">
    <!--Sweet Alert-->
    <link rel="stylesheet" href="{{url('public/admin/css/sweetalert.css')}}">

    <!--Bootstrap Datepicker [ OPTIONAL ]-->
    <link href="{{url('public/admin/plugins/bootstrap-datepicker/bootstrap-datepicker.min.css')}}" rel="stylesheet">

    <!--jQuery [ REQUIRED ]-->
    <script src="{{url('public/admin/js/jquery.min.js')}}"></script>

    <!--Notify Js for operation alert-->
    <script src="{{url('public/admin/js/notify.min.js')}}"></script>



    <script src="{{url('public/admin/js/bootstrap.min.js')}}"></script>

    <!-- Select 2 js start  -->

    <script type="text/javascript" src="{{url('public/admin/js/select2.js')}}"></script>

    <!-- Select 2 js End -->

    <!--NiftyJS [ RECOMMENDED ]-->
    <script src="{{url('public/admin/js/nifty.min.js')}}"></script>

    <!--Sweet Alert-->
    <script src="{{url('public/admin/js/sweetalert.min.js')}}"></script>

    <!--Sparkline [ OPTIONAL ]-->
    <script src="{{url('public/admin/plugins/sparkline/jquery.sparkline.min.js')}}"></script>

    <!--DataTable-->
    <script src="{{url('public/admin/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js')}}"></script>
    <script src="{{url('public/admin/plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{url('public/admin/plugins/datatables/js/dataTables.bootstrap.min.js')}}"></script>

    <script src="{{url('public/admin/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js')}}"></script>

    <!--JAVASCRIPT-->
    <!--=================================================-->
    <script>
        $(function(){
            $('.date').datepicker({autoclose:true});
        });
        /** CHECK EVERY ACTION DELETE CONFIRMATION BY SWEET ALERT **/
        $(document).on('click', '.erase', function () {
            var id = $(this).attr('data-id');
            var url=$(this).attr('data-url');
            console.log("Clicked ID : "+id+ " Request URL : "+url);
            var token = '{{csrf_token()}}';
            var $tr = $(this).closest('tr');
            swal({
                    title: "Are you sure?",
                    text: "You will not be able to recover this information!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: 'btn-danger',
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: "No, cancel plz!",
                    closeOnConfirm: false,
                    closeOnCancel: false
                },
                function (isConfirm) {
                    if (isConfirm) {

                        $.ajax({
                            url: url,
                            type: "post",
                            data: {id: id, _token: token},
                            dateType:'html',
                            success: function (response) {
                                swal("Deleted!", "Data has been Deleted.", "success"),
                                    swal({
                                            title: "Deleted!",
                                            text: "Data has been Deleted.",
                                            type: "success"
                                        },
                                        function (isConfirm) {
                                            if (isConfirm) {
                                                $tr.find('td').fadeOut(1000, function () {
                                                    $tr.remove();
                                                });
                                            }
                                        });
                            }
                        });
                    } else {
                        swal("Cancelled", "Your data is safe :)", "error");
                    }
                });

        });
    </script>
    <!--BootstrapJS [ RECOMMENDED ]-->

    <meta name="csrf-token" content="{{ csrf_token() }}">


</head>
<body>
<div id="container" class="effect aside-float aside-bright mainnav-lg">



{{--Mid content--}}
    <div class="boxed">
        <div>
            <div id="page-head">

            </div>
            <div id="page-content">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel">
                            <div class="panel-body">
                                <div class="invoice-masthead">
                                    <div class="invoice-text">
                                        <h3 class="h1 text-uppercase text-thin mar-no text-primary">INVOICE</h3>
                                    </div>
                                    <div class="invoice-brand" style="white-space:nowrap">
                                        <div class="invoice-logo">
                                            <img src=" {{ url('public\admin\img\logo.png') }}">
                                        </div>
                                    </div>
                                </div>

                                <div class="invoice-bill row">
                                    <div class="col-sm-6 text-xs-center">
                                        <address>
                                            <strong class="text-main"> {{ $invoice->supplier->name }} </strong><br>
                                            {{ $invoice->supplier->phone }}<br>
                                            {{ $invoice->supplier->address }}<br>
                                        </address>
                                    </div>
                                    <div class="col-sm-6 text-xs-center">
                                        <table class="invoice-details">
                                            <tbody>
                                            <tr>
                                                <td class="text-main text-bold">Invoice #</td>
                                                <td class="text-right text-info text-bold"> {{ $invoice->invoice_no }}</td>
                                            </tr>
                                            <tr>
                                                <td class="text-main text-bold">Order Status</td>
                                                @php
                                                        @endphp

                                                <td class="text-right">
                                                    @if($invoice->order_status == 0)
                                                        <span class="badge badge-danger">In-Complete</span>
                                                    @else
                                                        <span class="badge badge-success">Complete</span>
                                                    @endif
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="text-main text-bold">Billing Date</td>
                                                <td class="text-right">Jun 11, 2016</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                                <hr class="new-section-sm bord-no">

                                <div class="row">
                                    <div class="col-lg-12 table-responsive">
                                        <table class="table table-bordered invoice-summary">
                                            <thead>
                                            <tr class="bg-trans-dark">
                                                <th class="text-uppercase">Product Name</th>

                                                <th class="min-col text-center text-uppercase">Qty</th>
                                                <th class="min-col text-center text-uppercase">Price</th>

                                                <th class="min-col text-center text-uppercase">Size</th>
                                                <th class="min-col text-center text-uppercase">Origin</th>
                                                <th class="min-col text-right text-uppercase">Karat</th>
                                                <th class="min-col text-center text-uppercase">Color</th>
                                                <th class="min-col text-center text-uppercase">Type</th>
                                                <th class="min-col text-right text-uppercase">Weight</th>
                                                <th class="min-col text-right text-uppercase">W.Amount</th>
                                                <th class="min-col text-right text-uppercase">Total</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @php
                                                $total = 0;
                                                $subtotal = 0;
                                            @endphp
                                            @foreach($invoice->Purchases as $purchase_info)
                                                @php
                                                    $total+= $purchase_info->total_price;
                                                @endphp

                                                <tr>
                                                    <td>
                                                        <strong> {{ $purchase_info->Product->product_name }} </strong>
                                                    </td>
                                                    <td class="text-center">{{$purchase_info->product_qty}} </td>
                                                    <td class="text-center"> {{$purchase_info->product_price }} tk</td>
                                                    <td class="text-center"> {{$purchase_info->size->name }} </td>
                                                    <td class="text-center"> {{$purchase_info->origin->name }} </td>
                                                    <td class="text-center"> {{$purchase_info->karat->karat_size }} </td>
                                                    <td class="text-center"> {{$purchase_info->color->name }} </td>
                                                    <td class="text-center"> {{$purchase_info->gold_type->name }} </td>
                                                    <td class="text-center"> {{$purchase_info->weight->name }} </td>
                                                    <td class="text-center"> {{$purchase_info->weight_amount }} </td>
                                                    <td class="text-right">{{ $purchase_info->total_price }} tk</td>
                                                </tr>
                                            @endforeach

                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                                <div class="clearfix">
                                    <table class="table invoice-total">
                                        <tbody>
                                        <tr>
                                            <td><strong>TOTAL AMOUNT  :</strong></td>
                                            <td><b> {{ $total }} </b></td>
                                        </tr>
                                        <tr>
                                            <td><strong> DISCOUNT : </strong></td>
                                            <td><b> {{ $invoice->total_discount }} </b></td>
                                        </tr>
                                        <tr>
                                            @php $total=0; $after_discount = $purchase_info->total_price-$invoice->total_discount;  @endphp
                                            @foreach($invoice->cash_books as $CashBook)
                                                @php $total +=$CashBook->expense @endphp

                                           @endforeach
                                            <td><strong> BALANCE/DUE : </strong></td>
                                            <td class="text-bold h4"> <b> {{ $after_discount-$total }} </b></td>

                                        </tr>
                                        <tr>
                                            <td><strong>AFTER DISCOUNT :</strong></td>
                                            <td class="text-bold h4"> <b> {{ $total-$invoice->total_discount }} </b></td>
                                        </tr>

                                        <tr>
                                            <td><strong>TOTAL PAID :</strong></td>
                                            <td class="text-bold h4"><b> {{ $total }} </b></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>

                                <div class="text-right no-print">
                                    <a href="javascript:window.print()" class="btn btn-default"><i class="demo-pli-printer icon-lg"></i></a>
                                    <button type="button" class="btn btn-primary confirm_payment" data-id="{{ $invoice->id }}">Confirm Payment</button>
                                </div>

                                <hr class="new-section-sm bord-no">

                                <div class="well well-sm">
                                    <p class="text-bold text-main text-uppercase">Notes &amp; Information</p>
                                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>
                                    <p>If you have any questions concerning this invoice, please contact <strong class="text-main">billing[at]mycompany.com</strong></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <script>
            $(document).on("click",".confirm_payment",function(){
                var invoice_id = $(this).attr('data-id');
                $.ajax({
                    method:"post",
                    url:"{{ route('invoice.confirmation') }}",
                    data:{id:invoice_id,"_token":"{{ csrf_token() }}"},
                    dataType:"json",
                    success:function(response){
                        if(response.confirm == 1){
                            alert("Invoice Confirmation Done");
                            location.reload();
                        }

                    },
                    error:function(err){
                        console.log(err);
                    }
                });
            });
        </script>


<!-- FOOTER -->
    <!--===================================================-->
    <footer id="footer">

        <!-- Visible when footer positions are fixed -->
        <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
        <div class="show-fixed pad-rgt pull-right">
            You have <a href="#" class="text-main"><span class="badge badge-danger">3</span> pending action.</a>
        </div>
        <!-- Visible when footer positions are static -->
        <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
        <div class="hide-fixed pull-right pad-rgt">
            Developed By <strong>Spinner Tech</strong>.
        </div>

        <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
        <!-- Remove the class "show-fixed" and "hide-fixed" to make the content always appears. -->
        <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

        <strong class="pad-lft">&#0169; {{date('Y')}} <a target="blank" href="#">HD Exp.</a></strong>



    </footer>
    <!--===================================================-->
    <!-- END FOOTER -->
    <!-- SCROLL PAGE BUTTON -->
    <!--===================================================-->
    <button class="scroll-top btn">
        <i class="pci-chevron chevron-up"></i>
    </button>
    <!--===================================================-->
</div>

</body>
</html>
