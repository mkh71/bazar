@extends('admin.layouts.admin')

@section('content')
    <div class="boxed">
        <div id="content-container">
            <div id="page-head">
                <div id="page-title">
                    <h1 class="page-header text-overflow">View Invoice Summary</h1>
                </div>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="demo-pli-home"></i></a></li>
                    <li><a href="#">Admin</a></li>
                    <li class="active">Invoice</li>
                </ol>
            </div>
            <div id="page-content">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel">
                            <div class="panel-heading">
                                <h3 class="panel-title">Invoice </h3>
                            </div>
                            <div class="panel-body">
                               <div class="row">
                                    <table class="table table-bordered table-striped" id="CategoryTable">
                                        <thead>
                                        <tr>
                                            <th>SL</th>
                                            <th>Inv. ID</th>
                                            <th>S.Name</th>
                                            <th>S.Phone </th>
                                            <th>T.Amount</th>
                                            <th>Discount</th>
                                            <th>Balance</th>
                                            <th>Paid</th>

                                            <th>P.Type</th>
                                            <th>TrxID</th>
                                            <th>Inv. Date</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @php $i=0; @endphp
                                        @foreach($invoice as $invoice_info)
                                            <tr>
                                                <td>{{ ++$i }}</td>
                                                <td>{{ $invoice_info->invoice_no }}</td>
                                                <td>{{ $invoice_info->supplier->name }}</td>
                                                <td>{{ $invoice_info->supplier->phone }}</td>
                                                <td>{{ $invoice_info->total_amount }}</td>
                                                <td>{{ $invoice_info->total_discount }}</td>

                                                @php  $total = 0; $after_discount=0; $after_discount = $invoice_info->total_amount-$invoice_info->total_discount; @endphp
                                                @foreach($invoice_info->cash_books as $data)
                                                    @php
                                                        $total+=$data->expense
                                                    @endphp
                                                @endforeach

                                                <td>

                                                    @if($after_discount-$total == 0)
                                                        <label class="label label-success">Full Paid</label>
                                                    @else
                                                        <label class="label label-danger">Due :  @php echo $after_discount-$total; @endphp tk</label>
                                                    @endif
                                                </td>
                                                <td> {{ $total }} </td>
                                                <td>{{ $invoice_info->payment_type }}</td>
                                                <td>{{ $invoice_info->payment_trxid }}</td>
                                                <td>{{ $invoice_info->created_at}}</td>
                                                <td>
                                                    @if($after_discount-$total == 0)
                                                        <a href="{{ url('PurchaseMangement/Invoice-Visit/'.$invoice_info->id) }}" target="_blank" class="btn btn-success fa fa-print"></a>
                                                    @else
                                                        <label class="label label-danger">Please pay first all due</label>
                                                    @endif


                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>

        $(function(){
            $('#CategoryTable').DataTable();

        });

    </script>

@endsection