@extends('admin.layouts.admin')

@section('content')
    <div class="boxed">
        <div id="content-container">
            <div id="page-head">
                <div id="page-title">
                    <h1 class="page-header text-overflow">Add New Purchase</h1>
                </div>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="demo-pli-home"></i></a></li>
                    <li><a href="#">Admin</a></li>
                    <li class="active">Product</li>
                </ol>
            </div>
            <div id="page-content">
                <div class="row">
                    
                    <div class="panel">

                        <div class="panel-heading">
                            <h3 class="panel-title">Add New Purchase</h3>
                        </div>


                        <div class="panel-body">

                            <div class="row">
                                <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
                                    <div class="row">
                                        {{ Form::hidden('id',null,['id'=>'pid']) }}
                                        <div class="col-lg-3 col-sm-3 col-xs-12 {{$errors->has('product_code') ? 'has-error' : ''}}">
                                            <span style="display: block;height: 10px;width: 100%;background: #fff;"></span>
                                            {{ Form::label('product','Product Code : ',['class'=>'control-label'])}}
                                            {{ Form::select('product_code',$product_code,null,['class'=>'form-control product_code_select2','required','id'=>'product_code','multiple'])}}
                                           
                                        </div>
                                        <!--  PRODUCT NAME  -->
                                        <div class="col-lg-9 col-sm-9 col-xs-12 {{$errors->has('product_name') ? 'has-error' : ''}}">
                                            <span style="display: block;height: 10px;width: 100%;background: #fff;"></span>
                                            {{ Form::label('product','Product Name : ',['class'=>'control-label'])}}
                                            {{ Form::select('product_name',$product_name,null,['class'=>'form-control product_name_select2','required','id'=>'product_name','multiple'])}}
                                        </div>
                                        <!-- / PRODUCT NAME  -->
                                     </div> {{-- Product COde and Product Name Search Row End --}}
                                    <div class="row">
                                        <!--  PRODUCT Brand NAME  -->
                                        <div class="col-lg-4 col-sm-4 col-xs-12 {{$errors->has('brand_id') ? 'has-error' : ''}}">
                                            <span style="display: block;height: 10px;width: 100%;background: #fff;"></span>
                                            {{ Form::label('brand','Brand : ',['class'=>'control-label'])}}
                                            {{ Form::text('brand_id',null,['readonly','class'=>'form-control','required','id'=>'brand_id'])}}

                                        </div>
                                        <!-- / PRODUCT Brand NAME  -->

                                        <!--  PRODUCT category_id NAME  -->
                                        <div class="col-lg-4 col-sm-4 col-xs-12 {{$errors->has('category_id') ? 'has-error' : ''}}">
                                            <span style="display: block;height: 10px;width: 100%;background: #fff;"></span>
                                            {{ Form::label('color','Category : ',['class'=>'control-label'])}}
                                           {{ Form::text('category_id',null,['readonly','class'=>'form-control','id'=>'category_id'])}}
                                        </div>
                                        <!-- / PRODUCT category_id NAME  -->

                                        <!--  PRODUCT subcategory_id NAME  -->
                                        <div class="col-lg-4 col-sm-4 col-xs-12 {{$errors->has('subcategory_id') ? 'has-error' : ''}}">
                                            <span style="display: block;height: 10px;width: 100%;background: #fff;"></span>
                                            {{ Form::label('subcategory','Sub-Category : ',['class'=>'control-label'])}}
                                            {{ Form::text('subcategory_id',null,['readonly','class'=>'form-control','required','id'=>'subcategory_id'])}}

                                           
                                        </div>
                                        <!-- / PRODUCT subcategory_id NAME  -->

                                    </div>{{-- Brand Category Sub-Category Row End here  --}}

                                    <div class="row">

                                        <!--  PRODUCT SIZE NAME  -->

                                        <div class="col-lg-4 col-sm-4 col-xs-12 {{$errors->has('size_id') ? 'has-error' : ''}}">
                                            <span style="display: block;height: 10px;width: 100%;background: #fff;"></span>
                                            {{ Form::label('product','Size : ',['class'=>'control-label'])}}
                                            {{ Form::text('size_id',null,['readonly','class'=>'form-control','required','id'=>'size_id'])}}

                                        </div>
                                        <!-- / PRODUCT SIZE NAME  -->

                                        <!--  PRODUCT color NAME  -->
                                        <div class="col-lg-4 col-sm-4 col-xs-12 {{$errors->has('color_id') ? 'has-error' : ''}}">
                                            <span style="display: block;height: 10px;width: 100%;background: #fff;"></span>
                                            {{ Form::label('color','Color : ',['class'=>'control-label'])}}
                                            
                                            {{ Form::text('color_id',null,['readonly','class'=>'form-control','required','id'=>'color_id'])}}
                                        </div>
                                        <!-- / PRODUCT color NAME  -->

                                        <!--  PRODUCT karat NAME  -->
                                        <div class="col-lg-4 col-sm-4 col-xs-12 {{$errors->has('karat_id') ? 'has-error' : ''}}">
                                            <span style="display: block;height: 10px;width: 100%;background: #fff;"></span>
                                            {{ Form::label('karat','Karat : ',['class'=>'control-label'])}}
                                            {{ Form::text('karat_id',null,['readonly','class'=>'form-control','required','id'=>'karat_id'])}}
                                           
                                        </div>
                                    </div>{{-- Size Karat Color Row End Here --}}

                                   

                                    <div class="row">
                                        <!-- Origin Select Start -->

                                        <div class="col-lg-4 col-sm-4 col-xs-12 {{$errors->has('origin_id') ? 'has-error' : ''}}">
                                            <span style="display: block;height: 10px;width: 100%;background: #fff;"></span>

                                            {{ Form::label('origin','Origin : ',['class'=>'control-label'])}}
                                            {{ Form::text('origin_id',null,['readonly','class'=>'form-control','required','id'=>'origin_id'])}}


                                        </div>
                                        <!-- Origin Select End -->

                                        <!-- Origin Select Start -->
                                        <div class="col-lg-4 col-sm-4 col-xs-12 {{$errors->has('gold_type_id') ? 'has-error' : ''}}">
                                            <span style="display: block;height: 10px;width: 100%;background: #fff;"></span>

                                            {{ Form::label('Gold Type','Gold Type : ',['class'=>'control-label'])}}
                                            {{ Form::text('gold_type_id',null,['readonly','class'=>'form-control','required','id'=>'gold_type_id'])}}


                                        </div>
                                        <!-- Origin Select End -->

                                        <!--  PRODUCT quantity   -->
                                        <div class="col-lg-4 col-sm-4 col-xs-12">
                                            <span style="display: block;height: 10px;width: 100%;background: #fff;"></span>

                                            {{ Form::label('productqty','Quantity : ',['class'=>'control-label'])}}
                                            {{Form::number('prdocut_qty',null,['id'=>'product_qty','required','class'=>'form-control','placeholder'=>'Ex: 10'])}}
                                        </div>
                                        <!-- / PRODUCT quantity   -->

                                    </div> {{-- Upper Row End here --}}

                                    {{-- Product Qty Price and Rate Here --}}

                                    <div class="row">

                                        


                                        {{--weight start --}}
                                        <div class="col-lg-3 col-sm-3 col-xs-12 {{$errors->has('weight_id') ? 'has-error' : ''}}">
                                            <span style="display: block;height: 10px;width: 100%;background: #fff;"></span>
                                            {{ Form::label('origin','Weight  : ',['class'=>'control-label'])}}
                                            {{ Form::select('weight_id',$weight,false,['id'=>'weight_id','required','class'=>'form-control']) }}
                                            
                                        </div>
                                    {{-- weight End--}}

                                      {{--weight Amount start  --}}
                                        <div class="col-lg-3 col-sm-3 col-xs-12 {{$errors->has('weight_id') ? 'has-error' : ''}}">
                                            <span style="display: block;height: 10px;width: 100%;background: #fff;"></span>
                                            {{ Form::label('weight_amount','Weight Amount : ',['class'=>'label-control']) }}
                                            {{ Form::number('weight_amount','',['id'=>'weight_amount','class'=>'form-control','placeholder'=>'Gram/Ounce etc']) }}
                                        </div>
                                    {{-- weight End--}}



                                        <!--  PRODUCT product_price  -->
                                        <div class="col-lg-3 col-sm-3 col-xs-12 {{$errors->has('product_price') ? 'has-error' : ''}}">
                                            <span style="display: block;height: 10px;width: 100%;background: #fff;"></span>
                                            {{ Form::label('product','Price : ',['class'=>'control-label'])}}
                                            {{Form::number('product_price',old('product_price'),['id'=>'product_price','required','class'=>'form-control','placeholder'=>'Ex: 5000 tk'])}}
                                        </div>
                                        <!-- / PRODUCT product_price  -->

                                        <!--  PRODUCT Rate  -->
                                        <div class="col-lg-3 col-sm-3 col-xs-12 {{$errors->has('product_price') ? 'has-error' : ''}}">
                                            <span style="display: block;height: 10px;width: 100%;background: #fff;"></span>
                                            {{ Form::label('product','Rate : ',['class'=>'control-label'])}}
                                            {{Form::number('product_price',null,['id'=>'product_rate','readonly','class'=>'form-control','placeholder'=>'Ex: 5000 tk'])}}
                                        </div>
                                        <!-- / PRODUCT Rate NAME  -->

                                        




                                    </div>{{--  Rate Qty Price Row End--}}

                                        <div class="col-lg-12 col-xs-12 col-md-12 col-sm-12">
                                            <span style="display: block;height: 10px;width: 100%;background: #fff;"></span>
                                            <div class="form-group">
                                                <button class="btn btn-info form-control" type="button" id="add_bucket">Add To Bucket</button>
                                            </div>
                                        </div>
                                    </div>

                                <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 pull-right">
                                    <h1 class="text-center bg-primary">Bucket List</h1>
                                    <table class="table table-hover">
                                        <thead>
                                        <tr>
                                            <th>SL</th>
                                            <th>Product Name</th>
                                            <th> Qty </th>
                                            <th>Price</th>
                                            <th>S.Total</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody id="all_cart_item">

                                        </tbody>
                                    </table>
                                    <hr>

                                    {{-- suplier information added start from here --}}
                                    {{-- suplier information added start from here --}}
                                    {{-- suplier information added start from here --}}
                                    {{ Form::open(['route'=>'purchase.save','method'=>'post']) }}
                                    <div class="row">
                                        <h4 class="text-center bg-primary" style="padding: 8px 0">Supplier Information : </h4>
                                        <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                                            {{ Form::label('searchsupplier','Search Supplier',['class'=>'label-control']) }}
                                            {{ Form::select('supplier_id',$suppliers,false,['id'=>'supplier_id','class'=>'form-control supplier_id_select2','multiple'=>'multiple']) }}
                                        </div>
                                        <div class="col-lg-6 col-sm-6 col-xs-12">
                                            <span style="display: block;height: 10px;width: 100%;background: #fff;"></span>
                                            {{ Form::label('customername','Supplier Name : ',['class'=>'label-control']) }}
                                            {{ Form::text('supplier_name',null,['class'=>'form-control','id'=>'supplier_name','placeholder'=>'Ex. Mr.xyz']) }}
                                        </div>
                                        <div class="col-lg-6 col-sm-6 col-xs-12">
                                            <span style="display: block;height: 10px;width: 100%;background: #fff;"></span>
                                            {{ Form::label('customerphone','Supplier Phone : ',['class'=>'label-control']) }}
                                            {{ Form::text('Supplier_phone',null,['class'=>'form-control','placeholder'=>'Ex : 923584596','id'=>'supplier_phone']) }}
                                        </div>
                                        <div class="col-lg-12 col-sm-12 col-xs-12">
                                            <span style="display: block;height: 10px;width: 100%;background: #fff;"></span>
                                            {{ Form::label('customeraddress','Supplier Address : ',['class'=>'label-control']) }}
                                            {{ Form::textarea('Supplier_address',null,['id'=>'supplier_address','class'=>'form-control','placeholder'=>'New York City','rows'=>5,'cols'=>'10']) }}
                                        </div>
                                    </div>

                                    {{-- suplier information added start from here --}}
                                    {{-- suplier information added start from here --}}
                                    {{-- Suplier information added end from here --}}

                                    <div class="row">
                                        <h4 class="text-center bg-primary" style="padding: 8px 0">Buy Information : </h4>
                                        <div class="col-lg-12 col-sm-12 col-xs-12">
                                            <span style="display: block;height: 10px;width: 100%;background: #fff;"></span>
                                            <table class="table-hover table table-bordered table-responsive">
                                                <tbody>



                                                {{-- Total amount of cart loop end from here --}}
                                                <tr>
                                                    <td> <span class="btn btn-primary">Total Amount : </span>  </td>
                                                    <td><input type="number" id="total_amount" name="total_amount" readonly  placeholder="500 tk" class="form-control"></td>
                                                </tr>

                                                <tr>
                                                    <td>
                                                        <span class="btn btn-primary"> Total Discount : </span>
                                                    </td>
                                                    <td><input type="number" id="discount" name="total_discount" placeholder="Discount : 100 tk" class="form-control"></td>
                                                </tr>

                                                <tr>
                                                    <td> <span class="btn btn-primary"> Total Paid : </span></td>
                                                    <td><input type="number" id="paid" name="total_paid" placeholder="500 tk" class="form-control"></td>
                                                </tr>

                                                <tr>
                                                    <td><span class="btn btn-primary"> Total Balance : </span></td>
                                                    <td><input type="number" readonly name="total_balance" id="balance" placeholder="500 tk" class="form-control"></td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td>
                                                        {{ Form::button('PRODUCT PURCHASE ',['type'=>'submit','class'=>'btn btn-primary']) }}
                                                    </td>
                                                </tr>

                                                </tbody>

                                            </table>
                                        </div>
                                    </div>{{--/ Buy and Supllier Information end here  /--}}

                                    <div class="col-lg-12 col-sm-12 col-xs-12">
                                        <span style="display: block;height: 10px;width: 100%;background: #fff;"></span>
                                        <div class="row">
                                            {{ Form::label('customername','Customer Note : ',['class'=>'label-control']) }}
                                            {{ Form::textarea('sale_note','Thank you so much for stay with Ringer-Soft jewellery Shop',['class'=>'form-control','rows'=>2,'cols'=>8,'placeholder'=>'Thank you so much for buy from here']) }}
                                        </div>
                                        <span style="display: block;height: 10px;width: 100%;background: #fff;"></span>
                                        <div class="row">
                                            {{ Form::label('customername','Payment Type : ',['class'=>'label-control']) }}
                                            {{ Form::select('payment_type',[''=>'SELECT PAYMENT TYPE','bkash'=>'Bkash','card'=>'Credit Card','check'=>'Bank Check'],false,['class'=>'form-control','id'=>'payment_type']) }}
                                            <hr>
                                            {{ Form::text('bkash_code','',['class'=>'form-control','id'=>'bkash_code','placeholder'=>'Ex: T5x1G2']) }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div><!--  Panel Body Close -->

                    </div><!-- Panel Close -->

                </div><!-- Row Close -->
           </div><!-- Page Content Close -->
        </div>
    </div>



    <script>
        var global_price;
        var amount=0;
        var amount = "{{ Cart::total() }}";
        $(document).ready(function(){
            /* at first auto hide weight amount ... it will be show when weight value found either it will be hide */
            $("#tamount").val(amount);
            $("#weight_info").css("display","none");
            /* at first auto hide weight amount ... it will be show when weight value found either it will be hide */

            /* All added cart items showing with this functions */

            Carts();

            /* All added cart items showing with this functions */

            global_price = 0;
            product_code();
            $("#bkash_code").hide();
            $(".product_code_select2").select2({
                maximumSelectionLength: 1
            });

            $(".weight_id_select2").select2({ tags:true,maximumSelectionLength : 1});
            $(".product_name_select2").select2({maximumSelectionLength : 1});

            $(".supplier_id_select2").select2({tags: true ,maximumSelectionLength: 1});

        });

        /* when product code empty then automatic other field will be empty */



        /* suppliers search start */

        $(document).on('change','#supplier_id',function(){
            var supplier_id = $(this).val();
            var token = "{{ csrf_token() }}";
            $.ajax({
                method:"post",
                url:"{{route('supplier.search')}}",
                data:{supplier_id:supplier_id,"_token":token},
                dataType:'json',
                success:function(response){
                    $("#supplier_name").val(response[0].name);
                    $("#supplier_phone").val(response[0].phone);
                    $("#supplier_address").val(response[0].address);
                    console.log("Response List : "+response[0].id);
                },
                error:function(err){console.log("Error List : "+err);}
            });

        });

        /* suppliers search end */

        function product_code(){
            var p_code = $("#product_code").val();
            if(p_code == ""){
                $('#product_name').val();
                $('#product_price').val();
                $('#product_local_name').val();
                $('#product_details').val();
                /* product price multple quantity equal Total Price */
                $("#tamount").val();
            }
        }

        /*
             ========== product information search with Product Code ===========
       */

        $(document).on('change','#product_code',function(){
            var product_code_id = $(this).val();
            var token = "{{ csrf_token() }}";
            if(product_code_id != ''){
                $.ajax({
                    method:"post",
                    url:"{{ route('product.find') }}",
                    data:{product_code_id: product_code_id,"_token":token},
                    dataType:"json",
                    success:function(response){
                      
                        var parsedata = JSON.parse(response);

                        console.log(parsedata.brand);

                        $("#pid").val(parsedata.id);

                        global_price = parsedata.product_price;

                        $('#product_name').val(parsedata.id).attr('selected',true);
                        $('.product_code_select2').select2({tags:true,maximumSelectionLength : 1});
                        $('#product_price').val(parsedata.product_price);
                        
                     
                        $("#size_id").val(parsedata.size);
                        $("#color_id").val(parsedata.color);
                        $("#karat_id").val(parsedata.karat);
                        $("#origin_id").val(parsedata.origin);
                        $("#gold_type_id").val(parsedata.goldtype);
                            

                        $("#brand_id").val(parsedata.brand);
                        $("#category_id").val(parsedata.category);
                        $("#subcategory_id").val(parsedata.subcategorie);
                    },
                    error:function(err){
                        console.log(err);
                    }
                });
            }else{
                product_code();
            }
        });

        /*
         ========== product information search with Product Code ===========
         */

        /* if select product name */
        /*
         ========== product information search with Product Name ===========
         */

        $(document).on('change','#product_name',function(){
            var product_code_id = $(this).val();
            var token = "{{ csrf_token() }}";
            if(product_code_id != ''){
                $.ajax({
                    method:"post",
                    url:"{{ route('product.find') }}",
                    data:{product_code_id: product_code_id,"_token":token},
                    dataType:"json",
                    success:function(response){

                        var parsedata = JSON.parse(response);

                        console.log(parsedata.brand);

                        $("#pid").val(parsedata.id);

                        global_price = parsedata.product_price;

                        $('#product_name').val(parsedata.product_name);
                        $('#product_code').val(parsedata.id).attr('selected',true);
                        $('.product_code_select2').select2({tags:true,maximumSelectionLength : 1});
                        $('#product_price').val(parsedata.product_price);
                        
                     
                        $("#size_id").val(parsedata.size);
                        $("#color_id").val(parsedata.color);
                        $("#karat_id").val(parsedata.karat);
                        $("#origin_id").val(parsedata.origin);
                        $("#gold_type_id").val(parsedata.goldtype);


                        $("#brand_id").val(parsedata.brand);
                        $("#category_id").val(parsedata.category);
                        $("#subcategory_id").val(parsedata.subcategorie);


                        /* product price multple quantity equal Total Price */
                    },
                    error:function(err){
                        console.log(err);
                    }
                });
            }else{
                product_code();
            }
        });

        /*
         ========== product information search with Product Name ===========
         */

        $(document).on('change','#payment_type',function(){
            var type = $(this).val();
            if(type == 'bkash'){
                $("#bkash_code").show(500);
            }
        });

        /* onchange quantity for total amount */


          /* When someone try to increment and decrement product price and if there price value found less then zero then it will be Original Product Price automatically */

        $(document).on('keyup change','#product_price',function(){
            var price = $(this).val();
            if(price<=0){
                $(this).closest('div').addClass('has-error');
                alert('Sir/Madam Product Price Never be Zero');
                $(this).val(global_price);
                $(this).closest('div').removeClass('has-error');
            }

            var total = 0;
            total = price*$("#product_qty").val();
            $("#product_rate").val(total);

        });



        /* When someone try to increment and decrement product price and if there price value found less then zero then it will be Original Product Price automatically */

        /* if quantity change then it will be calculate with price and store total value inside rate start */

        $(document).on('keyup change','#product_qty',function(){
            var price = $("#product_price").val();
            var qty = $(this).val();
            if(qty<=0){
                $("#product_rate").val(price);
                $(this).closest('div').addClass('has-error');
                alert('Sir/Madam Product Quantity Never be Zero');
                alert(price);
                $(this).val(1);
                
                $(this).closest('div').removeClass('has-error');
            }

            var total = 0;
            total = $("#product_price").val()*qty;
            $("#product_rate").val(total);

        });

        /**/


        /* Product add to Cart / Bucket Start From Here */

        $(document).on('click','#add_bucket',function(){

            var id = $("#product_name").val();

            var product_price = $("#product_price").val();

            var weight_id = $("#weight_id").val();
            var weight_amount = $("#weight_amount").val();
            var productqty = $("#product_qty").val();


            if(  product_price == "" || weight_id == "" || weight_amount == "" || productqty<0){
                alert("Please Select Every single field before Add to Cart");
            }
            else{
                $.ajax({
                    method : "post",
                    url : "{{ route('purchase.addCart') }}",
                    data:{id:id,product_price:product_price,"_token":'{{ csrf_token() }}',weight_id:weight_id,weight_amount:weight_amount,product_qty:productqty},
                    dataType:'html',
                    success:function(response){
                        $("#product_name").val('').attr('selected',false);
                        $('.product_name_select2').select2();

                        $("#product_code").val('').attr('selected',false);
                        $('.product_code_select2').select2();

                        $("#product_price").val('');
                    

                        $("#all_cart_item").html(response);
                    },
                    error:function(err){
                        console.log(err);
                    }
                });
            }
        });
        /* Product add to Cart / Bucket End Here */

        /* onload added products fetch */

        function Carts(){
            $.ajax({
                method:"get",
                url : "{{ route('purchase.carts') }}",
                dataType:'html',
                success:function(response){
                    $("#all_cart_item").html(response);
                },
                error:function(err){
                    console.log(err);
                }
            });
        }
        /* onload added products fetch */

        /*  Cart Remove Start */
        $(document).on('click','#remove_item',function(){
            var token = '{{ csrf_token() }}';
            var rowId = $(this).attr('data-id');
            $.ajax({
                method:"post",
                url:'{{ route("purchase.cart-remove") }}',
                data:{rowId:rowId,"_token":token},
                dataType:"html",
                success:function(response){
                    $("#all_cart_item").html(response);
                },
                error:function(err){
                    console.log(err);
                }
            });
        });
        /*  Cart Remove End */

        /* Cart Product List Edit Start */
        $(document).on('click','#update_item',function(){
            var rowId = $(this).attr('data-id');
            var token = '{{ csrf_token() }}';

            $.ajax({
                method:"post",
                url:'{{ route("purchase.cart-edit") }}',
                data:{rowId:rowId,"_token":token},
                dataType:"json",
                success:function(response){

                        var parsedata = JSON.parse(response);

                        console.log(parsedata.qty);

                        $("#pid").val(parsedata.id);

                        global_price = parsedata.product_price;

                        
                        $('#product_code').val(parsedata.id).attr('selected',true);

                        $('.product_code_select2').select2({tags:true,maximumSelectionLength : 1});
                        $('.product_name_select2').select2({tags:true,maximumSelectionLength : 1});
                        $('#product_name').val(parsedata.id).attr('selected',true);

                        $('#product_price').val(parsedata.product_price);
                        
                     
                        $("#size_id").val(parsedata.size);
                        $("#color_id").val(parsedata.color);
                        $("#karat_id").val(parsedata.karat);
                        $("#origin_id").val(parsedata.origin);
                        $("#gold_type_id").val(parsedata.goldtype);
                            

                        $("#brand_id").val(parsedata.brand);
                        $("#category_id").val(parsedata.category);
                        $("#subcategory_id").val(parsedata.subcategorie);
                        $("#weight_amount").val(parsedata.weight_amount);

                        $("#product_qty").val(parsedata.qty);
 
                        var total =  $('#product_price').val() * $("#product_qty").val();

                        $("#product_rate").val(total);

                    


                },
                error:function(err){
                    console.log(err);
                }
            });
        });

        /* Cart Product List Edit End */

        /* update Process Start From here */

        $(document).on('click','#unique_cart_update',function(){
            var id = $("#product_name").val();
            var rowId = $(this).attr('data-id');

            var product_price = $("#product_price").val();


            var cart_quantity = $("#product_qty").val();


            var weight_id = $("#weight_id").val();
            var weight_amount = $("#weight_amount").val();

            if( product_price == "" || cart_quantity=="" || weight_id == "" || weight_amount==""){
                alert("Please Select Every single field before Add to Cart");
            }
            else{
                $.ajax({
                    method : "post",
                    url : "{{ route('purchase.UpdateCart') }}",
                    data:{id:id,rowId:rowId,product_price:product_price,"_token":'{{ csrf_token() }}',weight_id:weight_id,weight_amount:weight_amount,cart_quantity:cart_quantity},
                    dataType:'html',
                    success:function(response){
                        $(this).removeAttr("data-id");
                        $(this).text("Add To Bucket");
                        $(this).attr("class","btn btn-primary");
                        $(this).attr("id","add_bucket");

                        $("#unique_cart_update").text('Add To Cart').attr('id','add_bucket').removeAttr('data-id');

                        $("#product_name").val('').attr('selected',false);
                        $('.product_name_select2').select2({ tags:true,maximumSelectionLength : 1});

                        $("#product_code").val('').attr('selected',false);
                        $('.product_code_select2').select2({ tags:true,maximumSelectionLength : 1});

                        $("#product_price").val('');

                       

                        $("#karat_id").val('').attr('selected',false);
                        $('.karat_id_select2').select2({ tags:true,maximumSelectionLength : 1});

                        $("#all_cart_item").html(response);
                    },
                    error:function(err){
                        console.log(err);
                    }
                });
            }
        });

        /* Update Process End From here */


        /* payment calculation start from here */

        $(document).on('keyup','#discount',function() {
            var total_amount = $("#total_amount").val();
            var discount = $(this).val();
            var balance = total_amount - discount;
            if(balance>0){
                $("#balance").css({"border": "2px red","background": "red","color": "#fff","font-size": "1.5rem","font-weight": "bold"});
            }else{
                $("#balance").css({"border": "2px green","background": "#fff","color": "#000","font-size": "1.5rem","font-weight": "bold"});
            }
            $("#balance").val(balance);

        });


        $(document).on('keyup','#paid',function() {
            var total_amount = $("#total_amount").val();
            var discount = $("#discount").val();
            var payable_amount = total_amount - discount;
            var total_value  = $(this).val();
            var bal = payable_amount - total_value;
            if(bal>0){
                $("#balance").css({"border": "2px red","background": "red","color": "#fff","font-size": "1.5rem","font-weight": "bold"});
            }else{
                $("#balance").css({"border": "2px green","background": "#fff","color": "#000","font-size": "1.5rem","font-weight": "bold"});
            }
            $("#balance").val(bal);

        });
        /* Payment calculation end here */

        /* */

    </script>
@endsection