@extends('admin.layouts.admin')
@section("title","Edit-title | MTR")
@section('content')
    <div class="boxed">
        <div id="content-container">
            <div id="page-head">
                <div id="page-title">
                    <h1 class="page-header text-overflow">Edit title</h1>
                </div>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="demo-pli-home"></i></a></li>
                    <li><a href="#">Admin</a></li>
                    <li class="active">title</li>
                </ol>
            </div>
            <div id="page-content">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel">
                            <div class="panel-heading">
                                <h3 class="panel-title">Edit title</h3>
                            </div>
                            <div class="panel-body">
                                <div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
                                    <div class="col-lg-12 col-sm-12 {{$errors->has('name') ? 'has-error' : ''}}">
                                        {{ Form::model($title,['route'=>['title.update',$title->id],'method'=>'post']) }}

                                        {{ Form::label('title','title Name : ',['class'=>'control-label'])}}

                                        {{ Form::text('name',old('name'),['class'=>'form-control','placeholder'=>'Ex: Mikimoto'])}}
                                        @if ($errors->has('name'))
                                            <span class="help-block">
                                                 <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                        @endif
                                        <br>
                                    </div>

                                    <div class="col-lg-12 col-sm-12 {{$errors->has('page') ? 'has-error' : ''}}">

                                        {{ Form::label('','Page : ',['class'=>'control-label'])}}
                                        {{ Form::select('page',[
                                            "1"=>"Home Page",
                                            "2"=>"About Page",
                                            "3"=>"Contact Us Page",
                                            "4"=>"Services",
                                            "5"=>"Singe Service",
                                            "6"=>"Categories",
                                            "7"=>"Category wise Product",
                                            "8"=>"Single Product",
                                        ],$title->page,['class'=>'form-control'])}}
                                        @if ($errors->has('page'))
                                            <span class="help-block">
                        <strong>{{ $errors->first('page') }}</strong>
                </span>
                                        @endif
                                        <br>
                                    </div>



                                    <div class="col-md-6 col-xs-6">
                                        <br>
                                        {{ Form::submit('UPDATE title',['class'=>'form-control btn btn-primary']) }}
                                    </div>
                                    <div class="col-md-6 col-xs-6">
                                        <br>
                                        <a href="{{route('title.add')}}" class="btn btn-danger"> Cancel Edit</a>

                                    </div>
                                    {{ Form::close() }}
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
