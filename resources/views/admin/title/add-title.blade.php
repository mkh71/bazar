    @extends('admin.layouts.admin')
@section('title','Add title')
@section('content')
    <div class="boxed">
        <div id="content-container">
            <div id="page-head">
                <div id="page-title">
                    <h1 class="page-header text-overflow">Add / View title</h1>
                </div>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="demo-pli-home"></i></a></li>
                    <li><a href="#">Admin</a></li>
                    <li class="active">title</li>
                </ol>
            </div>
            <div id="page-content">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel">
                            <div class="panel-heading">
                                <h3 class="panel-title">Add New Title</h3>
                            </div>
                            <div class="panel-body">
                                <div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
        <div class="col-lg-12 col-sm-12 {{$errors->has('name') ? 'has-error' : ''}}">
            {{ Form::open(['route'=>'title.store','method'=>'post']) }}
            {{ Form::label('title','title Name : ',['class'=>'control-label'])}}
            {{Form::text('name',old('name'),['class'=>'form-control','placeholder'=>'Ex: Best Engineering Tools'])}}
            @if ($errors->has('name'))
                <span class="help-block">
                        <strong>{{ $errors->first('name') }}</strong>
                </span>
            @endif
            <br>
        </div>


        <div class="col-lg-12 col-sm-12 {{$errors->has('page') ? 'has-error' : ''}}">

            {{ Form::label('','Page : ',['class'=>'control-label'])}}
            {{ Form::select('page',[
                "1"=>"Home Page",
                "2"=>"About Page",
                "3"=>"Contact Us Page",
                "4"=>"Services",
                "5"=>"Singe Service",
                "6"=>"Categories",
                "7"=>"Category wise Product",
                "8"=>"Single Product",
            ],false,['class'=>'form-control'])}}
            @if ($errors->has('page'))
                <span class="help-block">
                        <strong>{{ $errors->first('page') }}</strong>
                </span>
            @endif
            <br>
        </div>



        <div class="col-md-12 col-xs-12">
            <br>
        </div>

        <div class="col-md-12 col-xs-12">
            {{ Form::submit('SAVE title',['class'=>' btn btn-primary']) }}
        </div>
        {{ Form::close() }}
    </div>

        <div class="col-lg-8 col-sm-8 col-md-8 col-xs-12">
            <table class="table table-bordered table-striped" id="titleTable">
                <thead>
                    <tr>
                        <th>SL</th>
                        <th>title</th>
                        <th>Page</th>
                        <th>Action </th>
                    </tr>
                </thead>
                <tbody>
                    @php $i=0; @endphp
                    @foreach($titles as $info)
                        <tr id="rowid{{$info->id}}" class="abcd">
                            <td>{{++$i}}</td>
                            <td>{{$info->name}}</td>
                            <td>
                                @if($info->page == 1)
                                    <label for="" class="btn btn-info"> Home Page</label>
                                @elseif($info->page == 2)
                                     <label for="" class="btn btn-info"> About Page</label>
                                @elseif($info->page == 3)
                                     <label for="" class="btn btn-info"> Contact Us  Page</label>
                                @elseif($info->page == 4)
                                     <label for="" class="btn btn-info"> Services Page</label>
                                @elseif($info->page == 5)
                                     <label for="" class="btn btn-info"> Singe Service Page</label>
                                @elseif($info->page == 6)
                                     <label for="" class="btn btn-info"> Categories Page</label>
                                @elseif($info->page == 7)
                                    <label for="" class="btn btn-info"> Category wise Product</label>
                                @else
                                    <label for="" class="btn btn-info"> Single Product Page</label>
                                @endif
                            </td>
                            <td>
                                <a href="{{route('title.edit',$info->id)}}" class="btn btn-sm btn-info edit" ><i class="demo-pli-pen-5"></i></a> ||
                                <button class="btn btn-sm btn-danger erase" data-id="{{$info->id}}" data-url="{{ route ('title.destroy')}}"><i class="demo-pli-trash"></i></button>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>

        </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>

        $(function(){
            $('#titleTable').DataTable();

        });

        /* UPDATE title END */

    </script>



@endsection
