<div class="col-lg-12">
    <h1 class="btn-primary btn btn-block text-center"> Profile Settings </h1>
    <br>

</div>

<div class="col-lg-3 col-sm-4 {{$errors->has('name') ? 'has-error' : ''}}">
    {{ Form::label('','Company Name : ',['class'=>'control-label'])}}
    {{ Form::text('name',old('name'),['class'=>'form-control','placeholder'=>'Ex: MTR Engineering Co.'])}}
    @if ($errors->has('name'))
        <span class="help-block">
             <strong>{{ $errors->first('name') }}</strong>
        </span>
    @endif
    <br>
</div>

<div class="col-lg-3 col-sm-4 {{$errors->has('phone') ? 'has-error' : ''}}">
    {{ Form::label('','Phone 1 : ',['class'=>'control-label'])}}
    {{ Form::text('phone',old('phone'),['class'=>'form-control','placeholder'=>'Ex: 01***-******'])}}
    @if ($errors->has('phone'))
        <span class="help-block">
             <strong>{{ $errors->first('phone') }}</strong>
        </span>
    @endif
    <br>
</div>


<div class="col-lg-3 col-sm-4 {{$errors->has('phone1') ? 'has-error' : ''}}">
    {{ Form::label('','Phone 2 : ',['class'=>'control-label'])}}
    {{ Form::text('phone1',old('phone'),['class'=>'form-control','placeholder'=>'Ex: 01***-******'])}}
    @if ($errors->has('phone1'))
        <span class="help-block">
             <strong>{{ $errors->first('phone1') }}</strong>
        </span>
    @endif
    <br>
</div>




<div class="col-lg-3 col-sm-4 {{$errors->has('mail') ? 'has-error' : ''}}">
    {{ Form::label('','Mail Address 1 : ',['class'=>'control-label'])}}
    {{ Form::text('mail',old('mail'),['class'=>'form-control','placeholder'=>'Ex: example@email.com'])}}
    @if ($errors->has('mail'))
        <span class="help-block">
             <strong>{{ $errors->first('mail') }}</strong>
        </span>
    @endif
    <br>
</div>



<div class="col-lg-3 col-sm-4 {{$errors->has('gmail') ? 'has-error' : ''}}">
    {{ Form::label('','Mail Address 2 : ',['class'=>'control-label'])}}
    {{ Form::text('gmail',old('gmail'),['class'=>'form-control','placeholder'=>'Ex: example@email.com'])}}
    @if ($errors->has('gmail'))
        <span class="help-block">
             <strong>{{ $errors->first('gmail') }}</strong>
        </span>
    @endif
    <br>
</div>


<div class="col-lg-4 col-sm-8 {{$errors->has('about') ? 'has-error' : ''}}">
    {{ Form::label('','About Company for Footer : ',['class'=>'control-label'])}}
    {{ Form::textarea('about',old('address'),['class'=>'form-control',"id"=>"demo-summernote"])}}
    @if ($errors->has('about'))
        <span class="help-block">
             <strong>{{ $errors->first('about') }}</strong>
        </span>
    @endif
    <br>
</div>


<div class="col-lg-5 col-sm-8 {{$errors->has('address') ? 'has-error' : ''}}">
    {{ Form::label('','Company Address : ',['class'=>'control-label'])}}
    {{ Form::textarea('address',old('address'),['class'=>'form-control',"id"=>"demo-summernote"])}}
    @if ($errors->has('address'))
        <span class="help-block">
             <strong>{{ $errors->first('address') }}</strong>
        </span>
    @endif
    <br>
</div>

<div class="col-lg-12">
    <h1 class="btn btn-primary text-center btn-block"> Social Setting </h1>
    <br>
</div>

<div class="col-lg-3 col-sm-4 {{$errors->has('facebook') ? 'has-error' : ''}}">
    {{ Form::label('','Facebook: ',['class'=>'control-label'])}}
    {{ Form::text('facebook',old('facebook'),['class'=>'form-control','placeholder'=>'Social'])}}
    @if ($errors->has('facebook'))
        <span class="help-block">
             <strong>{{ $errors->first('facebook') }}</strong>
        </span>
    @endif
    <br>
</div>




<div class="col-lg-3 col-sm-4 {{$errors->has('twitter') ? 'has-error' : ''}}">
    {{ Form::label('','Twitter: ',['class'=>'control-label'])}}
    {{ Form::text('twitter',old('twitter'),['class'=>'form-control','placeholder'=>'Social'])}}
    @if ($errors->has('twitter'))
        <span class="help-block">
             <strong>{{ $errors->first('twitter') }}</strong>
        </span>
    @endif
    <br>
</div>

<div class="col-lg-3 col-sm-4 {{$errors->has('instagram') ? 'has-error' : ''}}">
    {{ Form::label('','Instagram: ',['class'=>'control-label'])}}
    {{ Form::text('instagram',old('instagram'),['class'=>'form-control','placeholder'=>'Social'])}}
    @if ($errors->has('instagram'))
        <span class="help-block">
             <strong>{{ $errors->first('instagram') }}</strong>
        </span>
    @endif
    <br>
</div>



<div class="col-lg-3 col-sm-4 {{$errors->has('linkedin') ? 'has-error' : ''}}">
    {{ Form::label('','LinkedIN: ',['class'=>'control-label'])}}
    {{ Form::text('linkedin',old('linkedin'),['class'=>'form-control','placeholder'=>'Social'])}}
    @if ($errors->has('linkedin'))
        <span class="help-block">
             <strong>{{ $errors->first('linkedin') }}</strong>
        </span>
    @endif
    <br>
</div>



<div class="col-lg-12">
    <h1 class="btn btn-primary text-center btn-block"> Logo Setting </h1>
    <br>
</div>


<div class="col-lg-3 col-sm-4 {{$errors->has('logo') ? 'has-error' : ''}}">
    {{ Form::label('','Menu Logo : ',['class'=>'control-label'])}}
    {{ Form::file('logo',['class'=>'form-control',"accept"=>"image/*"])}}
    @if ($errors->has('logo'))
        <span class="help-block">
             <strong>{{ $errors->first('logo') }}</strong>
        </span>
    @endif
    <br>
</div>


<div class="col-lg-3 col-sm-4 {{$errors->has('favicon') ? 'has-error' : ''}}">
    {{ Form::label('','Title Bar Logo : ',['class'=>'control-label'])}}
    {{ Form::file('favicon',['class'=>'form-control',"accept"=>"image/*"])}}
    @if ($errors->has('favicon'))
        <span class="help-block">
             <strong>{{ $errors->first('favicon') }}</strong>
        </span>
    @endif
    <br>
</div>



<div class="col-lg-3 col-sm-4 {{$errors->has('mobile_logo') ? 'has-error' : ''}}">
    {{ Form::label('','Mobile Logo : ',['class'=>'control-label'])}}
    {{ Form::file('mobile_logo',['class'=>'form-control',"accept"=>"image/*"])}}
    @if ($errors->has('mobile_logo'))
        <span class="help-block">
             <strong>{{ $errors->first('mobile_logo') }}</strong>
        </span>
    @endif
    <br>
</div>



<div class="col-lg-3 col-sm-4 {{$errors->has('footer_logo') ? 'has-error' : ''}}">
    {{ Form::label('','Footer Logo : ',['class'=>'control-label'])}}
    {{ Form::file('footer_logo',['class'=>'form-control',"accept"=>"image/*"])}}
    @if ($errors->has('footer_logo'))
        <span class="help-block">
             <strong>{{ $errors->first('footer_logo') }}</strong>
        </span>
    @endif
    <br>
</div>


<div class="col-lg-4">
    {{ Form::submit('Update Company',['class'=>'btn btn-info']) }}
</div>
