@extends('admin.layouts.admin')
@section('title','Add Category')
@section('content')
    <div class="boxed">
        <div id="content-container">
            <div id="page-head">
                <div id="page-title">
                    <h1 class="page-header text-overflow">Add / View Category</h1>
                </div>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="demo-pli-home"></i></a></li>
                    <li><a href="#">Admin</a></li>
                    <li class="active">Category</li>
                </ol>
            </div>
            <div id="page-content">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel">
                            <div class="panel-heading">
                                <h3 class="panel-title">Add New Category</h3>
                            </div>
                            <div class="panel-body">
                                <div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
                                    <div class="col-lg-12 col-sm-12 {{$errors->has('name') ? 'has-error' : ''}}">
                                        {{ Form::open(['route'=>'category.store','method'=>'post','enctype'=>'multipart/form-data','id'=>'CategoryForm']) }}
                                        {{ Form::label('Category','Category Name : ',['class'=>'control-label'])}}
                                        {{Form::text('name',old('name'),['class'=>'form-control','placeholder'=>'Ex: Mikimoto'])}}
                                        @if ($errors->has('name'))
                                            <span class="help-block">
                                                 <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                        @endif
                                        <br>
                                    </div>
                                    
                                    <div class="col-lg-12 col-sm-12 {{$errors->has('image') ? 'has-error' : ''}}">
                                        {{ Form::label('','Cover image : ',['class'=>'control-label'])}}
                                        {{Form::file('image',['class'=>'form-control'])}}
                                        @if ($errors->has('image'))
                                            <span class="help-block">
                                                 <strong>{{ $errors->first('image') }}</strong>
                                            </span>
                                        @endif
                                        <br>
                                    </div>
                                    
                                    
                                    <div class="col-lg-12 col-sm-12 {{$errors->has('description') ? 'has-error' : ''}}">
                                        {{ Form::label('','Description : ',['class'=>'control-label'])}}
                                        {{Form::textarea('description',old('description'),['class'=>'form-control','placeholder'=>'Category Details....'])}}
                                        @if ($errors->has('description'))
                                            <span class="help-block">
                                                 <strong>{{ $errors->first('description') }}</strong>
                                            </span>
                                        @endif
                                    </div>

                                    <div class="col-lg-12 col-sm-12 {{$errors->has('adv_image') ? 'has-error' : ''}}">
                                        {{ Form::label('','Advertise image : ',['class'=>'control-label'])}}
                                        {{Form::file('adv_image',['class'=>'form-control'])}}
                                        @if ($errors->has('adv_image'))
                                            <span class="help-block">
                                                 <strong>{{ $errors->first('adv_image') }}</strong>
                                            </span>
                                        @endif
                                        <br>
                                    </div>

                                    <div class="col-md-12 col-xs-12">
                                        <br>
                                    </div>

                                    <div class="col-md-12 col-xs-12">
                                        {{ Form::button('SAVE Category',['type'=>'submit','id'=>'saveCategory','class'=>' btn btn-primary']) }}
                                    </div>
                                    {{ Form::close() }}
                                </div>

                                <div class="col-lg-8 col-sm-8 col-md-8 col-xs-12">
                                    <table class="table table-bordered table-striped" id="CategoryTable">
                                        <thead>
                                        <tr>
                                            <th>SL</th>
                                            <th>Category</th>
                                            <th>Cover Image</th>
                                            <th>Advertise Image</th>
                                            <th>Details</th>
                                            <th>Action </th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @php $i=0; @endphp
                                        @foreach($category as $info)
                                            <tr id="rowid{{$info->id}}" class="abcd">
                                                <td width="5%">{{++$i}}</td>
                                                <td width="15%">{{$info->name}}</td>
                                                <td width="15%"> <img src="{{ asset('public/admin/category/'.$info->image) }}" alt="no image" style="height: 80px;width: 80px;"> </td>
                                                <td width="15%"> <img src="{{ asset('public/admin/category/advertise/'.$info->advertise_image) }}" alt="no image" style="height: 80px;width: 80px;"> </td>
                                                <td width="55%"> {{$info->description !=null ? substr($info->description,0,150) : "no description " }}</td>
                                                <td width="30%">
                                                    <a href="{{route('category.edit',$info->id)}}" class="btn btn-sm btn-info edit" ><i class="demo-pli-pen-5"></i></a> ||
                                                    <button class="btn btn-sm btn-danger erase" data-id="{{$info->id}}" data-url="{{url('ProductManagement/category/erase')}}"><i class="demo-pli-trash"></i></button>
                                                </td>
                                            </tr>
                                        @endforeach

                                        </tbody>
                                    </table>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>

        $(function(){
            $('#CategoryTable').DataTable();

        });

        /* UPDATE Category END */

    </script>



@endsection
