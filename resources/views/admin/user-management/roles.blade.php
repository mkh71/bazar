@extends('admin.layouts.admin')
@section('content')
    <style>
        input[type='checkbox']{
            width: 50px;
            height: 20px;
        }

        input[type='checkbox']:checked + label{
            color: green;
            font-weight: bold;
        }

        input[type='checkbox'] + label {
            color: #0a6aa1;
            font-weight: 600;
        }

        label{
            line-height: 26px;
        }
    </style>
    <div class="boxed">
        <div id="content-container">
            <div id="page-head">
                <div id="page-title">
                    <h1 class="page-header text-overflow">Add Role</h1>
                </div>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="demo-pli-home"></i></a></li>
                    <li><a href="#">Admin</a></li>
                    <li class="active">Add Role</li>
                </ol>
            </div>
            <div id="page-content">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel">
                            <div class="panel-heading">
                                <h3 class="panel-title">Personal Information</h3>
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    {{--vaccine-transfer Start--}}
                                    <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                                        {{ Form::open(['route'=>'role.store','method'=>'post']) }}
                                        <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 {{$errors->has('name') ? 'has-error' : ''}}">
                                            <span style="display: block;height: 10px;width: 100%;background: #fff;"></span>
                                            {{ Form::label('name','Role Name:* ',['class'=>'control-label'])}}
                                            {{ Form::text('name',null,['class'=>'form-control','id'=>'name'])}}
                                            @if($errors->has('name'))
                                                <span class="help-block"><strong>{{ $errors->first('name') }}</strong></span>
                                            @endif
                                        </div>
                                        <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 {{$errors->has('permission') ? 'has-error' : ''}}">

                                            <label for="permission">
                                                <h4>Permissions</h4>
                                            </label>

                                                @foreach($menus as $menu)
                                                    <div class="col-lg-6 ">
                                                        <div class="row" style="padding: 10px;background: rgba(0,0,0,0.05);margin: 3px;">
                                                            <div class="col-lg-5 text-left">
                                                                <input id="checkList" data-prefix="{{ $menu->prefix }}" type="checkbox">
                                                                <label for="demo-form-checkbox"> Select all</label>
                                                            </div>
                                                            <div class="col-lg-7"> <h5>{{ $menu->prefix }}</h5> </div>
                                                            @php $menusList = \App\Menu::where('prefix',$menu->prefix)->get(); @endphp
                                                            @foreach($menusList as $inside)

                                                                <div class="col-md-12 text-left">
                                                                    <div class="checkbox">
                                                                        <input  type="checkbox" class="{{ $menu->prefix }}" name="menu_id[]" value="{{ $inside->id }}">
                                                                        <label for="demo-form-checkbox">{{ $inside->name }}</label>
                                                                    </div>
                                                                </div>
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                @endforeach

                                            {{--Trasfer end--}}
                                        </div>



                                        <div class="col-lg-12 col-sm-12 col-xs-12"><span style="display: block;height: 10px;width: 100%;background: #fff;"></span></div>
                                        <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
                                            <div class="col-lg-4 col-sm-4 col-xs-12">
                                                {{ Form::submit('SUBMIT',['class'=>'btn btn-success']) }}
                                            </div>
                                        </div>
                                        {{ Form::close() }}
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function(){
            $('#datepicker').datepicker({
                format: 'yyyy-mm-dd'
            });
            $("#protable").DataTable();
            $('#permission_id').select2({
                tags:true
            });
            $('#role_id').select2({
                tags:true
            });
        });



        $(document).on("click","#checkList",function () {
            var prefix = $(this).attr('data-prefix');
            $("."+prefix).prop('checked', $(this).prop('checked'));
        });


    </script>
@endsection