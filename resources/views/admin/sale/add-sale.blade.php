@extends('admin.layouts.admin')

@section('content')
    <div class="boxed">
        <div id="content-container">
            <div id="page-head">
                <div id="page-title">
                    <h1 class="page-header text-overflow">New Sale</h1>
                </div>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="demo-pli-home"></i></a></li>
                    <li><a href="#">Admin</a></li>
                    <li class="active">Sale</li>
                </ol>
            </div>
            <div id="page-content">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel">
                            <div class="panel-heading">
                                <h3 class="panel-title">Sale Management </h3>
                            </div>
                            <div class="panel-body">
                                {{-- Product Name search Button --}}
                                <div class="row">
                                    <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">

                                        {{-- First Row For Product Search --}}
                                        <div class="row">

                                            <div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
                                                <div class="form-group">
                                                    {{ Form::label('code','Barcode :',['class'=>'label-control']) }}
                                                    {{ Form::select('barcode_id',$barcode,false,['class'=>'form-control barcode_select2','multiple','id'=>'barcode']) }}
                                                </div>
                                            </div>

                                            <div class="col-lg-8 col-sm-8 col-md-8 col-xs-12">
                                                <div class="form-group">
                                                    {{ Form::label('product_name','Product Name',['class'=>'label-control']) }}
                                                    {{ Form::select('productname',$product,false,['class'=>'form-control product_id_select2','multiple','id'=>'product_id']) }}

                                                </div>
                                            </div>



                                        </div>
                                        {{-- Row Search End --}}

                                        <div class="row">
                                            {{-- Product Brand , Category , Sub-category Row Start --}}
                                            <div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
                                                <div class="form-group">
                                                    {{ Form::label('Brand','Brand :',['class'=>'label-control']) }}
                                                    {{ Form::select('brand',$brand,false,['class'=>'form-control brand_select2','multiple','id'=>'brand']) }}
                                                </div>
                                            </div>

                                            {{-- Category --}}
                                            <div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
                                                <div class="form-group">
                                                    {{ Form::label('Category','Category :',['class'=>'label-control']) }}
                                                    {{ Form::select('category',$category,false,['class'=>'form-control category_select2','multiple','id'=>'category']) }}
                                                </div>
                                            </div>
                                            {{-- End Category --}}

                                            {{--Sub Category --}}
                                            <div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
                                                <div class="form-group">
                                                    {{ Form::label('subCategory','Sub-Category :',['class'=>'label-control']) }}
                                                    {{ Form::text('subcategory',null,['class'=>'form-control','id'=>'subcategory','readonly']) }}
                                                </div>
                                            </div>
                                            {{-- End Sub-Category --}}
                                        </div>
                                        {{-- Product Brand , Category , Sub-category Row End here --}}


                                        {{-- Karat / Origin / Size Row Start From Here --}}
                                        <div class="row">
                                            {{-- karat --}}
                                            <div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
                                                <div class="form-group">
                                                    {{ Form::label('karat','Karat : ',['class'=>'label-control']) }}
                                                    {{ Form::select('karat',$karat,false,['class'=>'form-control karat_select2','multiple','id'=>'karat']) }}
                                                </div>
                                            </div>
                                            {{-- karat --}}

                                            {{-- Size --}}
                                            <div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
                                                <div class="form-group">
                                                    {{ Form::label('size','Size : ',['class'=>'label-control']) }}
                                                    {{ Form::select('size',$size,false,['class'=>'form-control size_select2','multiple','id'=>'size']) }}
                                                </div>
                                            </div>
                                            {{-- Size --}}

                                            {{-- Origin --}}
                                            <div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
                                                <div class="form-group">
                                                    {{ Form::label('origin','Origin',['class'=>'label-control']) }}
                                                    {{ Form::select('origin',$origin,false,['class'=>'form-control origin_select2','multiple','id'=>'origin']) }}



                                                </div>
                                            </div>
                                            {{-- Origin --}}
                                        </div>
                                        {{-- Karat / Origin / Size Row End From Here --}}

                                        {{-- Price Weight , Weight-amount Row Start From Here --}}
                                        <div class="row">
                                            {{-- Price Start --}}
                                            <div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
                                                {{ Form::label('price','Price : ',['class'=>'label-control']) }}
                                                {{ Form::number('price',null,['id'=>'price','class'=>'form-control','placeholder'=>'Ex : 1000']) }}
                                            </div>
                                            {{-- Price Start --}}
                                            {{-- Qty Start --}}
                                            <div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
                                                {{ Form::label('quantity','Quantity : ',['class'=>'label-control']) }}
                                                {{ Form::number('qty',1,['id'=>'qty','class'=>'form-control','placeholder'=>'Ex : 1','min'=>1]) }}
                                            </div>
                                            {{-- Qty Start --}}


                                            {{-- Weight Start --}}
                                            <div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
                                                {{ Form::label('weight','Weight : ',['class'=>'label-control']) }}
                                                {{ Form::select('weight',$weight,false,['class'=>'form-control weight_select2','multiple','id'=>'weight']) }}
                                            </div>
                                            {{-- Weight Start --}}

                                            {{-- Weight Amount --}}
                                            <div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
                                                {{ Form::label('wamount','Weight Amount : ',['class'=>'label-control']) }}
                                                {{ Form::number('weight_amount',null,['id'=>'weight_amount','class'=>'form-control','placeholder'=>'Ex : 9','min'=>1]) }}
                                            </div>
                                            {{-- Weight Amount --}}
                                        </div>

                                        {{-- Price Weight , Weight-amount Row End From Here --}}


                                    </div>{{-- left div end--}}

                                    <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
                                        <div class="row">
                                            <h4 class="bg-primary text-center" style="padding:10px 0;">Sale Bucket List </h4>
                                            <table class="table table-responsive">
                                                <thead>
                                                <th>SL</th>
                                                </thead>
                                            </table>
                                        </div>{{-- bucket row end --}}


                                    {{-- Customer Information --}}
                                    <h4 class="bg-primary text-center" style="padding:10px 0;">Customer Information</h4>
                                    <div class="row">
                                        <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                                            {{ Form::label('customer_serach','Old Customer Search : ',['class'=>'bg-primary form-control']) }}
                                            {{ Form::select('customer_id',[''],false,['class'=>'form-control','id'=>'customer_id']) }}
                                        </div>
                                        <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
                                            <div class="form-group">
                                                {{ Form::label('customername','Customer Name : ',['class'=>'label-control']) }}
                                                {{ Form::select('customer_name',[''],false,['class'=>'form-control','id'=>'customer_name']) }}
                                            </div>
                                        </div>

                                        <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
                                            <div class="form-group">
                                                {{ Form::label('customername','Customer Phone : ',['class'=>'label-control']) }}
                                                {{ Form::number('customer_phone',null,['class'=>'form-control','id'=>'customer_phone','placeholder'=>'Ex : 123456789']) }}
                                            </div>
                                        </div>

                                        <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                                            <div class="form-group">
                                                {{ Form::label('customername','Customer Name : ',['class'=>'label-control']) }}
                                                {{ Form::textarea('customer_details',null,['class'=>'form-control','id'=>'customer_details','placeholder'=>'Ex : Customer Address','rows'=>5,'cols'=>10]) }}
                                            </div>
                                        </div>
                                    </div> {{-- Customer Information Row End --}}

                                        {{-- sale information row below --}}
                                        <div class="row">
                                            <h4 class="text-center bg-primary" style="padding:10px 0;">Sale Information :  </h4>
                                            <table  class="table table-bordered table-condensed">
                                                <tbody>
                                                    <tr>
                                                        <td class="bg-primary text-left">Total Amount : </td>
                                                        <td class=" text-left">
                                                            {{ Form::number('total',null,['id'=>'total_price','class'=>'form-control text-center','placeholder'=>'Ex : 00 Tk','readonly']) }}                                    </td>
                                                    </tr>

                                                    <tr>
                                                        <td class="bg-primary text-left">Total Discount : </td>
                                                        <td class=" text-left">
 /                                                          {{ Form::number('dis  count',null,['id'=>'total_discount','class'=>'form-control text-center','placeholder'=>'Ex : 00 Tk']) }}                                                </td>
                                                    </tr>

                                                    <tr>
                                                        <td class="bg-primary text-left">Total Balance : </td>
                                                        <td class="text-left">
                                                            {{ Form::number('balance',null,['id'=>'total_balance','class'=>'form-control text-center','placeholder'=>'Ex : 00 Tk','readonly']) }}
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td class="bg-primary text-left">Total Paid : </td>
                                                        <td class="text-left">
                                                            {{ Form::number('paid',null,['id'=>'total_paid','class'=>'form-control text-center','placeholder'=>'Ex : 00 Tk']) }}                                                        </td>
                                                    </tr>

                                                </tbody>
                                            </table>

                                        </div>
                                    </div>
                               </div>{{-- Right div end--}}
                           </div>
                        </div>
                   </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function(){
            $("#product_id").select2({tags:true, maximumSelectionLength : 1});
            $(".barcode_select2").select2({tags:true, maximumSelectionLength : 1});
            $(".weight_select2").select2({tags:true, maximumSelectionLength : 1});
            $(".karat_select2").select2({tags:true, maximumSelectionLength : 1});
            $(".size_select2").select2({tags:true, maximumSelectionLength : 1});
            $(".origin_select2").select2({tags:true, maximumSelectionLength : 1});
            $(".category_select2").select2({tags:true, maximumSelectionLength : 1});
            $(".brand_select2").select2({tags:true, maximumSelectionLength : 1});
        });

        /* Barcode Select and Fire every single Field Start  */

        $(document).on('change','#barcode',function(){
            var barcode = $(this).val();

            $.ajax({
                method:"post",
                url:"{{ route('sale.barcode')}}",
                data:{barcode:barcode,"_token":"{{ csrf_token() }}"},
                dataType:"json",
                success:function(response){
                    console.log(response);
                    // console.log(response.size[0]['name']);
                },
                error:function(err){
                    console.log(err);
                }
            });

        });

        /* Barcode Select and Fire every single Field Ebd  */
    </script>
@endsection