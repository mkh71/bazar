@extends('admin.layouts.admin')

@section('content')
<div class="boxed">
    <div id="content-container">
        <div id="page-head">
            <div id="page-title">
                <h1 class="page-header text-overflow">View Stock Summary</h1>
            </div>
            <ol class="breadcrumb">
                <li><a href="#"><i class="demo-pli-home"></i></a></li>
                <li><a href="#">Admin</a></li>
                <li class="active">Stock</li>
            </ol>
        </div>
        <div id="page-content">
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">Stock </h3>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-lg-5 col-sm-5 col-md-5 col-xs-12">
                                        {{ Form::label('search','Search Stock : ') }}
                                        {{ Form::text('searchkey',null,['class'=>'form-control','id'=>'searchkey','placeholder'=>'Search......']) }}
                                    </div>
                                </div>
                                <div class="col-lg-12 col-xs-12">
                                    <span style="width:100%;height: 20px;display: block;"></span>
                                </div>
                                <table class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>SL</th>
                                        <th>Product Code </th>
                                        <th>Product Name </th>
                                        <th>Available Product </th>

                                    </tr>
                                    </thead>
                                    <tbody id="allstock">

                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){
        ViewStock();
    });


    function ViewStock() {
        $.ajax({
            url:"{{route('stock.all')}}",
            dataType:"html",
            success:function(response){
                $("#allstock").html(response);
            },
            error:function(err){
                console.log(err);
            }
        });
    }

    $(document).on('keyup',"#searchkey",function () {
        var keysearch = $(this).val();
        $.ajax({
            method:"post",
            url:"{{ route('stock.search') }}",
            data:{proname:keysearch,"_token":"{{ csrf_token() }}"},
            success:function(response){
                $("#allstock").html(response);
            },
            error:function(err){
                console.log(err);
            }

        });
    });


</script>

@endsection