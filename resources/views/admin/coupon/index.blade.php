@extends('admin.layouts.admin')

@section('content')
    <div class="boxed">
        <div id="content-container">
            <div id="page-head">
                <div id="page-title">
                    <h1 class="page-header text-overflow">Add / View Coupon</h1>
                </div>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="demo-pli-home"></i></a></li>
                    <li><a href="#">Admin</a></li>
                    <li class="active">Coupon</li>
                </ol>
            </div>
            <div id="page-content">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel">
                            <div class="panel-heading">
                                <h3 class="panel-title">Add New Coupon</h3>
                            </div>
                            <div class="panel-body">
                                <div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
                                    {{ Form::open(['route'=>'coupon.store','method'=>'post', 'enctype'=>'multipart/form-data',]) }}

                                    <div class="col-lg-12 col-sm-12 {{$errors->has('name') ? 'has-error' : ''}}">
                                        {{ Form::label('Coupon','Coupon Name : ',['class'=>'control-label'])}}
                                        {{Form::text('name',old('name'),['class'=>'form-control coupon','placeholder'=>'Ex: Seasonal Coupon'])}}
                                        @if ($errors->has('name'))
                                            <span class="help-block">
                                                 <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                        @endif
                                    </div>

                                    <div class="col-lgo12 col-sm-12 {{$errors->has('user_id') ? 'has-error' : ''}}">
                                        {{Form::label('User_id', 'Select User : ', ['class'=> 'control-label'] )}}
                                        {{Form::select('user_id', $users, null, ['class'=>'form-control select2', 'id'=>'select2']) }}
                                    </div>

                                    <div class="col-lg-12 col-sm-12 {{$errors->has('coupon') ? 'has-error' : ''}}">
                                        {{ Form::label('amount','Coupon Offer Amount : ',['class'=>'control-label'])}}
                                        <div class="row">
                                            <div class="col-lg-12 col-sm-12">

                                                {{Form::number('amount',old('amount'),['class'=>'form-control col-4','placeholder'=>'Ex: 200 (Only Taka)'])}}
                                            </div>
                                            {{--<div class="col-lg-6 col-sm-12">
                                                <label class="checkbox-inline">
                                                    <input type="radio" name="type" value=1 checked> Taka
                                                </label>
                                                <label class="checkbox-inline">
                                                    <input type="radio" name="type" value=2> %
                                                </label>
                                            </div>--}}
                                        </div>
                                        @if ($errors->has('amount'))
                                            <span class="help-block">
                                                 <strong>{{ $errors->first('amount') }}</strong>
                                            </span>
                                        @endif
                                    </div>


                                    <div class="col-lg-12 col-sm-12 {{$errors->has('description') ? 'has-error' : ''}}">
                                        {{ Form::label('','Description : ',['class'=>'control-label']) }}
                                        {{ Form::textarea('description',old('description'),['class'=>'form-control','placeholder'=>'About coupon/ Optional']) }}
                                        @if ($errors->has('description'))
                                            <span class="help-block">
                                                 <strong>{{ $errors->first('description') }}</strong>
                                            </span>
                                        @endif
                                    </div>


                                    <div class="col-md-12 col-xs-12">
                                        <br>
                                    </div>

                                    <div class="col-md-12 col-xs-12">
                                        {{ Form::button('SAVE Coupon',['type'=>'submit','id'=>'saveCoupon','class'=>'col-sm-5 btn btn-primary']) }}
                                    </div>
                                    {{ Form::close() }}
                                </div>

                                <div class="col-lg-8 col-sm-8 col-md-8 col-xs-12">
                                    <table class="table table-bordered table-striped" id="CouponTable">
                                        <thead>
                                        <tr>
                                            <th>SL</th>
                                            <th>Coupon code</th>
                                            <th>Amount</th>
                                            <th>Description</th>
                                            <th>Action </th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @php $i=0; @endphp
                                        @foreach($coupons as $info)
                                            <tr id="rowid{{$info->id}}" class="abcd">
                                                <td>{{++$i}}</td>

                                                <td> {{$info->name}}</td>
                                                <td> {{$info->amount}} Taka}}</td>
                                                <td> {{$info->description == null ? 'No brief' : $info->description }}</td>

                                                <td>
                                                    <a href="{{route('coupon.edit',$info->id)}}"
                                                       class="btn btn-sm btn-info edit" >
                                                        <i class="demo-pli-pen-5"></i>
                                                    </a> ||
                                                    <button class="btn btn-sm btn-danger erase"
                                                            data-id="{{$info->id}}"
                                                            data-url="{{url('Coupon/delete-coupon')}}">
                                                        <i class="demo-pli-trash"></i>
                                                    </button>
                                                </td>
                                            </tr>
                                        @endforeach

                                        </tbody>
                                    </table>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function(){
            var code ='';
            var key = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz";
            for (var i = 0; i < 5; i++)
                code += key.charAt(Math.floor(Math.random() * key.length));
            $(".coupon").val("P"+code);

        });

        $(function(){
            $('#CouponTable').DataTable();

        });

        /* UPDATE Category END */

    </script>



@endsection