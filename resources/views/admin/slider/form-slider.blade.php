<div class="col-lg-12 col-sm-12 {{$errors->has('title') ? 'has-error' : ''}}">
{{ Form::label('Category','Slider    Name : ',['class'=>'control-label'])}}
{{Form::text('title',old('title'),['class'=>'form-control','placeholder'=>'Ex: LED LIGHT'])}}
@if ($errors->has('title'))
    <span class="help-block">
         <strong>{{ $errors->first('title') }}</strong>
    </span>
@endif
<br>
</div>

<div class="col-lg-12 col-sm-12 {{$errors->has('image') ? 'has-error' : ''}}">
    {{ Form::label('','Cover image : ',['class'=>'control-label'])}}
    {{Form::file('image',['class'=>'form-control'])}}
    @if ($errors->has('image'))
        <span class="help-block">
             <strong>{{ $errors->first('image') }}</strong>
        </span>
    @endif
    <br>
</div>


<div class="col-lg-12 col-sm-12 {{$errors->has('description') ? 'has-error' : ''}}">
    {{ Form::label('','Description : ',['class'=>'control-label'])}}
    {{Form::textarea('description',old('description'),['class'=>'form-control','placeholder'=>'Slider Details....'])}}
    @if ($errors->has('description'))
        <span class="help-block">
             <strong>{{ $errors->first('description') }}</strong>
        </span>
    @endif
</div>