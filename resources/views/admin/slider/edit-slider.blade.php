@extends('admin.layouts.admin')
@section('title','Add Category')
@section('content')
    <div class="boxed">
        <div id="content-container">
            <div id="page-head">
                <div id="page-title">
                    <h1 class="page-header text-overflow">Add / View Sliders</h1>
                </div>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="demo-pli-home"></i></a></li>
                    <li><a href="#">Admin</a></li>
                    <li class="active">Sliders</li>
                </ol>
            </div>
            <div id="page-content">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel">
                            <div class="panel-heading">
                                <h3 class="panel-title">Add New Sliders</h3>
                            </div>
                            <div class="panel-body">

                                <div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">

                                    {{ Form::model($slider,['route'=>['slider.update',$slider->id],'method'=>'post','files'=>true]) }}
                                    @include('admin.slider.form-slider')
                                    <div class="col-md-12 col-xs-12">
                                        <br>
                                    </div>
                                    <div class="col-md-12 col-xs-12">
                                        {{ Form::button('SAVE Slider',['type'=>'submit','id'=>'saveCategory','class'=>' btn btn-primary']) }}
                                    </div>
                                    {{ Form::close() }}
                                </div>

                                <div class="col-lg-8 col-sm-8 col-md-8 col-xs-12">
                                    <table class="table table-bordered table-striped" id="CategoryTable">
                                        <thead>
                                        <tr>
                                            <th>SL</th>
                                            <th>Slider Header</th>
                                            <th>Slider Image</th>
                                            <th>Slider description</th>
                                            <th>Action </th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @php $i=0; @endphp
                                        @foreach($sliders as $info)
                                            <tr id="rowid{{$info->id}}" class="abcd">
                                                <td width="5%">{{++$i}}</td>
                                                <td width="15%">{{$info->title}}</td>
                                                <td width="15%">
                                                    <img src="{{ asset('public/slider/'.$info->image) }}" alt="no image" style="height: 80px;width: 80px;">
                                                </td>
                                                <td width="55%">
                                                    {{$info->description !=null ? substr($info->description,0,150) : "no description " }}
                                                </td>
                                                <td width="30%">
                                                    <a href="{{route('slider.edit',$info->id)}}" class="btn btn-sm btn-info edit" >
                                                        <i class="demo-pli-pen-5"></i>
                                                    </a> ||
                                                    <button class="btn btn-sm btn-danger erase" data-id="{{$info->id}}" data-url="{{route('slider.destroy')}}">
                                                        <i class="demo-pli-trash"></i>
                                                    </button>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>

        $(function(){
            $('#CategoryTable').DataTable();
        });
        /* UPDATE Category END */
    </script>
@endsection