@extends('admin.layouts.admin')

@section('content')
    <div class="boxed">
        <div id="content-container">
            <div id="page-head">
                <div id="page-title">
                    <h1 class="page-header text-overflow">Add / View Offer</h1>
                </div>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="demo-pli-home"></i></a></li>
                    <li><a href="#">Admin</a></li>
                    <li class="active">Offer</li>
                </ol>
            </div>
            <div id="page-content">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel">
                            <div class="panel-heading">
                                <h3 class="panel-title">Add New Offer</h3>
                            </div>
                            <div class="panel-body">
                                <div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
                                    {{ Form::open(['route'=>'offer.store','method'=>'post', 'enctype'=>'multipart/form-data',]) }}

                                    <div class="col-lg-12 col-sm-12 {{$errors->has('name') ? 'has-error' : ''}}">
                                        {{ Form::label('Offer','Offer Name : ',['class'=>'control-label'])}}
                                        {{Form::text('name',old('name'),['class'=>'form-control','placeholder'=>'Ex: Seasonal Offer'])}}
                                        @if ($errors->has('name'))
                                            <span class="help-block">
                                                 <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                        @endif
                                    </div>

                                    <div class="col-lg-12 col-sm-12 {{$errors->has('offer') ? 'has-error' : ''}}">
                                        {{ Form::label('Offer','Offer : ',['class'=>'control-label'])}}
                                        <div class="row">
                                            <div class="col-lg-6 col-sm-12">

                                                {{Form::number('offer',old('offer'),['class'=>'form-control col-4','placeholder'=>'Ex: 200Tk / 5% '])}}
                                            </div>
                                            <div class="col-lg-6 col-sm-12">
                                                <label class="checkbox-inline">
                                                    <input type="radio" name="type" value=1 checked> Taka
                                                </label>
                                                <label class="checkbox-inline">
                                                    <input type="radio" name="type" value=2> %
                                                </label>
                                            </div>
                                        </div>
                                        @if ($errors->has('offer'))
                                            <span class="help-block">
                                                 <strong>{{ $errors->first('offer') }}</strong>
                                            </span>
                                        @endif
                                    </div>

                                    <div class="col-lg-12 col-sm-12 {{$errors->has('image') ? 'has-error' : ''}}">
                                        {{ Form::label('','Offer Image : ',['class'=>'control-label'])}}
                                        {{Form::file('image',['class'=>'form-control'])}}
                                        @if ($errors->has('image'))
                                            <span class="help-block">
                                                 <strong>{{ $errors->first('image') }}</strong>
                                            </span>
                                        @endif
                                        <br>
                                    </div>


                                    <div class="col-lg-12 col-sm-12 {{$errors->has('description') ? 'has-error' : ''}}">
                                        {{ Form::label('','Description : ',['class'=>'control-label'])}}
                                        {{Form::textarea('description',old('description'),['class'=>'form-control','placeholder'=>'About offer/ Optional'])}}
                                        @if ($errors->has('description'))
                                            <span class="help-block">
                                                 <strong>{{ $errors->first('description') }}</strong>
                                            </span>
                                        @endif
                                    </div>


                                    <div class="col-md-12 col-xs-12">
                                        <br>
                                    </div>

                                    <div class="col-md-12 col-xs-12">
                                        {{ Form::button('SAVE Offer',['type'=>'submit','id'=>'saveOffer','class'=>'col-sm-5 btn btn-primary']) }}
                                    </div>
                                    {{ Form::close() }}
                                </div>

                                <div class="col-lg-8 col-sm-8 col-md-8 col-xs-12">
                                    <table class="table table-bordered table-striped" id="OfferTable">
                                        <thead>
                                        <tr>
                                            <th>SL</th>
                                            <th>Offer Name</th>
                                            <th>Offer</th>
                                            <th>Description</th>
                                            <th>Cover Image</th>
                                            <th>Action </th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @php $i=0; @endphp
                                        @foreach($offers as $info)
                                            <tr id="rowid{{$info->id}}" class="abcd">
                                                <td>{{++$i}}</td>

                                                <td> {{$info->name}}</td>
                                                <td> {{$info->offer}} {{$info->type ==1 ? 'Taka' : '%'}}</td>
                                                <td> {{$info->description == null ? 'No brief' : $info->description }}</td>

                                                <td>
                                                    <img src="{{ asset('public/admin/offer/'.$info->image) }}" alt="no image" style="height: 80px;width: 80px;">
                                                </td>

                                                <td>
                                                    <a href="{{route('offer.edit',$info->id)}}"
                                                       class="btn btn-sm btn-info edit" >
                                                        <i class="demo-pli-pen-5"></i>
                                                    </a> ||
                                                    <button class="btn btn-sm btn-danger erase"
                                                            data-id="{{$info->id}}"
                                                            data-url="{{url('Offer/delete-offer')}}">
                                                        <i class="demo-pli-trash"></i>
                                                    </button>
                                                </td>
                                            </tr>
                                        @endforeach

                                        </tbody>
                                    </table>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>

        $(function(){
            $('#OfferTable').DataTable();

        });

        /* UPDATE Category END */

    </script>



@endsection