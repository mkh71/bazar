@extends('admin.layouts.admin')
@section('content')
    <div class="boxed">
        <div id="content-container">
            <div id="page-head">
                <div id="page-title">
                    <h1 class="page-header text-overflow">Create / View Offer</h1>
                </div>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="demo-pli-home"></i></a></li>
                    <li><a href="#">Admin</a></li>
                    <li class="active">Manage Offer</li>
                </ol>
            </div>
            <div id="page-content">
                <div class="row">
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">View Product Offer</h3>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                                    <table class="table table-bordered table-striped" id="OfferTable">
                                        <thead>
                                        <tr>
                                            <th>SL</th>
                                            <th>Offer Name</th>
                                            <th>Product Name</th>
                                            <th>Product Price</th>
                                            <th>Offer</th>
                                            <th>Offer Price</th>
                                            {{--<th>Description</th>--}}
                                            <th>Action </th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @php $i=0; @endphp
                                        @foreach($offer_product as $info)
                                            <tr id="rowid{{$info->id}}" class="abcd">
                                                <td>{{++$i}}</td>

                                                <td> {{$info->offer->name}}</td>
                                                <td> {{$info->product->name}}</td>
                                                <td> {{$info->product->price}}</td>
                                                <td>
                                                    {{$info->offer->offer}}
                                                    {{$info->offer->type == 1 ? 'Taka' : '%'}}
                                                </td>
                                                <td> {{$info->offer_price}} </td>
                                          {{--      <td> {{$info->description == null ? 'No brief' : $info->description }}</td>--}}

                                                <td width="15%">
                                                    <a href="{{route('offer_product.edit',$info->id)}}"
                                                       class="btn btn-sm btn-info edit" >
                                                        <i class="demo-pli-pen-5"></i>
                                                    </a>
                                                    <button class="btn btn-sm btn-danger erase"
                                                            data-id="{{$info->id}}"
                                                            data-url="{{url('Offer/delete-offer')}}">
                                                        <i class="demo-pli-trash"></i>
                                                    </button>
                                                </td>
                                            </tr>
                                        @endforeach

                                        </tbody>
                                    </table>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>

        function openNav() {
            document.getElementById("myNav").style.height = "100%";
        }


        $(function(){
            $('#OfferTable').DataTable();
        });

        $(document).ready(function(){
            subcatload();

        });

        function subcatload(){
            var category_id = $("#category_id").val();
            $.ajax({
                method:"post",
                url:"{{ route('purchase.subcategory') }}",
                data:{category_id:category_id,"_token":"{{ csrf_token() }}"},
                dataType:"html",
                success:function(response){
                    $("#sub_category_id").html(response);
                },
                error:function(err){
                    console.log(err);
                }
            });
        }

        /* select Category and load sub-category automatically start */
        $(document).on('change','#category_id',function(){
            product_load();
            var category_id = $(this).val();
            $.ajax({
                method:"post",
                url:"{{ route('purchase.subcategory') }}",
                data:{category_id:category_id,"_token":"{{ csrf_token() }}"},
                dataType:"html",
                success:function(response){
                    $("#sub_category_id").html(response);
                },
                error:function(err){
                    console.log(err);
                }
            });
        });
        /* select category and load sub-category automatically end */

        function product_load(){
            var category_id = $("#category_id").val();
            $.ajax({
                method:"post",
                url:"{{ route('load.product') }}",
                data:{cat_id:category_id, subcat_id: 0, "_token":"{{ csrf_token() }}"},
                dataType:"html",
                success:function(response){
                    $("#product_id").html(response);
                },
                error:function(err){
                    console.log(err);
                }
            });
        }
        $(document).on('change','#sub_category_id',function(){
            var sub_category_id = $(this).val();

            $.ajax({
                method:"post",
                url:"{{ route('load.product') }}",
                data:{cat_id: 0, subcat_id:sub_category_id,"_token":"{{ csrf_token() }}"},
                dataType:"html",
                success:function(response){
                    $("#product_id").html(response);
                },
                error:function(err){
                    console.log(err);
                }
            });
        });




        function closeNav() {
            document.getElementById("myNav").style.height = "0%";
        }

    </script>



@endsection