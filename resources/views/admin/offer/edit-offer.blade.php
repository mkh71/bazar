@extends('admin.layouts.admin')

@section('content')
    <div class="boxed">
        <div id="content-container">
            <div id="page-head">
                <div id="page-title">
                    <h1 class="page-header text-overflow">Edit Offer</h1>
                </div>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="demo-pli-home"></i></a></li>
                    <li><a href="#">Admin</a></li>
                    <li class="active">Offer</li>
                </ol>
            </div>
            <div id="page-content">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel">
                            <div class="panel-heading">
                                <h3 class="panel-title">Edit Offer</h3>
                            </div>
                            <div class="panel-body">
                                <div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
                                    @if(Session::has('exist'))
                                        <div class="has-error">
                                            <strong class="help-block">
                                                <span> {{ Session::get('exist') }}</span>
                                            </strong>
                                        </div>
                                    @endif



                                        {{ Form::open(['route'=>'offer.update','method'=>'post', 'enctype'=>'multipart/form-data',]) }}

                                        <div class="col-lg-12 col-sm-12 {{$errors->has('name') ? 'has-error' : ''}}">
                                            {{ Form::label('Offer','Offer Name : ',['class'=>'control-label'])}}
                                            {{Form::text('name',$offer->name,['class'=>'form-control','placeholder'=>'Ex: Seasonal Offer'])}}
                                            @if ($errors->has('name'))
                                                <span class="help-block">
                                                 <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                            @endif
                                        </div>

                                        <div class="col-lg-12 col-sm-12 {{$errors->has('offer') ? 'has-error' : ''}}">
                                            {{ Form::label('Offer','Offer : ',['class'=>'control-label'])}}
                                            <div class="row">
                                                <div class="col-lg-6 col-sm-12">
                                                    {{Form::number('offer',$offer->offer,['class'=>'form-control col-4','placeholder'=>'Ex: 200Tk / 5% '])}}
                                                </div>
                                                <div class="col-lg-6 col-sm-12">
                                                    <label class="checkbox-inline">
                                                        <input type="radio" name="offerType" value='%'> %
                                                    </label>
                                                    <label class="checkbox-inline">
                                                        <input type="radio" name="offerType" value="taka"> Taka
                                                    </label>
                                                </div>
                                            </div>
                                            @if ($errors->has('offer'))
                                                <span class="help-block">
                                                 <strong>{{ $errors->first('offer') }}</strong>
                                            </span>
                                            @endif
                                        </div>

                                        <div class="col-lg-12 col-sm-12 {{$errors->has('image') ? 'has-error' : ''}}">
                                            {{ Form::label('','Offer Image : ',['class'=>'control-label'])}}
                                            {{Form::file('image',['class'=>'form-control'])}}
                                            @if ($errors->has('image'))
                                                <span class="help-block">
                                                 <strong>{{ $errors->first('image') }}</strong>
                                            </span>
                                            @endif
                                            <br>
                                        </div>


                                        <div class="col-lg-12 col-sm-12 {{$errors->has('description') ? 'has-error' : ''}}">
                                            {{ Form::label('','Description : ',['class'=>'control-label'])}}
                                            {{Form::textarea('description',$offer->description, ['class'=>'form-control','placeholder'=>'About offer/ Optional'])}}
                                            @if ($errors->has('description'))
                                                <span class="help-block">
                                                 <strong>{{ $errors->first('description') }}</strong>
                                            </span>
                                            @endif
                                        </div>

                                        {{Form::hidden('id', $offer->id ,[''])}}

                                        <div class="col-md-12 col-xs-12">
                                            <br>
                                        </div>

                                        <div class="col-md-12 col-xs-12">
                                            {{ Form::button('UPDATE OFFER',['type'=>'submit','id'=>'','class'=>'col-sm-5 btn btn-primary']) }}
                                             <a href="{{ route('offer.index') }}" class="btn btn-danger">Cancel Edit</a>
                                        </div>
                                    {{ Form::close() }}
                                    </div>
                                    <hr>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
@endsection