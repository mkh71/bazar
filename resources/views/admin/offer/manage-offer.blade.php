@extends('admin.layouts.admin')
@section('css')
    <style>
        body {
            font-family: 'Lato', sans-serif;
        }

        .overlay {
            height: 0%;
            width: 100%;
            position: fixed;
            z-index: 1;
            top: 0;
            left: 0;
            background-color: rgb(0,0,0);
            background-color: rgba(0,0,0, 0.9);
            overflow-y: hidden;
            transition: 0.5s;
        }

        .overlay-content {
            position: relative;
            top: 25%;
            width: 100%;
            text-align: center;
            margin-top: 30px;
        }

        .overlay a {
            padding: 8px;
            text-decoration: none;
            font-size: 36px;
            color: #818181;
            display: block;
            transition: 0.3s;
        }

        .overlay a:hover, .overlay a:focus {
            color: #f1f1f1;
        }

        .overlay .closebtn {
            position: absolute;
            top: 20px;
            right: 45px;
            font-size: 60px;
        }

        @media screen and (max-height: 450px) {
            .overlay {overflow-y: auto;}
            .overlay a {font-size: 20px}
            .overlay .closebtn {
                font-size: 40px;
                top: 15px;
                right: 35px;
            }
        }
    </style>
@stop
@section('content')
    <div class="boxed">
        <div id="content-container">
            <div id="page-head">
                <div id="page-title">
                    <h1 class="page-header text-overflow">Create / View Offer</h1>
                </div>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="demo-pli-home"></i></a></li>
                    <li><a href="#">Admin</a></li>
                    <li class="active">Manage Offer</li>
                </ol>
            </div>
            <div id="page-content">
                <div class="row">
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">Create New Offer</h3>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                                    {{ Form::open(['route'=>'create.offer','method'=>'post' ,'files'=>true]) }}

                                    <div class="row">
                                        <div class="col-md-4 col-lg-4 col-sm-6 ">
                                            <div class="form-group">
                                                {{ Form::label('Offer','Offer : ',['class'=>'control-label'])}}

                                                {{ Form::select('offer_id', ['0' => 'Select Offer*'] + $offers->toArray(), 0, ['class'=>'form-control' ,'id'=>'offer_id', 'required'=>'required']) }}
                                                @if($errors->has('offer_id'))
                                                    <span class="help-block">
                                                <strong>{{$errors->first('offer_id')}}</strong>
                                            </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-lg-4 col-sm-6 ">
                                            <div class="form-group">
                                                {{ Form::label('Category','Category : ',['class'=>'control-label'])}}

                                                {{ Form::select('category_id', ['0' => 'Select Category*'] + $categories->toArray(), 0, ['class'=>'form-control' ,'id'=>'category_id', 'required'=>'required']) }}
                                                @if($errors->has('category_id'))
                                                    <span class="help-block">
                                                <strong>{{$errors->first('category_id')}}</strong>
                                            </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="col-md-4 col-lg-4 col-sm-6 ">
                                            <div class="form-group">
                                                {{ Form::label('Category','Sub-Category : ',['class'=>'control-label'])}}

                                                {{ Form::select('subcategory_id',[''],false,['class'=>'form-control','id'=>'sub_category_id']) }}
                                                @if($errors->has('subcategory_id'))
                                                    <span class="help-block">
                                                <strong>{{$errors->first('subcategory_id')}}</strong>
                                            </span>
                                                @endif
                                            </div>
                                        </div>

                                    </div>

                                    <div class="row">
                                        <div class="col-md-4 col-lg-4 col-sm-6 ">
                                            <div class="form-group">
                                                {{ Form::label('start','Offer Start : ',['class'=>'control-label'])}}

                                                {{ Form::date('start', null, ['class'=>'form-control' ,'id'=>'category_id date']) }}
                                                @if($errors->has('start'))
                                                    <span class="help-block">
                                                <strong>{{$errors->first('start')}}</strong>
                                            </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-lg-4 col-sm-6 ">
                                            <div class="form-group">
                                                {{ Form::label('expire','Offer expire : ',['class'=>'control-label'])}}

                                                {{ Form::date('expire', null, ['class'=>'form-control' ,'id'=>'category_id date']) }}
                                                @if($errors->has('expire'))
                                                    <span class="help-block">
                                                <strong>{{$errors->first('expire')}}</strong>
                                            </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                                            {{ Form::label('','Product : ',['class'=>'control-label text-left'])}}
                                            <div class="form-group">
                                                    <span id="product_id"></span>

                                                    @if($errors->has('product_id'))
                                                        <span class="help-block">
                                                            <strong>{{$errors->first('product_id')}}</strong>
                                                        </span>
                                                    @endif
                                            </div>
                                        </div>
                                    </div>
<br>
                                        {{ Form::button('Product Offer',['type'=>'submit', 'class'=>'col-sm-5 btn btn-primary', 'id'=>"openNav"]) }}
                                    </div>

                                    {{ Form::close() }}
                                </div>
                            <hr>
                            <div class="row">
                                <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                                    <table class="table table-bordered table-striped" id="OfferTable">
                                        <thead>
                                        <tr>
                                            <th>SL</th>
                                            <th>Offer Name</th>
                                            <th>Product Name</th>
                                            <th>Product Price</th>
                                            <th>Offer</th>
                                            <th>Offer Price</th>
                                            <th>Offer Expire</th>
                                            <th>offer Image</th>
                                            <th>Action </th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @php $i=0; @endphp
                                        @foreach($offer_product as $info)
                                            <tr id="rowid{{$info->id}}" class="abcd">
                                                <td>{{++$i}}</td>

                                                <td> {{$info->offer->name}}</td>
                                                <td> {{$info->product->name}}</td>
                                                <td> {{$info->product->price}}</td>
                                                <td>
                                                    {{$info->offer->offer}}
                                                    {{$info->offer->type == 1 ? 'Taka' : '%'}}
                                                </td>
                                                <td> {{$info->offer_price}} </td>
                                                <td>
                                                    @if($info->expire == null)
                                                        Unlimited Time
                                                    @elseif($info->expire < date('Y-m-d h:i:s'))
                                                        <span class="text-danger">Expired</span>
                                                    @else
                                                        Till - {{date('d-m-Y | H:s:i', strtotime($info->expire))}}
                                                    @endif
                                                </td>

                                                <td>
                                                    <img src="{{ asset('public/admin/offer/offer_product/'.$info->image) }}" alt="no image" style="height: 80px;width: 80px;">
                                                </td>

                                                <td width="5%">
                                                    <button class="btn btn-sm btn-danger erase"
                                                            data-id="{{$info->id}}"
                                                            data-url="{{route('offer_product.delete')}}">
                                                        <i class="demo-pli-trash"></i>
                                                    </button>
                                                </td>
                                            </tr>
                                        @endforeach

                                        </tbody>
                                    </table>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(function(){
            $('#OfferTable').DataTable();
        });

        $(document).ready(function(){
            subcatload();

        });

        function subcatload(){
            var category_id = $("#category_id").val();
            $.ajax({
                method:"post",
                url:"{{ route('purchase.subcategory') }}",
                data:{category_id:category_id,"_token":"{{ csrf_token() }}"},
                dataType:"html",
                success:function(response){
                    $("#sub_category_id").html(response);
                },
                error:function(err){
                    console.log(err);
                }
            });
        }

        /* select Category and load sub-category automatically start */
        $(document).on('change','#category_id',function(){
            product_load();
            var category_id = $(this).val();
            $.ajax({
                method:"post",
                url:"{{ route('purchase.subcategory') }}",
                data:{category_id:category_id,"_token":"{{ csrf_token() }}"},
                dataType:"html",
                success:function(response){
                    $("#sub_category_id").html(response);
                },
                error:function(err){
                    console.log(err);
                }
            });
        });
        /* select category and load sub-category automatically end */

        function product_load(){
            var category_id = $("#category_id").val();
            $.ajax({
                method:"post",
                url:"{{ route('load.product') }}",
                data:{cat_id:category_id, subcat_id: 0, "_token":"{{ csrf_token() }}"},
                dataType:"html",
                success:function(response){
                    $("#product_id").html(response);
                },
                error:function(err){
                    console.log(err);
                }
            });
        }
        $(document).on('change','#sub_category_id',function(){
            var sub_category_id = $(this).val();

            $.ajax({
                method:"post",
                url:"{{ route('load.product') }}",
                data:{cat_id: 0, subcat_id:sub_category_id,"_token":"{{ csrf_token() }}"},
                dataType:"html",
                success:function(response){
                    $("#product_id").html(response);
                },
                error:function(err){
                    console.log(err);
                }
            });
        });
    </script>



@endsection