@extends('admin.layouts.admin')
@section('title','Add About')
@section('content')
    <div class="boxed">
        <div id="content-container">
            <div id="page-head">
                <div id="page-title">
                    <h1 class="page-header text-overflow">Add About/Mission/Vission</h1>
                </div>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="demo-pli-home"></i></a></li>
                    <li><a href="#">Admin</a></li>
                    <li class="active">About/Mission/Vission</li>
                </ol>
            </div>
            <div id="page-content">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel">
                            <div class="panel-heading">
                                <h3 class="panel-title">About/Mission/Vission   </h3>
                            </div>
                            <div class="panel-body">
                                <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                                    <div class="col-lg-4 col-sm-4  {{$errors->has('name') ? 'has-error' : ''}}">
                                        {{ Form::open(['route'=>'about.store','method'=>'post','enctype'=>'multipart/form-data','id'=>'brandForm']) }}
                                        {{ Form::label('brand','Name : ',['class'=>'control-label'])}}
                                        {{Form::text('name',old('name'),['class'=>'form-control','placeholder'=>'Ex: Electronics Tools'])}}

                                        @if ($errors->has('name'))
                                            <span class="help-block">
                                                 <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                        @endif
                                        <br>
                                    </div>

                                    <div class="col-lg-4 col-sm-4 {{ $errors->has('type') ? 'has-error' : '' }} ">
                                        <label>SELECT ABOUT / MISION / VISSION / Why Choose Us </label>
                                        {{ Form::select('type',['1'=>"About US",'2'=>'Mission','3'=>'Vission',"4"=>"Why Choose Us"],false,['class'=>'form-control']) }}
                                        <br>

                                        @if($errors->has('type'))
                                            <span class="help-block">
                                                <strong> {{ $errors->first('type') }} </strong>
                                            </span>
                                        @endif

                                    </div>

                                    <div class="col-lg-4 col-sm-4  {{$errors->has('image') ? 'has-error' : ''}}">

                                        {{ Form::label('brand','Image : ',['class'=>'control-label'])}}
                                        {{ Form::file('image',['class'=>'form-control'])}}
                                        @if ($errors->has('image'))
                                            <span class="help-block">
                                                 <strong>{{ $errors->first('image') }}</strong>
                                            </span>
                                        @endif
                                        <br>
                                    </div>

                                    <div class="col-lg-12 col-sm-12 {{$errors->has('description') ? 'has-error' : ''}}">

                                        {{ Form::label('brand','Description : ',['class'=>'control-label'])}}

                                        {!! Form::textarea('description',old('description'),['class'=>'form-control', 'rows' => 6, 'cols' => 40,'placeholder'=>' Details.......','id'=>'demo-summernote']) !!}
                                        @if ($errors->has('description'))
                                            <span class="help-block">
                                                 <strong>{{ $errors->first('description') }}</strong>
                                            </span>
                                        @endif

                                    </div>

                                    <div class="col-md-12 col-xs-12">
                                        {{ Form::button('SAVE ',['type'=>'submit','id'=>'savebrand','class'=>'col-sm-5 btn btn-primary']) }}
                                    </div>
                                    {{ Form::close() }}
                                    <hr>
                                </div>

                                <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 table-responsive">
                                    <hr>
                                    <table class="table table-bordered table-striped" id="brandTable">
                                        <thead>
                                        <tr>
                                            <th>SL</th>
                                            <th>Name</th>
                                            <th>Type</th>
                                            <th>Image</th>
                                            <th>Description</th>
                                            <th>Action </th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            @php $i=0; @endphp
                                            @foreach($about as $info)
                                                <tr id="rowid{{$info->id}}" class="abcd">
                                                    <td>{{++$i}}</td>
                                                    <td >{{$info->name}}</td>
                                                    <td>
                                                        @if($info->type == 1)
                                                            About us
                                                        @elseif($info->type ==2)
                                                            Mission
                                                        @else
                                                            Vission
                                                        @endif
                                                    </td>
                                                    <td >
                                                        <img src="{{ asset('public/admin/service/'.$info->image) }}" style="height: 60px;width: 60px;">
                                                    </td>
                                                    <td >{{ substr(strip_tags($info->description),0,30)}}</td>
                                                    <td>
                                                        <a class="btn btn-sm btn-info edit" href="{{route('about.edit',$info->id)}}" ><i class="demo-pli-pen-5"></i></a> ||
                                                        <button class="btn btn-sm btn-danger erase" data-id="{{$info->id}}" data-url="{{url('WebManagement/erase-about')}}"><i class="demo-pli-trash"></i></button>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<script>
    $(function(){
        $('#brandTable').DataTable();
        $("#canceledit").hide();
        $(".note-codable").attr("name","description");
        });
</script>


@endsection
