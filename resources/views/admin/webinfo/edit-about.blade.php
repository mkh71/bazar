@extends('admin.layouts.admin')

@section('title','Edit-About')
@section('content')
    <div class="boxed">
        <div id="content-container">
            <div id="page-head">
                <div id="page-title">
                    <h1 class="page-header text-overflow">Update About/Mission/Vission</h1>
                </div>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="demo-pli-home"></i></a></li>
                    <li><a href="#">Admin</a></li>
                    <li class="active">About/Mission/Vission</li>
                </ol>
            </div>
            <div id="page-content">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel">
                            <div class="panel-heading">
                                <h3 class="panel-title">Add New About/Mission/Vission</h3>
                            </div>
                            <div class="panel-body">
                                <div class="col-lg-8 col-sm-8 col-md-8 col-xs-12">
                                    <div class="col-lg-12 col-sm-12 {{$errors->has('name') ? 'has-error' : ''}}">
                                        {{ Form::model($about,['route'=>'about.update','method'=>'post','enctype'=>'multipart/form-data','id'=>'brandForm']) }}
                                        {{ Form::label('brand','Name : ',['class'=>'control-label'])}}
                                        {{Form::text('name',old('name'),['class'=>'form-control','placeholder'=>'Ex: Electronics Tools'])}}
                                        {{ Form::hidden('id',$about->id) }}

                                        @if ($errors->has('name'))
                                            <span class="help-block">
                                                 <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                        @endif
                                        <br>
                                    </div>

                                    <div class="col-lg-12 col-sm-12 col-xs-12 {{ $errors->has('type') ? 'has-error' : '' }}">
                                        <label>SELECT ABOUT / MISION / VISSION</label>
                                        {{ Form::select('type',['1'=>"About US",'2'=>'Mission','3'=>'Vission'],false,['class'=>'form-control']) }}
                                        <br>
                                        
                                        @if($errors->has('type'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('type') }}</strong>
                                            </span>
                                        @endif
                                        
                                    </div>



                                    <div class="col-lg-12 col-sm-12 {{$errors->has('image') ? 'has-error' : ''}}">
                                        <BR>
                                        {{ Form::label('brand','Old Image : ',['class'=>'control-label'])}}
                                        <img src="{{ asset('public/admin/service/'.$about->image) }}" style="height: 60px;width: 60px;">
                                    </div>




                                    <div class="col-lg-12 col-sm-12 {{$errors->has('image') ? 'has-error' : ''}}">
                                        <BR>
                                        {{ Form::label('brand','Image : ',['class'=>'control-label'])}}
                                        {{ Form::file('image',['class'=>'form-control'])}}
                                        @if ($errors->has('image'))
                                            <span class="help-block">
                                                 <strong>{{ $errors->first('image') }}</strong>
                                            </span>
                                        @endif
                                    </div>

                                    <div class="col-lg-12 col-sm-12 {{$errors->has('description') ? 'has-error' : ''}}">
                                        <BR>
                                        {{ Form::label('brand','Service Description : ',['class'=>'control-label'])}}
                                        {!! Form::textarea('description',old('description'),['class'=>'form-control', 'rows' => 6, 'cols' => 40,'placeholder'=>' Details.......','id'=>'demo-summernote']) !!}
                                        @if ($errors->has('description'))
                                            <span class="help-block">
                                                 <strong>{{ $errors->first('description') }}</strong>
                                            </span>
                                        @endif
                                    </div>



                                    <div class="col-md-12 col-xs-12">
                                        <br>
                                    </div>

                                    <div class="col-md-12 col-xs-12">
                                        {{ Form::button('UPDATE ABOUT',['type'=>'submit','id'=>'savebrand','class'=>'col-sm-5 btn btn-primary']) }}
                                        || <a href="{{ route('about.add') }}" class="btn btn-danger">Edit Cancel</a>
                                    </div>
                                    {{ Form::close() }}
                                </div>


                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>

        $(function(){
            $('#brandTable').DataTable();
            $("#canceledit").hide();
        });
    </script>

@endsection