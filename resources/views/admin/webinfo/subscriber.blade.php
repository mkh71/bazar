@extends('admin.layouts.admin')
@section('title','Subscriber | MTR Engineering')
@section('content')
    <div class="boxed">
        <div id="content-container">
            <div id="page-head">
                <div id="page-title">
                    <h1 class="page-header text-overflow">All Subscribers</h1>
                </div>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="demo-pli-home"></i></a></li>
                    <li><a href="#">Admin</a></li>
                    <li class="active">Subscriber</li>
                </ol>
            </div>
            <div id="page-content">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel">
                            <div class="panel-heading">
                                <h3 class="panel-title">Subscriber List </h3>
                            </div>
                            <div class="panel-body">
                                

                                <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 table-responsive">
                                    <hr>
                                    <table class="table table-bordered table-striped" id="brandTable">
                                        <thead>
                                        <tr>
                                            <th>SL</th>
                                            <th>Email</th>
                                            <th>Subscribed Day</th>
                                            <th>Subscribed with US</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            @php $i=0; @endphp
                                            @foreach($subscribers as $subscriber)
                                                <tr >
                                                    <td>{{++$i}}</td>
                                                    <td >{{$subscriber->email}}</td>
                                                    <td >{{$subscriber->created_at}}</td>
                                                    <td >{{$subscriber->created_at->diffForHumans()}}</td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<script>
    $(function(){
        $('#brandTable').DataTable();
        $("#canceledit").hide();
        $(".note-codable").attr("name","description");
        });
</script>


@endsection