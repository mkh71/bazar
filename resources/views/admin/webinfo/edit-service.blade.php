@extends('admin.layouts.admin')
@section('title','EDIT-SERVICE | HD Exp')
@section('content')
    <div class="boxed">
        <div id="content-container">
            <div id="page-head">
                <div id="page-title">
                    <h1 class="page-header text-overflow">Add / View Service</h1>
                </div>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="demo-pli-home"></i></a></li>
                    <li><a href="#">Admin</a></li>
                    <li class="active">Service</li>
                </ol>
            </div>
            <div id="page-content">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel">
                            <div class="panel-heading">
                                <h3 class="panel-title">Add New Service</h3>
                            </div>
                            <div class="panel-body">
                                <div class="col-lg-8 col-sm-8 col-md-8 col-xs-12">
                                    <div class="col-lg-12 col-sm-12 {{$errors->has('name') ? 'has-error' : ''}}">
                                        {{ Form::model($service,['route'=>['service.update',$service->id],'method'=>'post','enctype'=>'multipart/form-data','id'=>'brandForm']) }}
                                        {{ Form::label('brand','Name : ',['class'=>'control-label'])}}
                                        {{Form::text('name',old('name'),['class'=>'form-control','placeholder'=>'Ex: Electronics Tools'])}}

                                        @if ($errors->has('name'))
                                            <span class="help-block">
                                                 <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                        @endif
                                    </div>

                                    <div class="col-lg-12 col-sm-12 {{$errors->has('image') ? 'has-error' : ''}}">
                                    <BR>
                                        {{ Form::label('brand','Old Image : ',['class'=>'control-label'])}}
                                        <img src="{{ asset('public/admin/service/'.$service->image) }}" style="height: 60px;width: 60px;">
                                    </div>

                                    <div class="col-lg-12 col-sm-12 {{$errors->has('image') ? 'has-error' : ''}}">
                                        <BR>
                                        {{ Form::label('brand','Image : ',['class'=>'control-label'])}}
                                        {{ Form::file('image',['class'=>'form-control'])}}
                                        @if ($errors->has('image'))
                                            <span class="help-block">
                                                 <strong>{{ $errors->first('image') }}</strong>
                                            </span>
                                        @endif
                                    </div>

                                    <div class="col-lg-12 col-sm-12 {{$errors->has('description') ? 'has-error' : ''}}">
                                        {{ Form::label('','Service Description : ',['class'=>'control-label'])}}

                                        {!! Form::textarea('description',old('description'),['class'=>'form-control', 'rows' => 6, 'cols' => 40,'placeholder'=>'Service Details.......','id'=>'demo-summernote']) !!}
                                        @if ($errors->has('description'))
                                            <span class="help-block">
                                             <strong>{{ $errors->first('description') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                    <div class="col-md-12 col-xs-12">
                                        <br>
                                    </div>

                                    <div class="col-md-12 col-xs-12">
                                        {{ Form::button('SAVE SERVICE',['type'=>'submit','id'=>'savebrand','class'=>'col-sm-5 btn btn-primary']) }}
                                        || <a href="{{ route('service.add') }}" class="btn btn-danger">Edit Cancel</a>
                                    </div>
                                    {{ Form::close() }}
                                </div>


                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>

        $(function(){
            $('#brandTable').DataTable();
            $("#canceledit").hide();
        });
    </script>

@endsection