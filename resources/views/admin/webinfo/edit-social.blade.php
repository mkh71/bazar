@extends('admin.layouts.admin')
@section('title','Edit-Social | MTR Engineering CO.')
@section('content')
    <div class="boxed">
        <div id="content-container">
            <div id="page-head">
                <div id="page-title">
                    <h1 class="page-header text-overflow">Add / View Social</h1>
                </div>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="demo-pli-home"></i></a></li>
                    <li><a href="#">Admin</a></li>
                    <li class="active">Social</li>
                </ol>
            </div>
            <div id="page-content">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel">
                            <div class="panel-heading">
                                <h3 class="panel-title">Add New Social</h3>
                            </div>
                            <div class="panel-body">
                                <div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
                                    {{ Form::model($social,['route'=>['social.update',$social->id],'method'=>'post','enctype'=>'multipart/form-data','id'=>'brandForm']) }}
                                    @include('admin.webinfo.social-form')
                                    <div class="col-md-12 col-xs-12">
                                        <br>
                                    </div>

                                    <div class="col-md-12 col-xs-12">
                                        {{ Form::submit('UPDATE SOCIAL',['class'=>'btn btn-primary']) }}
                                    </div>
                                    {{ Form::close() }}
                                </div>

                                <div class="col-lg-8 col-sm-8 col-md-8 col-xs-12">
                                    <table class="table table-bordered table-striped" id="brandTable">
                                        <thead>
                                        <tr>
                                            <th>SL</th>
                                            <th>Name</th>
                                            <th>Type</th>
                                            <th>Action </th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @php $i=0; @endphp
                                        @foreach($socials as $info)
                                            <tr id="rowid{{$info->id}}" class="abcd">
                                                <td>{{++$i}}</td>
                                                <td >{{$info->name}}</td>
                                                <td>
                                                    @if($info->type==1)
                                                        <label class="label label-success">Facebook</label>
                                                    @elseif($info->type ==2)
                                                        <label class="label label-success">Twitter</label>
                                                    @elseif($info->type ==3)
                                                        <label class="label label-success">Instagram</label>
                                                    @elseif($info->type ==4)
                                                        <label class="label label-success">LinkedIn</label>
                                                    @elseif($info->type ==5)
                                                        <label class="label label-success">Google Plus</label>
                                                    @endif
                                                </td>
                                                <td>
                                                    <a class="btn btn-sm btn-info edit" href="{{ route('social.edit',$info->id) }}" ><i class="demo-pli-pen-5"></i></a> ||
                                                    <button class="btn btn-sm btn-danger erase" data-id="{{ $info->id }}" data-url="{{url('WebManagement/erase-social')}}"><i class="demo-pli-trash"></i></button>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>

        $(function(){
            $('#brandTable').DataTable();
            $("#canceledit").hide();
        });
    </script>

@endsection