<div class="col-lg-12 col-sm-12 {{$errors->has('social') ? 'has-error' : ''}}">

    {{ Form::label('','Social Link : ',['class'=>'control-label'])}}
    {{Form::select('type',['1'=>'Facebook','2'=>'Twitter','3'=>'Instagram','4'=>'LinkedIn','5'=>'Google Plus'],null,['class'=>'form-control'])}}
    @if ($errors->has('social'))
        <span class="help-block">
             <strong>{{ $errors->first('social') }}</strong>
        </span>
    @endif
</div>

<div class="col-lg-12 col-sm-12 {{$errors->has('name') ? 'has-error' : ''}}">

    {{ Form::label('','Social Name : ',['class'=>'control-label'])}}
    {{ Form::text('name',old('social'),['class'=>'form-control','placeholder'=>"https://facebook.com/UserLink"])}}
    @if ($errors->has('name'))
        <span class="help-block">
             <strong>{{ $errors->first('name') }}</strong>
        </span>
    @endif
</div>