@php $i=0; @endphp
@foreach($socials as $info)
    <tr id="rowid{{$info->id}}" class="abcd">
        <td>{{++$i}}</td>
        <td >{{$info->name}}</td>
        <td>
            @if($info->type==1)
                <label class="label label-success">Facebook</label>
            @elseif($info->type ==2)
                <label class="label label-success">Twitter</label>
            @elseif($info->type ==3)
                <label class="label label-success">Instagram</label>
            @elseif($info->type ==4)
                <label class="label label-success">LinkedIn</label>
            @elseif($info->type ==5)
                <label class="label label-success">Google Plus</label>
            @endif
        </td>
        <td>
            <a class="btn btn-sm btn-info edit" href="{{ route('social.edit',$info->id) }}" ><i class="demo-pli-pen-5"></i></a> ||
            <button class="btn btn-sm btn-danger erase" data-id="{{ $info->id }}" data-url="{{url('WebManagement/erase-social')}}"><i class="demo-pli-trash"></i></button>
        </td>
    </tr>
@endforeach