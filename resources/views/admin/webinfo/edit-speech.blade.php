@extends('admin.layouts.admin')
@section('title','EDIT-SPEECH | HD Exp')
@section('content')
    <div class="boxed">
        <div id="content-container">
            <div id="page-head">
                <div id="page-title">
                    <h1 class="page-header text-overflow">Add / View Service</h1>
                </div>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="demo-pli-home"></i></a></li>
                    <li><a href="#">Admin</a></li>
                    <li class="active">Service</li>
                </ol>
            </div>
            <div id="page-content">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel">
                            <div class="panel-heading">
                                <h3 class="panel-title">Add New Service</h3>
                            </div>
                            <div class="panel-body">
                                <div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
                                    <div class="col-lg-12 col-sm-12 {{$errors->has('name') ? 'has-error' : ''}}">
                                        {{ Form::model($speech,['route'=>'speech.update','method'=>'post','enctype'=>'multipart/form-data','id'=>'brandForm']) }}
                                        {{ Form::label('brand','Select Person : ',['class'=>'control-label'])}}

                                        {{ Form::select('team_id',$team,true,['class'=>'form-control']) }}
                                        {{ Form::hidden('id',$speech->id) }}
                                        @if ($errors->has('team_id'))
                                            <span class="help-block">
                                                 <strong>{{ $errors->first('team_id') }}</strong>
                                            </span>
                                        @endif
                                    </div>


                                    <div class="col-lg-12 col-sm-12 {{$errors->has('signature') ? 'has-error' : ''}}">
                                        <br>
                                        {{ Form::label('brand','Signature : ',['class'=>'control-label'])}}
                                        {{ Form::file('signature',['class'=>'form-control'])}}
                                        @if ($errors->has('signature'))
                                            <span class="help-block">
                                                 <strong>{{ $errors->first('signature') }}</strong>
                                            </span>
                                        @endif
                                    </div>

                                    <div class="col-lg-12 col-sm-12">
                                        <br>
                                        {{ Form::label('signature',"Old Signature",['class'=>'label-control']) }}
                                        <img src="{{ asset('public/admin/service/'.$speech->signature) }}" style="height: 80px;width: 80px;">
                                    </div>

                                    <div class="col-lg-12 col-sm-12 {{$errors->has('description') ? 'has-error' : ''}}">
                                        <br>
                                        {{ Form::label('brand','Speech Description : ',['class'=>'control-label'])}}
                                        {{ Form::textarea('description',old('description'),['class'=>'form-control','rows'=>'5','cols'=>5,'placeholder'=>'Speech Details.....']) }}
                                        @if ($errors->has('description'))
                                            <span class="help-block">
                                                 <strong>{{ $errors->first('description') }}</strong>
                                            </span>
                                        @endif
                                    </div>



                                    <div class="col-md-12 col-xs-12">
                                        <br>
                                    </div>

                                    <div class="col-md-12 col-xs-12">
                                        {{ Form::button('SAVE SPEECH',['type'=>'submit','id'=>'savebrand','class'=>'col-sm-5 btn btn-primary']) }}
                                        || <a href="{{ route('speech.add') }}" class="btn btn-danger">Edit Cancel</a>
                                    </div>
                                    {{ Form::close() }}
                                </div>


                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>

        $(function(){
            $('#brandTable').DataTable();
            $("#canceledit").hide();
        });
    </script>

@endsection