@extends('admin.layouts.admin')

@section('content')
    <div class="boxed">
        <div id="content-container">
            <div id="page-head">
                <div id="page-title">
                    <h1 class="page-header text-overflow">Add / View Team</h1>
                </div>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="demo-pli-home"></i></a></li>
                    <li><a href="#">Admin</a></li>
                    <li class="active">Team</li>
                </ol>
            </div>
            <div id="page-content">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel">
                            <div class="panel-heading">
                                <h3 class="panel-title">Add New Team</h3>
                            </div>
                            <div class="panel-body">
                                <div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
                                    <div class="col-lg-12 col-sm-12 {{$errors->has('name') ? 'has-error' : ''}}">
                                        {{ Form::open(['route'=>'team.store','method'=>'post','enctype'=>'multipart/form-data','id'=>'brandForm']) }}
                                        {{ Form::label('brand','Name : ',['class'=>'control-label'])}}
                                        {{Form::text('name',old('name'),['class'=>'form-control','placeholder'=>'Ex: Mikimoto'])}}

                                        @if ($errors->has('name'))
                                            <span class="help-block">
                                                 <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                        @endif
                                    </div>

                                    <div class="col-lg-12 col-sm-12 {{$errors->has('designation') ? 'has-error' : ''}}">

                                        {{ Form::label('brand','Designation : ',['class'=>'control-label'])}}
                                        {{ Form::text('designation',old('designation'),['class'=>'form-control','placeholder'=>'Ex: CEO'])}}

                                        @if ($errors->has('designation'))
                                            <span class="help-block">
                                                 <strong>{{ $errors->first('designation') }}</strong>
                                            </span>
                                        @endif
                                    </div>

                                    <div class="col-lg-12 col-sm-12 {{$errors->has('image') ? 'has-error' : ''}}">

                                        {{ Form::label('brand','Image : ',['class'=>'control-label'])}}
                                        {{ Form::file('image',['class'=>'form-control'])}}
                                        @if ($errors->has('image'))
                                            <span class="help-block">
                                                 <strong>{{ $errors->first('image') }}</strong>
                                            </span>
                                        @endif
                                    </div>

                                    <div class="col-lg-12 col-sm-12 {{$errors->has('facebook') ? 'has-error' : ''}}">

                                        {{ Form::label('brand','Facebook : ',['class'=>'control-label'])}}
                                        {{ Form::text('facebook',old('facebook'),['class'=>'form-control','placeholder'=>"https://facebook.com/UserLink"])}}
                                        @if ($errors->has('facebook'))
                                            <span class="help-block">
                                                 <strong>{{ $errors->first('facebook') }}</strong>
                                            </span>
                                        @endif
                                    </div>


                                    <div class="col-lg-12 col-sm-12 {{$errors->has('twitter') ? 'has-error' : ''}}">

                                        {{ Form::label('brand','Twitter : ',['class'=>'control-label'])}}
                                        {{ Form::text('twitter',old('twitter'),['class'=>'form-control','placeholder'=>"https://twitter.com/UserLink"])}}
                                        @if ($errors->has('twitter'))
                                            <span class="help-block">
                                                 <strong>{{ $errors->first('twitter') }}</strong>
                                            </span>
                                        @endif
                                    </div>

                                    <div class="col-lg-12 col-sm-12 {{$errors->has('instagram') ? 'has-error' : ''}}">

                                        {{ Form::label('brand','Instagram : ',['class'=>'control-label'])}}
                                        {{ Form::text('instagram',old('instagram'),['class'=>'form-control','placeholder'=>"https://instagram.com/UserLink"])}}
                                        @if ($errors->has('instagram'))
                                            <span class="help-block">
                                                 <strong>{{ $errors->first('instagram') }}</strong>
                                            </span>
                                        @endif
                                    </div>

                                    <div class="col-lg-12 col-sm-12 {{$errors->has('google') ? 'has-error' : ''}}">

                                        {{ Form::label('brand','Google+ : ',['class'=>'control-label'])}}
                                        {{ Form::text('google',old('google'),['class'=>'form-control','placeholder'=>"https://google.com/UserLink"])}}
                                        @if ($errors->has('google'))
                                            <span class="help-block">
                                                 <strong>{{ $errors->first('google') }}</strong>
                                            </span>
                                        @endif
                                    </div>


                                    <div class="col-md-12 col-xs-12">
                                        <br>
                                    </div>

                                    <div class="col-md-12 col-xs-12">
                                        {{ Form::button('SAVE TEAM',['type'=>'submit','id'=>'savebrand','class'=>'col-sm-5 btn btn-primary']) }}
                                    </div>
                                    {{ Form::close() }}
                                </div>

                                <div class="col-lg-8 col-sm-8 col-md-8 col-xs-12">
                                    <table class="table table-bordered table-striped" id="brandTable">
                                        <thead>
                                        <tr>
                                            <th>SL</th>
                                            <th>Name</th>
                                            <th>Image</th>
                                            <th>Designation</th>
                                            <th>Action </th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @php $i=0; @endphp
                                        @foreach($teams as $info)
                                            <tr id="rowid{{$info->id}}" class="abcd">
                                                <td>{{++$i}}</td>
                                                <td id="brand{{$info->id}}">{{$info->name}}</td>
                                                <td id="brand{{$info->id}}">
                                                    <img src="{{ asset('public/admin/team/'.$info->image) }}" style="height: 60px;width: 60px;">
                                                </td>
                                                <td id="brand{{$info->id}}">{{$info->designation}}</td>
                                                <td>
                                                    <a class="btn btn-sm btn-info edit" href="{{route('team.edit',$info->id)}}" ><i class="demo-pli-pen-5"></i></a> ||
                                                    <button class="btn btn-sm btn-danger erase" data-id="{{$info->id}}" data-url="{{url('WebManagement/erase')}}"><i class="demo-pli-trash"></i></button>
                                                </td>
                                            </tr>
                                        @endforeach

                                        </tbody>
                                    </table>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>

        $(function(){
            $('#brandTable').DataTable();
            $("#canceledit").hide();
        });
    </script>

@endsection