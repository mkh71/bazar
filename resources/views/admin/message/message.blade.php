@extends('admin.layouts.admin')

@section('content')
    <div class="boxed">
        <div id="content-container">
            <div id="page-head">
                <div id="page-title">
                    <h1 class="page-header text-overflow">View Messages</h1>
                </div>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="demo-pli-home"></i></a></li>
                    <li><a href="#">Admin</a></li>
                    <li class="active">Messages</li>
                </ol>
            </div>
            <div id="page-content">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel">
                            <div class="panel-heading">
                                <h3 class="panel-title">Message</h3>
                            </div>
                            <div class="panel-body">
                                <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                                    <table class="table table-bordered table-striped" id="brandTable">
                                        <thead>
                                        <tr>
                                            <th>SL</th>
                                            <th>Name</th>
                                            <th>Phone </th>
                                            <th>Email</th>
                                            <th>Suggestion</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @php $i=0; @endphp
                                        @foreach($messages as $info)
                                            <tr id="rowid{{$info->id}}" class="abcd">
                                                <td>{{++$i}}</td>
                                                <td id="name{{ $info->id }}" data-id="{{ $info->name }}">{{$info->name}}</td>
                                                <td id="phone{{ $info->id }}" data-id="{{ $info->phone }}">{{$info->phone}}</td>
                                                <td id="address{{ $info->id }}" data-id="{{ $info->email }}">
                                                        {{ $info->email }}
                                                </td>
                                                <td id="address{{ $info->id }}" data-id="{{ $info->email }}">
                                                    {{ $info->details }}
                                                </td>

                                            </tr>
                                        @endforeach

                                        </tbody>
                                    </table>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>

        $(function(){
            $('#brandTable').DataTable();
            $("#canceledit").hide();
        });


        /* edit request end */
    </script>

@endsection