@extends('front.layouts.master')
@section('title',$helpers->titles()->where('page',1)->first())
@section('content')
    {{-- Slider --}}
    @include('front.include.slider')

    <div class="container">
        <div class="row">
            <div class="title">
                <h2>Products & Services</h2>
            </div>
        </div>
        <div class="row">
            @php($products= \App\Product::all())
            @foreach($products as $product)
                <div class="col-md-4 about">
                    <div class="clearfix"></div>
                    <p>
                    <h5>{{$product->name}} </h5>
                    <img src="{{asset('admin/product/'.$product->image)}}" height="120px" width="100px" />
                    </p>
                    <div class="button">
                        <a class="hvr-bounce-to-right btn" href="#">Add to cart</a>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
    {{-- About us --}}
    @include('front.include.about')

@endsection
