@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    @Can('IsAdmin')
                        You are logged in as {{Auth::user()->usertype}} !
                    @endcan



                    @Can('IsUser')
                        Sorry Champ You are user. You Cant visit until admin Approval {{ Auth::user()->usertype }}
                    @endcan

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
