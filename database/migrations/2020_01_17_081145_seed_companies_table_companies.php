<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SeedCompaniesTableCompanies extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('companies', function (Blueprint $table) {
            \App\Company::create([
                'phone'=>"+880123456789" ,
                'phone1'=> "+880123456789",
                'mail'=>"hdexpress@gmail.com" ,
                'address'=> "2No gate, CDA Avenue, East Nasirabad, Chattogram.",
                'facebook'=> "https://facebook.com/hdexpress" ,
                'twitter'=>"https://facebook.com/hdexpress" ,
                'instagram'=> "https://facebook.com/hdexpress",
                'linkedin'=> "https://facebook.com/hdexpress",
                'gmail'=> "hdexpress@gmail.com",
                'whatsapp'=> null,
                'logo'=> null,
                'favicon'=> null,
                'mobile_logo'=> null,
                'footer_logo'=> null,
                'name'=> "HD Express",
                'download'=>null
           ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('companies', function (Blueprint $table) {
            //
        });
    }
}
