<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOfferProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offer_products', function (Blueprint $table){
            $table->increments('id');
            $table->unsignedInteger('offer_id');
            $table->unsignedInteger('product_id');
            $table->float('offer_price', 8, 2);
            $table->string('image')->nullable();
            $table->dateTime('start')->nullable();
            $table->dateTime('expire')->nullable();
            $table->boolean('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offer_products');
    }
}
