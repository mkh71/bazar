<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SeedRolesAndUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('roles', function (Blueprint $table) {

            $roles = ['Admin','User'];
            foreach ($roles as $role){
                \App\Role::query()->create([
                    'name'=>$role
                ]);
            }

            /* User Assign */

            \App\UserRole::query()->create(['user_id'=>1,'role_id'=>1]);


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('roles', function (Blueprint $table) {
            //
        });
    }
}
