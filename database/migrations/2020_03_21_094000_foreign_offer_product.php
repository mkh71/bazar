<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ForeignOfferProduct extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('offer_products', function (Blueprint $table){
           $table->foreign('offer_id')
               ->references('id')
               ->on('offers')
               ->onDelete('cascade');
           $table->foreign('product_id')
               ->references('id')
               ->on('products')
               ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('offer_products', function (Blueprint $table){
           $table->dropForeign('offer_products');
        });
    }
}
