<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('order_no')->nullable();
            $table->unsignedInteger('user_id')->nullable();
            $table->string('total')->nullable();
            $table->string('qty')->nullable();
            $table->string('discount')->nullable();
            $table->integer('isDelivered')->default(1);
            $table->integer('isReady')->default(0);
            $table->integer('isCancel')->default(0);
            $table->string('delivered_date')->nullable();
            $table->string('ready_date')->nullable();
            $table->string('cancel_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
