<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ForeignSubcatSubcatChild extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('subcat_children', function (Blueprint $table){
           $table->foreign('subcategory_id')
               ->references('id')
               ->on('subcategories')
               ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('subcat_children', function (Blueprint $table){
            $table->dropForeign('subcat_children');
        });
    }
}
