<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $fillable = ['name'];

    public function menus(){
        return $this->belongsToMany( Menu::class,'menu_roles');
    }
}
