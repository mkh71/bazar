<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderShip extends Model
{
    protected $table = 'order_ships';
    protected $fillable = ['order_id', 'user_id', 'mobile', 'district', 'thana', 'address'];
}
