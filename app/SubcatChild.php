<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubcatChild extends Model
{
    protected $fillable = ['category_id', 'subcategory_id', 'image', 'description', 'advertise_image'];
}
