<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

class CheckPermit
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $auth = Auth::user();
        $route= Route::current()->getName();
        foreach ($auth->roles as $key=>$role){
            foreach ($role->menus as $menu){
                if ($menu->route == $route){
                    return $next($request);
                }
            }
        }
        abort('404');
    }
}
