<?php

namespace App\Http\Controllers\frontend;

use App\About;
use App\Message;
use App\Product;
use App\Service;
use App\Slider;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;
use App\SubCategory;

use App\helper\Helpers\Helper;

class BasicController extends Controller
{
    /*public $helpers;

    public function __construct()
    {
        $this->helpers = new Helper();
    }*/

    public function index(){
       $data['sliders'] = Slider::orderBy('id','desc')->get();
       //$data['helpers'] = $this->helpers;

       return view('front.pages.index')->with($data);
    }

    public function Contact(){
        return view('finalfront.contact');
    }

    public function singleservice($id){
        $service = Service::findOrFail($id);
        $services = Service::all();
        return view('finalfront.visit-service',compact('service','services'));
    }

    public function product(){
        $categories = Category::all();
        return view('finalfront.product',compact('categories'));
    }

    public function singleproduct(){
    	return view("front.product");
    }

    public function CategoryProductVisit($id){

       $category = Category::findOrFail($id);
       $products = Product::where('category_id',$id)->get();

       return view('finalfront.category-product',compact('products','category'));
    }


    public function interior(){
    	return view("front.interior");
    }


    public function businesstool(){
    	return view("front.business");
    }



    public function ledlight(){
    	return view("front.led");
    }

    public function substation(){
    	return view("front.substation");
    }

    public function firesafty(){
    	return view("front.safety");
    }

    public function medicure(){
    	return view("front.medicure");
    }

    public function repair(){
        return view("front.repair");
    }



    public function AboutMtr(){
        $categories  = Category::all();
        $abouts = About::all();

        return view('finalfront.about',compact('categories','abouts'));
    }




    public function industrial(){
        return view("front.industrial");
    }

    public function VisitorInbox(Request $request){
     //   dd($request->all());
        $this->validate($request,[
            'name'=>'required|string',
            'phone'=>'required|string',
            'email'=>'required|email',
            'details'=>'required'
        ]);

        Message::create($request->all());

        return view('front.success');
    }

    public function Messages(){
        $messages = Message::all();
        return view('admin.message.message',compact('messages'));
    }

    public function SubscribersStore(Request $request){
        $this->validate($request,[
            'email'=>"required|unique:subscribers|email",
        ]);

        Subscriber::create($request->all());
        session()->flash("subscriber","Subcription Complete");
        return redirect()->route("/");
    }

    public function category($id){
        $category = Category::findOrFailOrFail($id);
        return view("finalfront.visit-category",compact('category'));
    }


    public function categoryProducts($id){

        $product = Product::findOrFail($id);
        $category = Category::findOrFail($product->category_id);
        return view('finalfront.single-product',compact('product','category'));
    }

    public function visitSingleProduct($id){
        $product = Product::findOrFailOrFail($id);
        return view('finalfront.single-product',compact('product'));
    }

}
