<?php

namespace App\Http\Controllers\frontend;

use App\Category;
use App\helper\Helpers\Helper;
use App\Product;
use App\SubCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductViewController extends Controller
{
 //    public $helpers;

    public function __construct()
    {
     //   $this->helpers = new Helper();
    }

    public function CategoryVisit($id){
        $category = Category::query()->findOrFail($id);
        $sub_cats = SubCategory::query()->where('category_id', $id)->get();
        $products = Product::query()->where('category_id', $id)->get();
        /*$helpers = $this->helpers;*/

        return view('front.pages.view-subcategory', compact('category', 'sub_cats', 'products'));
    }

    public function ProductVisit($id){
        $data['category'] = Category::query()->findOrFail($id);
        $data['sub_cats'] = SubCategory::query()->where('category_id', $id)->get();
        $data['products'] = Product::query()->where('category_id', $id)->get();

        return view('front.pages.view-product')->with($data);
    }
}

