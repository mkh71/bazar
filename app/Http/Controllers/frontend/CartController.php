<?php

namespace App\Http\Controllers\frontend;

use App\Cart;
use App\Order;
use App\OrderProduct;
use App\OrderShip;
use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Session;
class CartController extends Controller
{
    public function carts(){
        //Session::forget('cart');
        if (Session::has('cart')){
            $oldCart = Session::get('cart');
            $cart = new Cart($oldCart);
            $list='';
            $productIds = [];
            $productQtys = [];
            if ($cart->items == null ){
                $cart->totalPrice = $cart->totalPrice * 0;
                $cart->totalQty = $cart->totalQty * 0;
                $data['items'] = '<tr> <td style="width: 30%"> <p class="bg-danger btn btn-danger btn-block text-center">No Items found </p> </td> </tr>';
                $data['totalAmount']= 0;
                $data['totalItem']= 0;
                $data['isEmpty'] = true;
                return json_encode($data);
            }else{
                $output = '';
                $sl = 0;
                foreach ($cart->items as $tid=>$info){

                    $qty = $info['item']['qty'];

                    $productIds[$sl]['product_id'] = $info['item']['product_id'];
                    $productQtys[$sl]['qty'] = $qty;
                    ++$sl;
                    $name = $info['item']['name'];
                    $img = $info['item']['img'];
                    $price = $info['item']['price'];

                    $product = Product::query()->findOrFail($info['item']['product_id']);

                    $output.='
                            <tr>
                                <td>
                                    <div class="input-group cart-bdy row">
                                        <span
                                            class="plus-item col-md-12 text-success fas fa-angle-up input-group-addon"
                                            data-name="'.$product->name.'">
                                        </span>
                                        <span
                                            type="text"
                                            class="col-md-12 mx-auto item-count form-control"
                                            data-name="'.$product->name.'"
                                            >
                                            '.$info['item']['qty'].'
                                        </span>
                                        <span
                                            class="col-md-12 minus-item input-group-addon text-danger fas fa-angle-down"
                                            data-name="'.$product->name.'"
                                            >
                                        </span>
                                        </div>
                                    </td>
                                    <td>
                                       <img class="img-fluid cart-product-image" src="'.$img.'">
                                    </td>
                                    <td>
                                        <div class="row">
                                        <div class="col-md-12">
                                            <strong>'.$name.'</strong>
                                        </div>
                                        <div class="col-md-12">
                                                <small>'.$price.'</small>
                                            <!--/<small>200 gm</small>-->
                                        </div>
                                        </div>
                                    </td>
                                    <td
                                        class="font-weight-bold">
                                        '.$price*$qty.' ৳
                                    </td>
                                    <td>
                                    <button
                                    type="button"
                                    class="btn destroyItem"
                                    data-id="'.$product->id.'"
                                    >
                                        <i class="fas fa-trash-alt text-danger"></i>
                                    </button>
                                    </td>
                                </tr>';

                }

                $data['items']= $output;
                $data['totalAmount'] = $cart->totalPrice;
                $data['totalItem']= count($cart->items);
                $data['products'] = $productIds;
                $data['qtys'] = $productQtys;
                $data['isEmpty'] = false;
                //dd($cart);
                return json_encode($data);
            }

        }else{
            $data['items'] = '<tr> <td class="bg-danger text-center">No Items found</td> </tr>';
            $data['totalAmount']= 0;
            $data['totalItem']= 0;
            $data['isEmpty'] = false;
            return json_encode($data);
        }
    }

    public function updateCart(Request $request){
        $id = $request->product_id;
        if (Session::has('cart')){
            $oldCart = Session::get('cart');
            $cart = new Cart($oldCart);
            $cart->updateCart($id);
            session()->put('cart',$cart);
        }
        return json_encode(['success'=>true]);
    }

    public function isCartEmpty(){
        if (session()->has('cart')){
            $cart = new Cart(session()->get('cart'));
            $data['success'] = ($cart->totalPrice <0 ? 1 : 0 );
            return json_encode($data);
        }
    }/* Method End*/


    public function removeItem(Request $request){
        $id = $request->product_id;
        if (Session::has('cart')){
            $oldCart = Session::get('cart');
            $cart = new Cart($oldCart);
            $cart->remove($id);
            $request->session()->put('cart',$cart);
        }
        return json_encode(['success'=>true]);
    }


    public function visitCart(){
        $data['info']      = $this->info;
        $data['slider']    = Slider::all();
        $data['tours']     = Tour::orderBy('created_at','desc')->limit(9)->get();

        $data['carts'] =  Session::get('cart');
        return view('frontend.pages.tour-cart')->with($data);

    }

    public function store(Request $request){

        $oldCart = Session::has('cart') ? Session::get('cart') : null;

        $cart = new Cart($oldCart);

        $id = $request->product_id;
        $cart->add($request->except('_token'),$id);
        $request->session()->put('cart',$cart);

        return json_encode(['success'=>1]);

    }


    public function locationStore(Request $request){
        $request->session()->put('locationStore',$request->except('_token'));
        return redirect()->route('cart.payment');
    }

    public function paymentPage(){
        return view('front.pages.order-4');
    }

    public function placeOrder(){
        return view('front.pages.order-5');
    }

    public function confirmOrder(Request $request){
        $user_id = Auth::user();
        $address = session('locationStore');

        $orderNo = 'SHO-'.substr(md5(time()),6);
        $isValid = Order::query()->where('order_no',$orderNo)->first();
        if (!$isValid){
            $orderNo.'1';
        }
        $cart ='';

        if (Session::has('cart')){
            $oldCart = Session::get('cart');
            $cart = new Cart($oldCart);
        }

        $order = Order::query()->create([
            'order_no'=>$orderNo,
            'user_id'=>Auth::id(),
            'total'=>$cart->totalPrice,
            'qty'=>$cart->totalQty // qty as item
        ]);

        $address['user_id'] = $user_id->id;
        $address['order_id'] = $order->id;

        OrderShip::query()->create($address); /* SHipping information saved */

        /* order Product Summary */

        foreach ($cart->items as $key=>$info){
            OrderProduct::query()->create([
                'order_id'=>$order->id,
                'product_id'=>$info['item']['product_id'],
                'qty'=>$info['item']['qty'],
                'price'=>$info['item']['price']
            ]);
            $cart->remove($info['item']['product_id']);
            $request->session()->put('cart',$cart);
        }

        /* order Product Summary Exit */
        session()->flash('success','Order successfully complete.');
        return redirect()->route('cart.placeorder');

    }

    public function order_product(){
        if (Auth::id()){
            return view('front.pages.order-2');
        }
        return view('front.pages.order');
    }

}
