<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Service;
use App\Http\Requests\ServiceRequest;

class ServiceController extends Controller
{

    public function index()
    {
        $services = Service::all();

        return view('admin.webinfo.add-service',compact('services'));
    }


    public function store(ServiceRequest $request)
    {
       
        $data=$request->except('image');
        
        if($request->hasFile('image')){
            $image = $request->file('image');
            $filename = Date('Y').'_'.substr(md5(time()),0,6).".".$image->getClientOriginalExtension();
            $image->move(base_path('public/admin/service/'),$filename);
            $data = $request->except('image');
            $data['image'] = $filename;
        }
        
        Service::create($data);
        session()->flash('success','service has store successfully');
        return redirect()->route('service.add');
    }

    public function show($id)
    {
        $service = Service::find($id);
        return view('admin.webinfo.edit-service',compact('service'));
    }

    public function update(Request $request,$id)
    {
        
        $update = Service::find($id);
        if( $request->hasFile('image'))
        {
            if($update->image!=null){
                /* old image unlink start */
                $path = 'public/admin/service/'.$update->image;
                unlink($path);
                /* old image unlink end */
            }

            $image = $request->file('image');
            $filename = Date('Y').'_'.substr(md5(time()),0,6).".".$image->getClientOriginalExtension();
            $image->move(base_path('public/admin/service/'),$filename);
            $data = $request->except('image');
            $data['image'] = $filename;
            $update->update($data);
        }else{
            $update->update($request->except('image'));
        }

        session()->flash('success','service has update Successfull');
        return redirect()->route('service.add');
    }


    public function destroy(Request $request)
    {
        /* old image unlink start */

        $delete = Service::find($request->id);
        if($delete->image !=null){
            $path = 'public/admin/service/'.$delete->image;
            unlink($path);
        }
        $delete->delete();
    }
}
