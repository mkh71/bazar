<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;


class BookingController extends Controller
{
    public $info;
    public $category;

    public function __construct()
    {
        App::setLocale('locale','bn');
        $info = SiteSetting::first();
        $this->info = $info;
    }


    public function index(Request $request){
        $child = $request->child;
        $per_child = $request->per_child;

        $adult = $request->adults;

        $per_adult = $request->per_adult;

        $kid = $request->kid;
        $per_kid = $request->per_kid;

        $student = $request->student;
        $per_student = $request->per_student;

        $tourDate = $request->tourDate;
        $tourTime = $request->tourTime;

        $total = ($child*$per_child)+($adult*$per_adult)+($kid*$per_kid)+($student*$per_student);

        $tour = Tour::findOrFail($request->tourId);

        $oldCart = Session::has('cart') ? Session::get('cart') : null;

        $cart = new Cart($oldCart);

        $cart->add($request->except('_token'),$request->tourId);

        $cartInfo = $request->session()->put('cart',$cart);

        return json_encode(['success'=>1]);

    }

    public function carts(){
        //Session::forget('cart');

        if (Session::has('cart')){
            $oldCart = Session::get('cart');
            $cart = new Cart($oldCart);
            $list='';
            if ($cart->totalPrice == 0){
                echo '<li> <p style="background: red;color: #fff;font-size: 1.2rem;margin: 0;text-align: center;padding: 5px 0;">Nothing for Checkout</p> </li>';

            }else{
                foreach ($cart->items as $tid=>$info){

                    $fair_price = ($info['item']['adults']*$info['item']['per_adult'])+
                        ($info['item']['child']*$info['item']['per_child'])+
                        ($info['item']['kid']*$info['item']['per_kid'])+
                        ($info['item']['student']*$info['item']['per_student']);

                    $name = $info['item']['tour_name'];
                    $img = $info['item']['img'];
                    $list.= '
                    <li class="lino'.$tid.'">
                        <div class="image" style="display: inline-block">
                            <img style="height: 37px;width: 60px;" src="'.$img.'" alt="image">
                        </div>
                        <strong><a href="#"> '.$name.' </a> </strong> <strong> Price : $ '.$fair_price.' </strong>
                        <a href="#" onclick="removeCartItem('.$tid.')" class="action icon-trash " data-id="'.$tid.'" style="background: red;color: #fff;font-size: 0.9rem;display: inline-block;height: 25px;width: 25px;text-align: center;padding: 3px 0 0 0;border-radius: 14px;margin: 10px 0 0 0" ></a>
                    </li>';
                }

                $list.='<li>
                        <div>Total: <span>$ '.$cart->totalPrice.'</span></div>
                        <a href="'.route('tour.cartvisit').'" class="button_drop">Go to cart</a>
                        <a href="'.route('checkout').'" class="button_drop outline">Check out</a>
                    </li>';

                return $list;
            }

        }else{
            echo '<li> <p style="background: red;color: #fff;font-size: 1.2rem;margin: 0;text-align: center;padding: 5px 0;">Nothing for Checkout</p> </li>';
        }


    }

    public function isCartEmpty(){
        if (session()->has('cart')){
            $cart = new Cart(session()->get('cart'));
            $data['success'] = ($cart->totalPrice <0 ? 1 : 0 );
            return json_encode($data);
        }
    }/* Method End*/


    public function removeItem(Request $request){
        $id = $request->id;
        if (Session::has('cart')){
            $oldCart = Session::get('cart');
            $cart = new Cart($oldCart);
            $cart->remove($id);
            $request->session()->put('cart',$cart);
        }
    }


    public function visitCart(){

        $data['info']      = $this->info;
        $data['slider']    = Slider::all();
        $data['tours']     = Tour::orderBy('created_at','desc')->limit(9)->get();

        $data['carts'] =  Session::get('cart');
        return view('frontend.pages.tour-cart')->with($data);

    }


    public function tourCheckout(){
        if (session()->has('cart')){
            $cart = new \App\Cart(session('cart'));
            if ($cart->totalPrice == 0){
                return redirect("/");
            }
        }

        $data['info']= $this->info;
        return view('frontend.checkout')->with($data);
    }

}
