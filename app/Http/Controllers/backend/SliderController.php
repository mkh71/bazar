<?php

namespace App\Http\Controllers\backend;

use App\Slider;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Image;

class SliderController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $sliders = Slider::all();
        return view('admin.slider.add-slider',compact('sliders'));
    }


    public function store(Request $request)
    {
        $this->validate($request,[
            'title'=>'required',
            'image'=>'required|mimes:jpeg,png,gif',
        ]);

        $data=$request->except('image');

        $file = $request->file('image');

        $original = Image::make($file);
        $data = $request->except('image');
        $filename = Date('Y')."_".substr(md5(time()),0,5).".".$file->getClientOriginalExtension();
        $path = public_path('slider/'.$filename);
        $data['image']=$filename;
        $original->resize(1920,600)->save($path); // resize(width,height)
        Slider::create($data);

        session()->flash('success','Slider Added ');
        return redirect()->route('slider.add');

    }


    public function edit($id)
    {
        $slider = Slider::findOrFail($id);
        $sliders= Slider::all();
        return view('admin.slider.edit-slider',compact('slider','sliders'));
    }


    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'title'=>'required',
            'image'=>'mimes:jpeg,png,gif',
        ]);

        $update = Slider::findOrFail($id);

        $data=$request->except('image');
        if($request->hasFile('image')){
            $path = "public/slider/".$update->image;
            @unlink($path);
            $file = $request->file('image');
            $original = Image::make($file);
            $data = $request->except('image');
            $filename = Date('Y')."_".substr(md5(time()),0,5).".".$file->getClientOriginalExtension();
            $path = public_path('slider/'.$filename);
            $data['image']=$filename;
            $original->resize(1351,567)->save($path); // resize(width,height)

        }
        $update->update($data);
        session()->flash('success','Slider Updated');
        return redirect()->route('slider.add');
    }


    public function destroy(Request $request)
    {
        $delete = Slider::findOrFail($request->id);
        @unlink("public/slider/".$delete->image);
        $delete->delete();
    }
}
