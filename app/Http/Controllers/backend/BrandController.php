<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Brand;
class BrandController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $brands= Brand::all();
        return view('admin.brand.add-brand',compact('brands'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name'=>'required|string|unique:brands',
        ]);

        $brand = new Brand();
        $brand->name = $request->name;
        $brand->save();
        session()->flash('success','Brand has store Successfully');
        return redirect()->route('brand.add');
    }

    public function show($id)
    {
        $brand = Brand::find($id);
        return view('admin.brand.edit-brand',compact('brand'));
    }


    public function update(Request $request)
    {
        $this->validate($request, [
            'name'=>'required|string|unique:brands',
        ]);
        $brand =Brand::find($request->id);
        $brand->name = $request->name;
        $brand->update();
        session()->flash('success','Brand has Update Successfully');
        return redirect()->route('brand.add');

    }

    public function destroy(Request $request)
    {
        $delete = Brand::find($request->id);
        $delete->delete();
    }
}
