<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Title;
use Illuminate\Validation\Rule;

class TitleController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){

        $data['titles'] = Title::orderBy('id','desc')->get();
        return view('admin.title.add-title')->with($data);
    }

    public function store(Request $request){
      $this->validate($request,[
          'name'=>['required'],
          'page'=>'required|unique:titles'
      ]);

      Title::create($request->all());

      return redirect()->route('title.add')->with('success','title successfully saved');
    }

    public function show($id){
        $title = Title::findOrFail($id);
        return view('admin.title.edit-title',compact('title'));
    }

    public function update(Request $request,$id){
        $this->validate($request,[
            'name'=>'required',
            'page'=>['required',Rule::unique('titles')->ignore($id)]
        ]);

        Title::findOrFail($id)->update($request->all());
        return redirect()->route('title.add')->with('success','title successfully updated');

    }

    public function destroy(Request $request){
         Title::findOrFail($request->id)->delete();
    }
}
