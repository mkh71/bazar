<?php

namespace App\Http\Controllers\backend;

use App\Role;
use App\User;
use App\Warehouse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class UserController extends Controller
{
    public function index(){
        $roles = Role::pluck('name','id');
        $role = Role::all();
        $users = User::all();

        return view('admin.user-management.users',compact('users','roles','role'));
    }
    public function create()
    {
        $roles = Role::all()->pluck('name','id');
        return view('user.create',compact('roles'));
    }

    public function store(Request $request)
    {

        $user_data = $request->except('role_id');

        $user_data['password'] = bcrypt($request->password);

        $user_data['r_password'] = $request->password;

        $user = User::query()->create($user_data);

        $user->roles()->attach($request->role_id);


        Session::flash('success','User created successfully');

        return redirect()->route('user.create');
    }

    public function edit($id)
    {
        $roles = Role::all();
        $user = User::query()->findOrFail($id);
        $role_ids = [];
        foreach($user->roles as $role)
        {
            $role_ids[] = $role->id;
        }

        return view('admin.user-management.edit-users',compact('user','roles','role_ids'));
    }

    public function update($id, Request $request)
    {
        $data = $request->except(['role_id']);
        $data['password'] = bcrypt($request->password);
        $data['r_password'] = $request->password;
        $user = User::query()->findOrFail($id);
        $user->update($data);
        $user->roles()->sync($request->role_id);

        Session::flash('success','User has been updated!');
        return redirect()->route('user.create');
    }

    public function destroy(Request $request)
    {
        $delete = User::find($request->id);
        $delete->roles()->detach();
        $delete->delete();
    }
}
