<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Social;
use Illuminate\Validation\Rule;

class SocialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $socials = Social::all();
        return view('admin.webinfo.social',compact('socials'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name'=>'required|unique:socials',
            'type'=>'required'
        ]);
        Social::create($request->all());
        session()->flash("success","Social Link Stored");
        return redirect()->route('social.add');
    }


    public function edit($id)
    {
        $social = Social::findOrFail($id);
        $socials = Social::all();
        return view('admin.webinfo.edit-social',compact('social','socials'));
    }


    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name'=>['required',Rule::unique('socials')->ignore($id)],
            'type'=>'required'
        ]);
        Social::findOrFail($id)->update($request->all());
        session()->flash("success","Social Link Updated");
        return redirect()->route('social.add');
    }


    public function destroy(Request $request)
    {
        Social::findOrFail($request->id)->delete();
        session()->flash("success","Social Link Deleted");
        return redirect()->route('social.add');
    }
}
