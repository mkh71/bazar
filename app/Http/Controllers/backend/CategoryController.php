<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Category;
use Illuminate\Validation\Rule;

class CategoryController extends Controller
{
    public function __construct(){
        $this->middleware("auth");
    }
    
    public function index()
    {
        $category= Category::all();
        return view('admin.category.add-category',compact('category'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name'=>'required|string|unique:categories',
            //'image'=>'image|mimes:jpeg,jpg,png',
        ]);

        $category = $request->except(['image', 'adv_image']);
        if($request->hasFile('image')){
            $image = $request->file('image');
            $filename = Date('Y').'_'.substr(md5(time()),0,6).'.'.$image->getClientOriginalExtension();
            $image->move(base_path('public/admin/category/'),$filename);
            $category['image']=$filename;
        }

        if($request->hasFile('adv_image')){
            $image = $request->file('adv_image');
            $filename = Date('Y').'_'.substr(md5(time()),0,6).'.'.$image->getClientOriginalExtension();
            $image->move(base_path('public/admin/category/advertise'),$filename);
            $category['advertise_image']=$filename;
        }

       // dd($category);

        Category::create($category);
        session()->flash('success','Category has store Successfully');
        return redirect()->route('category.add');
    }

    public function show($id)
    {
        $category = Category::findOrFail($id);
        return view('admin.category.edit-category',compact('category'));
    }


    public function update(Request $request)
    {
        $this->validate($request, [
            'name'=>['required',Rule::unique('categories')->ignore($request->id)],
            'image'=>'image|mimes:jpeg,jpg,png',
        ]);
        
        $category = Category::findOrFail($request->id);
        $save = $request->except(['image', 'adv_image']);

        if($request->hasFile('image')){
            $image = $request->file('image');
            $filename = Date('Y').'_'.substr(md5(time()),0,6).'.'.$image->getClientOriginalExtension();
            $image->move(base_path('public/admin/category/'),$filename);
            $category['image']=$filename;
        }

        if($request->hasFile('adv_image')){
            $image = $request->file('adv_image');
            $filename = Date('Y').'_'.substr(md5(time()),0,6).'.'.$image->getClientOriginalExtension();
            $image->move(base_path('public/admin/category/advertise'),$filename);
            $category['advertise_image']=$filename;
        }


        $category->update($save);
        
        session()->flash('success','Category has Update Successfully');
        return redirect()->route('category.add');
    }

    public function destroy(Request $request)
    {
        $delete = Category::findOrFail($request->id);
        if($delete->image != null){
            $path = 'public/admin/category/'.$delete->image;
            unlink($path);

            $path = 'public/admin/category/advertise'.$delete->adv_image;
            unlink($path);
        }
        $delete->delete();
    }
}
