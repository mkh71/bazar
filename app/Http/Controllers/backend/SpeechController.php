<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Speech;
use App\Http\Requests\SpeechRequest;
use App\Team;

class SpeechController extends Controller
{
    public function index()
    {
        $speeches = Speech::all();
        $team = Team::pluck('name','id');
        return view('admin.webinfo.add-speech',compact('speeches','team'));
    }


    public function store(SpeechRequest $request)
    {
        $image = $request->file('signature');
        $filename = Date('Y').'_'.substr(md5(time()),0,6).".".$image->getClientOriginalExtension();
        $image->move(base_path('public/admin/service/'),$filename);
        $data = $request->except('signature');
        $data['signature'] = $filename;
        Speech::query()->create($data);
        session()->flash('success','speech has store successfully');
        return redirect()->route('speech.add');
    }

    public function show($id)
    {
        $team = Team::pluck('name','id');
        $speech = Speech::find($id);
        return view('admin.webinfo.edit-speech',compact('speech','team'));
    }


    public function update(SpeechRequest $request)
    {
        $update = Speech::find($request->id);
        if( $request->hasFile('signature'))
        {
            /* old image unlink start */
            $path = 'public/admin/service/'.$update->signature;
            unlink($path);
            /* old image unlink end */
            $image = $request->file('signature');
            $filename = Date('Y').'_'.substr(md5(time()),0,6).".".$image->getClientOriginalExtension();
            $image->move(base_path('public/admin/service/'),$filename);
            $data = $request->except('signature');
            $data['signature'] = $filename;
            $update->update($data);
        }else{
            $update->update($request->except('signature'));
        }

        session()->flash('success','speech has update Successfull');
        return redirect()->route('speech.add');
    }


    public function destroy(Request $request)
    {
        /* old image unlink start */
        $delete = Speech::find($request->id);
        $path = 'public/admin/service/'.$delete->signature;
        unlink($path);
        $delete->delete();
    }
}
