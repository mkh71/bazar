<?php

namespace App\Http\Controllers\backend;

use App\Faq;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;

class FaqController extends Controller
{
    public function __construct(){
        $this->middleware("auth");
    }

    public function index()
    {
        $faq= Faq::all();
        return view('admin.faq.add-faq',compact('faq'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'title'=>'required|string|unique:faqs',
        ]);
        Faq::create($request->all());
        session()->flash('success','faq has store Successfully');
        return redirect()->route('faq.add');
    }

    public function show($id)
    {
        $faq = Faq::findOrFail($id);
        $faqs = Faq::orderBY('id','desc')->get();
        return view('admin.faq.edit-faq',compact('faq','faqs'));
    }

    public function update(Request $request,$id)
    {
        $this->validate($request, [
            'title'=>['required',Rule::unique('faqs')->ignore($id)],
        ]);

         Faq::findOrFail($id)->update($request->all());

        session()->flash('success','faq has Update Successfully');
        return redirect()->route('faq.add');
    }

    public function destroy(Request $request)
    {
        $delete = Faq::findOrFail($request->id);
        if($delete->image != null){
            $path = 'public/admin/product/testing/'.$delete->image;
            unlink($path);
        }
        $delete->delete();
    }
}
