<?php

namespace App\Http\Controllers\backend;

use App\Company;
use App\ImageProduct;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use Image;

class SettingController extends Controller
{
    public function edit($id){
        $company = Company::findOrFail($id);

        return view('admin.setting.edit-setting',compact('company'));
    }

    public function update(Request $request,$id){
        $company = Company::findOrFail($id);
        $data = $request->except(['logo','favicon','mobile_logo','footer_logo']);
        /* Main Logo */
        if ($request->hasFile('logo')){
            if ($company->logo !=null){
                $path = "public/admin/company/".$company->logo;
                @unlink($path);
            }

            $sizes = [252,72,1]; // First = Width  = 72, Second = Height : 252
            $data['logo'] = $this->imageProcess($request->file('logo'),$sizes);
        }

        /* FavIcon Logo */
        if ($request->hasFile('favicon')){
             if ($company->favicon !=null){
                $path = "public/admin/company/".$company->favicon;
                @unlink($path);
            }

            $sizes = [36,20,2]; // First = Width  = 72, Second = Height : 252
            $data['favicon'] = $this->imageProcess($request->file('favicon'),$sizes);
        }


        /* Mobile Logo */
        if ($request->hasFile('mobile_logo')){
             if ($company->mobile_logo !=null){
                $path = "public/admin/company/".$company->mobile_logo;
                @unlink($path);
            }

            $sizes = [252,72,3]; // First = Width  = 72, Second = Height : 252
            $data['mobile_logo'] = $this->imageProcess($request->file('mobile_logo'),$sizes);
        }


        /* Footer Logo */
        if ($request->hasFile('footer_logo')){
             if ($company->footer_logo !=null){
                $path = "public/admin/company/".$company->footer_logo;
                @unlink($path);
            }


            $sizes = [252,72,4]; // First = Width  = 72, Second = Height : 252
            $data['footer_logo'] = $this->imageProcess($request->file('footer_logo'),$sizes);
        }

        $company->update($data);
        session()->flash('success','Company Profile Updated');
        return redirect()->route('company.edit',1);

    }

    public function imageProcess($image,$sizes){

        $image_name = substr(md5(time()),0,6);

        $filename = Date('Y').'_'.$image_name.$sizes[2].".".$image->getClientOriginalExtension();

        $original = Image::make($image);

        $path = public_path('admin/company/'.$filename);

        $original->resize($sizes[0],$sizes[1])->save($path); // resize(width,height)
        return $filename;
    }

}
