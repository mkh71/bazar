<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\About;
use App\Http\Requests\AboutRequest;
use App\Subscriber;
use Illuminate\Validation\Rule;
class MtrController extends Controller
{

    public function __construct(){
        $this->middleware("auth");
    }

    public function index()
    {
        $about = About::all();
        return view('admin.webinfo.add-about',compact('about'));
    }


    public function store(Request $request)
    {

        $this->validate($request,[
            'name'=>'required',
            'type'=>['required'],
            'description'=>'required',
            'image'=>'image|mimes:jpg,jpeg,png'
        ]);

        $data = $request->except('image');

        if($request->hasFile('image')){
            $image = $request->file('image');
            $filename = Date('Y').'_'.substr(md5(time()),0,6).".".$image->getClientOriginalExtension();
            $image->move(base_path('public/admin/service/'),$filename);

            $data['image'] = $filename;
        }

        About::create($data);

        if($request->type ==1 ){
            $type="About us";
        }

        if($request->type ==2 ){
            $type="Mission";
        }

        if($request->type ==3 ){
            $type="Vission ";
        }

        if($request->type ==4 ){
            $type="Why Choose Us ";
        }

        session()->flash('success',$type.' has store successfully');
        return redirect()->route('about.add');
    }

    public function show($id)
    {
        $about = About::find($id);
        return view('admin.webinfo.edit-about',compact('about'));
    }


    public function update(Request $request)
    {
        $this->validate($request,[
            'name'=>'required',
            'type'=>['required',Rule::unique('abouts')->ignore($request->id)],
            'description'=>'required',
            'image'=>'image|mimes:jpg,jpeg,png'
        ]);


        $update = About::find($request->id);
        if( $request->hasFile('image'))
        {
            if($update->image !=null){
                 /* old image unlink start */
                    $path = 'public/admin/service/'.$update->image;
                    unlink($path);
                /* old image unlink end */
            }

            $image = $request->file('image');
            $filename = Date('Y').'_'.substr(md5(time()),0,6).".".$image->getClientOriginalExtension();
            $image->move(base_path('public/admin/service/'),$filename);
            $data = $request->except('image');
            $data['image'] = $filename;
            $update->update($data);

        }else{

            $update->update($request->except('image'));

        }

        if($request->type ==1 ){
            $type="About us";
        }

        if($request->type ==2 ){
            $type="Mission";
        }

        if($request->type ==3 ){
            $type="Vission ";
        }

        session()->flash('success',$type.' has update successfully');
        return redirect()->route('about.add');
    }


    public function destroy(Request $request)
    {
        /* old image unlink start */
        $delete = About::find($request->id);
        $path = 'public/admin/service/'.$delete->image;
        unlink($path);
        $delete->delete();
    }

    public function subscriberList(){
        $subscribers = Subscriber::all();
        return view('admin.webinfo.subscriber',compact('subscribers'));
    }
}
