<?php

namespace App\Http\Controllers\backend;

use App\Category;
use App\Offer;
use App\OfferProduct;
use App\Product;
use App\SubCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OfferController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $offers = Offer::all();
        return view('admin.offer.add-offer', compact('offers'));
    }

    public function store(Request $request){

        $this->validate($request, [
            'offer' => 'required',
            'type' => 'required',
            'image' => 'image|mimes:jpeg, jpg, png',
        ]);

        if ($request->hasFile('image')){
            $offer = $request->except('image');

            $image = time().$request->file('image')->getClientOriginalExtension();
            $request->file('image')->move(base_path('public/admin/offer'),$image);
            $offer['image'] = $image;


            Offer::query()->create($offer);
        }else{
            Offer::query()->create($request->all());
        }

        session()->flash('success', 'Offer has been created successfully');
        return redirect()->route('offer.index');
    }

    public function edit($id){
        $offer = Offer::query()->findOrFail($id);
        return view('admin.offer.edit-offer', compact('offer'));
    }

    public function update(Request $request){

        $this->validate($request, [
            'offer' => 'required',
            'type' => 'required',
            'image' => 'image|mimes:jpeg, jpg, png',
        ]);

        $offer_id = Offer::query()->findOrFail($request->id);
        if ($request->hasFile('image')){
            $offer = $request->except('image');
            $image = time().$request->file('image')->getClientOriginalExtension();
            $request->file('image')->move(base_path('public/admin/offer'),$image);
            $offer['image'] = $image;
            $offer_id->update($offer);
        }else{
            $offer_id->update($request->all());
        }

        session()->flash('success', 'Offer has been Updated successfully');
        return redirect()->route('offer.index');
    }

    public function destroy(Request $request)
    {
        $delete = Offer::query()->findOrFail($request->id);
        if($delete->image != null){
            $path = 'public/admin/offer/'.$delete->image;
            unlink($path);
        }
        $delete->delete();
    }

    public function manage(){
        $offers = Offer::query()->pluck('name','id');
        $categories = Category::query()->pluck('name' ,'id');

        $offer_product = OfferProduct::query()
            ->orderBy('id', 'desc')
            ->get();
        return view('admin.offer.manage-offer', compact('offers', 'categories', 'offer_product'));
    }

    public function load_product(Request $request){
        $products = Product::query()
            ->where('category_id', $request->cat_id)
            ->orWhere('subcategory_id', $request->subcat_id)
            ->get();

        foreach ($products as $product){
            echo '
                <div class="form-inline col-md-4">
                    <div class="">
                          <input type="checkbox" name="product_id[]" value="'.$product->id.'" class="form-control">
                          <span class="text-info">'.$product->name.'</span> <br>
                          <input type="file" name="image[]" placeholder="image for Advertise" class="form-control">
                    </div>
                </div>
            ';
        }
    }

    public function create_offer(Request $request){
        $this->validate($request, [
            'offer_id' => 'required',
            'product_id' => 'required',
        ]);
        $data = $request->except(['product_id', 'image']);
        $offer = Offer::query()->findOrFail($request->offer_id);

        $products = $request->product_id;
        $images = $request->image;
      //  dd($products, $images);

        foreach ($products as $idx => $product){

            /* image upload process */

            $product_price = Product::query()->findOrFail($product)->price;
            if ($offer->type == 2){ //type 2 = % & type 1 = taka.
                $offer_taka = number_format(($product_price * $offer->offer) / 100, 2);
                $data['offer_price'] = $product_price - $offer_taka;
            }else{
                $data['offer_price'] = $product_price - $offer->offer;
            }
            $data['product_id'] = $product;


            foreach ($images as $key => $img){
                if ($idx == $key) {
                    $image = time().$img->getClientOriginalExtension();
                    $img->move(base_path('public/admin/offer/offer_product'), $image);
                    $data['image'] = $image;
                    break;
                }else{
                    $data['image'] = '';
                }
            }

            OfferProduct::query()->create($data);
        }
        session()->flash('success', 'The product offer has been created successfully');
        return redirect()->route('offer.manage');
    }

    public function offerProduct_edit($id){

    }

    public function offerProduct_delete(Request $request)
    {
        $delete = OfferProduct::query()->findOrFail($request->id);
        /*if($delete->image != null){
            $path = 'public/admin/offer/offer_product/'.$delete->image;
            unlink($path);
        }
        $delete->delete();*/
        $data['status']= 0;
        $delete->update($data);
    }

    public function coupon(){
        $coupons = [];
        $users = [];
        return view('admin.coupon.index', compact('coupons', 'users'));
    }
}
