<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;
use App\SubCategory;
use App\Http\Request\SubCategoryRequest;

class SubCategoryController extends Controller
{

    public function index()
    {
        $subcategory = SubCategory::all();
        $category = Category::pluck('name','id');
        return view('admin.sub-category.add-sub-category',compact('subcategory','category'));
    }

    public function store(Request $request)
    {
        $exist = SubCategory::Where('name',$request->name)->Where('category_id',$request->category_id)->exists();
        if($exist){
            session()->flash('error','Category and Sub-Category already exist in Database');
            return redirect()->route('sub-category.add');
        }

        $this->validate($request, [
            'name'=>'required|string|unique:subcategories',
            'image'=>'mimes:jpeg,jpg,png',
        ]);

        $subcategory = $request->except(['image', 'adv_image']);
        if($request->hasFile('image')){
            $image = $request->file('image');
            $filename1 = Date('Y').'_'.substr(md5(time()),0,6).'.'.$image->getClientOriginalExtension();
            $image->move(base_path('public/admin/subcategory/'),$filename1);
            $subcategory['image']=$filename1;
        }

        if($request->hasFile('adv_image')){
            $image = $request->file('adv_image');
            $filename2 = Date('Y').'_'.substr(md5(time()),0,6).'.'.$image->getClientOriginalExtension();
            $image->move(base_path('public/admin/subcategory/advertise'),$filename2);
            $subcategory['advertise_image']=$filename2;
        }

        SubCategory::query()->create($subcategory);

        session()->flash('success','Sub-Category has store successfully');
        return redirect()->route('sub-category.add');
    }


    public function show($id)
    {
        $subcategory = SubCategory::find($id);
        $category = Category::all();
        return view('admin.sub-category.edit-sub-category',compact('subcategory','category'));
    }

    public function update(Request $request)
    {
        $this->validate($request,[
            'name'=>'required',
        ]);

        $update = SubCategory::find($request->id);

        $subcategory = $request->except(['image', 'adv_image']);
        if($request->hasFile('image')){
            $image = $request->file('image');
            $filename = Date('Y').'_'.substr(md5(time()),0,6).'.'.$image->getClientOriginalExtension();
            $image->move(base_path('public/admin/subcategory/'),$filename);
            $subcategory['image']=$filename;
        }

        if($request->hasFile('adv_image')){
            $image = $request->file('adv_image');
            $filename = Date('Y').'_'.substr(md5(time()),0,6).'.'.$image->getClientOriginalExtension();
            $image->move(base_path('public/admin/subcategory/advertise'),$filename);
            $subcategory['advertise_image']=$filename;
        }

        $update->update($subcategory);
        session()->flash('success','Sub Category has store successfully');
        return redirect()->route('sub-category.add');

    }


    public function destroy(Request $request)
    {
        $delete = SubCategory::find($request->id);
        $delete->delete();
    }
}
