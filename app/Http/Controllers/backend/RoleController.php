<?php

namespace App\Http\Controllers\backend;

use App\Menu;
use App\MenuRole;
use App\Permission;
use App\Role;
use App\Wishlist;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;

class RoleController extends Controller
{
    public function index()
    {
        $role = Role::pluck('name','id');
        $roles = Role::all();

        $menus = Menu::groupBy('prefix')->get();
        return view('admin.user-management.roles',compact('role','roles','menus'));
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'name'=>'required:unique:roles'
        ]);

        $role_id = Role::updateOrcreate(['name'=>$request->name])->id;

        foreach ($request->menu_id as $menu_id){
            MenuRole::updateOrcreate([
                'role_id' => $role_id,
                'menu_id' =>$menu_id
            ]);
        }
        session()->flash('success','Role has store successfully');
        return redirect()->route('role.create');
    }


    public function edit($id){
        // $role = PermissionRole::find($id);
        $role = Role::query()->findOrFail($id);
        $menus = Menu::groupBy('prefix')->get();

        return view('admin.user-management.edit-roles',compact('role','menus'));
    }


    public function update(Request $request,$id)
    {
        $this->validate($request,[
            'name'=>['required',Rule::unique('roles')->ignore($id)]
        ]);


        Role::findOrFail($id)->update(['name'=>$request->name]);

        MenuRole::where('role_id',$id)->delete();

        foreach ($request->menu_id as $menu_id){
            MenuRole::updateOrcreate([
                'role_id' => $id,
                'menu_id' =>$menu_id
            ]);
        }

        session()->flash('success', 'Role updated successfully');
        return redirect()->route('role.view');
    }


    public function view(){
        $roles = Role::all();
        return view('admin.user-management.view-roles',compact('roles'));
    }


    public function destroy(Request $request)
    {
        $delete = Role::find($request->id);
        $delete->delete();
    }

}
