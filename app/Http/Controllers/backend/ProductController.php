<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;
use App\SubCategory;
use App\Size;
use App\ProductSize;
use App\Brand;
use App\Product;
use App\ImageProduct;
use Image;

class ProductController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
       // ini_set('memory_limit', '4096M');

    }

    public function index(){
        $category = Category::pluck('name','id');
        $brand = Brand::pluck('name','id');
        return view('admin.product.add-product',compact('category', 'brand'));
    }

    public function store(Request $request){
        $this->validate($request,[
           'name'=>'required',
           'category_id'=>'required',
           'image*'=>'image|mimes:jpg,jpeg,png'
        ]);
        //dd($request->all());

        $product= Product::create($request->except('image'));

        $sl=0;

        if($request->hasFile('image')){
            foreach ($request->file("image") as $img){

                $imagePro = new ImageProduct;
                $image_name = substr(md5(time()),0,6);
                $filename = Date('Y').'_'.$image_name.++$sl.".".$img->getClientOriginalExtension();
                $original = Image::make($img);
                $data = $request->except('image');
                $filename = Date('Y')."_".substr(md5(time()),0,5).".".$img->getClientOriginalExtension();
                $path = public_path('admin/product/'.$filename);
                $original->resize(570,570)->save($path); // resize(width,height)
                $imagePro->image = $filename;
                $imagePro->product_id = $product->id;

                $imagePro->save();
            }
        }

        session()->flash('success','Product has store successfully');
        return redirect()->route('product.add');
    }


    public function  viewproduct(){
        $products = Product::all();
        return view('admin.product.view-product',compact('products'));
    }

    /* get product for edit*/

    public function show($id){
        $product = Product::find($id);
        $category = Category::pluck('name','id');
        $brand = Brand::pluck('name','id');

        return view('admin.product.edit-product',compact('category','brand','product'));

    }

    /* product update*/

    public function update(Request $request)
    {

        $this->validate($request,[
            'name'=>'required',
            'categorie_id'=>'required',
            'image*'=>'image|mimes:jpeg,jpg,png'
        ]);

        $product = Product::find($request->id);
        $product->code = $request->code;
        $product->name = $request->name;
        $product->price = $request->price;
        $product->description = $request->description;
        $product->brand_id = $request->brand_id;
        $product->category_id = $request->categorie_id;
        $product->subcategory_id = $request->subcategorie_id;

        if($request->pro_image_delete!=NULL){
            foreach ($request->pro_image_delete as $img_id){
                $image_product = ImageProduct::find($img_id);
                $path = 'public/admin/product/' . $image_product->image;
                unlink($path);
                $image_product->delete();
            }
        }

        $sl=0;

        if($request->has('image')){
            foreach ($request->file('image') as $img){
                $imagePro = new ImageProduct;

                $image_name = substr(md5(time()),0,6);

                $original = Image::make($img);

                $filename = Date('Y')."_".substr(md5(time()),0,5).".".$img->getClientOriginalExtension();
                $path = public_path('admin/product/'.$filename);

                $original->resize(567,567)->save($path); // resize(width,height)
                $imagePro->image = $filename;
                $imagePro->product_id = $product->id;

                $imagePro->save();
            }
        }

        $product->update();
        session()->flash('success','Product has Update successfully');
        return redirect()->route('product.view');

    }


    /* search sub-category by category*/

    public function searchsubcategory(Request $request){
        $subcategories = SubCategory::where('category_id',$request->category_id)->get();
        echo '<option value=0 disabled selected>Select Subcategory</option>';
        foreach($subcategories as $subcategory){
            echo '<option value="'.$subcategory->id.'">'.$subcategory->name.'</option>';
        }
    }
}
