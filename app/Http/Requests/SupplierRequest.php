<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SupplierRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        
        return [
            'name'=>'required',
            'email'=>'unique:suppliers',
            'phone'=>'required|unique:suppliers',
            'address'=>'string|max:2500',
            'details'=>'string|max:2500',
        ];
    }


    public function messages(){
        return [
            'name.required'=>"Please input supplier Name",
            'email.unique'=>"This Email already taken, Please choose new one",
            'phone.required'=>"Phone Number must needed",
        ];
    }
}
