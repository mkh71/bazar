<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TeamRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'required',
            'designation'=>'required',
            'image'=>'required|image|mimes:jpg,jpeg,png,gif,svg',
            'facebook'=>'unique:teams',
            'twitter'=>'unique:teams',
            'google'=>'unique:teams',
            'instagram'=>'unique:teams',
        ];
    }


    public function messages(){
        return [
            'name.required'=>"Sir, please input name for save as Member Name",
            'designation.required'=>"Sir, please input name for save as Member Designation",
            'image.required'=>"Sir, please select image for upload",
            'facebook.unique'=>"Sir, Your input already taken. Please choose valid user link",
            'twitter.unique'=>"Sir, Your input already taken. Please choose valid user link",
            'google.unique'=>"Sir, Your input already taken. Please choose valid user link",
            'instagram.unique'=>"Sir, Your input already taken. Please choose valid user link",
        ];
    }
}
