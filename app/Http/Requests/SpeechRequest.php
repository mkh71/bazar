<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SpeechRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'team_id'=>'required',
            'description'=>'required',
            'signature'=>'required|image|mimes:jpg,jpeg,png,gif,svg',
        ];
    }


    public function messages(){
        return [
            'team_id.required'=>"Sir, please select team member from list",
            'description.required'=>"Sir, please input name for save as Speech description",
            'signature.required'=>"Sir, please select image for upload",
        ];
    }
}
