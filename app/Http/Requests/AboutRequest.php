<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AboutRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name'=>'required',
            'description'=>'required',
            'image'=>'image|mimes:jpg,jpeg,png,gif,svg',
        ];
    }


    public function messages(){
        return [
            'name.required'=>"Sir, please input name",
            'description.required'=>"Sir, please input name for save as description",
            'image'=>"Sir, please select image for upload",
        ];
    }
}
