<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';
    protected $fillable = ['code','name','price','quantity', 'description','brand_id','category_id','subcategory_id', 'status'];

   /* public function sizes(){
        return $this->belongsToMany(Size::class);
    }*/

    public function images(){
        return $this->hasMany(ImageProduct::class);
    }

    public function oneImage($product_id){
        $product = Product::query()->findOrFail($product_id);
        if ($product && $product->images){
            foreach ($product->images as $key=>$image){
                return asset('public/admin/product/'.$image->image);
            }
        }
    }

    public function category(){
        return $this->belongsTo(Category::class);
    }

    public function subcategory(){
        return $this->belongsTo(SubCategory::class);
    }

}
