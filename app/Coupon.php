<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Coupon extends Model
{
    protected $fillable = ['name', 'user_id', 'amount', 'expire', 'description'];

    public function user(){
        return $this->belongsTo(User::class);
    }
}
