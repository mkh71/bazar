<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubCategory extends Model
{
    protected $table = 'subcategories';

    protected $fillable = ['name','category_id', 'image', 'description', 'advertise_image'];

    public function category(){
        return $this->belongsTo(Category::class);
    }
}
