<?php
    namespace App\helper\Helpers;
    use App\About;
    use App\Category;
    use App\Company;
    use App\Faq;
    use App\OfferProduct;
    use App\Service;
    use App\Title;

    class Helper{
        /* Categories*/
        function categories(){
            return  Category::all();
        }

        /*Contact */

        function contact(){
            return About::all();
        }

        /* Services */

        function services(){
            return Service::orderBy('id','desc')->get();
        }

        /* About */

        function about(){
           return  About::where('type',1)->first();
        }

        /* Faq */

        function faqs(){
            return Faq::orderBy('id','desc')->get();
        }

        function testimonial(){

        }

        function chooseus(){
            return About::where('type',4)->get();
        }

        function company(){
            return Company::first();
        }

        function titles(){
            return Title::orderBy('id','desc')->get();
        }

        function offers(){
            return OfferProduct::query()
//                ->where('expire', '>', date('Y-m-d h:i:s'))
                ->where('status', 1)
                ->orderBy('id', 'desc')
                ->get();
        }

    }
