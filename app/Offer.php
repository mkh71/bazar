<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Offer extends Model
{
    protected $fillable = ['name', 'offer', 'type', 'image', 'description', 'status'];

    public function product(){
        return $this->belongsTo(Product::class);
    }
}
