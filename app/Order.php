<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = 'orders';
    protected $fillable = [
        'order_no',
        'user_id',
        'total',
        'qty',
        'discount', 'isDelivered', 'isReady', 'isCancel', 'delivered_date', 'ready_date', 'cancel_date'];
}
