<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $fillable = ['about','phone', 'phone1', 'mail', 'address', 'facebook', 'twitter', 'instagram', 'linkedin', 'gmail', 'whatsapp', 'logo', 'favicon', 'mobile_logo', 'footer_logo', 'name', 'download'];
}
