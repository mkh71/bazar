<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Speech extends Model
{
    protected $fillable = ['team_id','description','signature'];

    public function team(){
        return $this->belongsTo(Team::class);
    }
}
