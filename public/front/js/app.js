//js will start form heere--------
//Right side barss
// Attach listener function on state changes
//left side bar
//live-chat

function w3_open() {
	document.getElementById("main").style.marginRight = "30%";
	document.getElementById("mySidebar").style.width = "30%";
	document.getElementById("mySidebar").style.display = "block";
	document.getElementById("openNav").style.display = 'none';
}
function w3_close() {
	document.getElementById("main").style.marginRight = "0%";
	document.getElementById("mySidebar").style.display = "none";
	document.getElementById("openNav").style.display = "inline-block";
}

$(document).ready(function(){
	$(".add-to-cart",this).click(function(){
		$(".d-j").removeClass("d-none");
		$(".d-j").addClass("d-block");
		$(".cart-b").addClass("d-none");

	});
});
$(document).ready(function(){
	$("img").addClass("img-fluid");
});
$(document).ready(function(){
	var ib = $(".input-details").val();
	var ib = 1;
	if (ib = 1) {

	}
	$(".plus").click(function(){
		var pro_id = $(this).attr('data-id');
		alert(pro_id);
		ib++;
		$(".modal .input-details").val(ib);
		$(".product .input-details").val(ib+" "+"in bag");

	});

	$(".minus").click(function(){
		ib--;
		$("modal .input-details").val(ib);
		$("product .input-details").val(ib+" "+"in bag");
	});

});


$(document).ready(function(){
	$('.why-hd').slick({
		infinite: true,
		slidesToShow: 1,
		slidesToScroll: 1,
		autoplay: true,
		autoplaySpeed: 2000,
	});
});
$(document).ready(function(){
	$('.speacial-product').slick({
		infinite: true,
		slidesToShow: 5,
		slidesToScroll: 5,
		autoplay: true,
		autoplaySpeed: 2000,
		pauseOnHover: true,

		responsive: [
		{
			breakpoint: 1024,
			settings: {
				slidesToShow: 5,
				slidesToScroll: 5,
			}
		},
		{
			breakpoint: 770,
			settings: {
				slidesToShow: 3,
				slidesToScroll: 3,
			}
		},
		{
			breakpoint: 480,
			settings: {
				slidesToShow: 1,
				slidesToScroll: 1,
			}
		}

		]


	});
});

/*

$(document).ready(function () {
	$('#owlDemo1').owlCarousel({
		loop:true,
		dots:false,
		margin:0,
		nav:false,
		autoplay:false,
		responsive:{
			0:{
				items:1
			},
			600:{
				items:1
			},
			768:{
				items:2
			},
			1024:{
				items:3
			},
			1300:{
				items:5
			},
		}
	})

});
$(document).ready(function() {
            $("#owlDemo2").owlCarousel({
            	margin:20,
            	center:true,
                autoplay: true,
                autoplayHoverPause: true,
                autoplayTimeout: 4000,
                items: 1,
                loop: true,
              });
        });
//crousel-2nd
//crousel-3rd
/*$(document).ready(function () {
	$('#owlDemo3').owlCarousel({
		items:1,
		loop:true,
		margin:0,
		nav:false,
		dots:true,
		autoplay:true,
		responsive:{
			0:{
				items:1
			},
			600:{
				items:2
			},
			1000:{
				items:3
			}
		}
	});
});*/
//crousel-3rd
// Disable form submissions if there are invalid fields
(function() {
	'use strict';
	window.addEventListener('load', function() {
		// Get the forms we want to add validation styles to
		var forms = document.getElementsByClassName('needs-validation');
		// Loop over them and prevent submission
		var validation = Array.prototype.filter.call(forms, function(form) {
			form.addEventListener('submit', function(event) {
				if (form.checkValidity() === false) {
					event.preventDefault();
					event.stopPropagation();
				}
				form.classList.add('was-validated');
			}, false);
		});
	}, false);
})();



// ************************************************
// Shopping Cart API
// ************************************************

var shoppingCart = (function() {
	// =============================
	// Private methods and propeties
	// =============================
	cart = [];

	// Constructor
	function Item(name, price, count) {
		this.name = name;
		this.price = price;
		this.count = count;
	}

	// Save cart
	function saveCart() {
		sessionStorage.setItem('shoppingCart', JSON.stringify(cart));
	}

	// Load cart
	function loadCart() {
		cart = JSON.parse(sessionStorage.getItem('shoppingCart'));
	}
	if (sessionStorage.getItem("shoppingCart") != null) {
		loadCart();
	}


	// =============================
	// Public methods and propeties
	// =============================
	var obj = {};

	// Add to cart
	obj.addItemToCart = function(name, price, count) {
		for(var item in cart) {
			if(cart[item].name === name) {
				cart[item].count ++;
				saveCart();
				return;
			}
		}
		var item = new Item(name, price, count);
		cart.push(item);
		saveCart();
	}
	// Set count from item
	obj.setCountForItem = function(name, count) {
		for(var i in cart) {
			if (cart[i].name === name) {
				cart[i].count = count;
				break;
			}
		}
	};
	// Remove item from cart
	obj.removeItemFromCart = function(name) {
		for(var item in cart) {
			if(cart[item].name === name) {
				cart[item].count --;
				if(cart[item].count === 0) {
					cart.splice(item, 1);
				}
				break;
			}
		}
		saveCart();
	}

	// Remove all items from cart
	obj.removeItemFromCartAll = function(name) {
		for(var item in cart) {
			if(cart[item].name === name) {
				cart.splice(item, 1);
				break;
			}
		}
		saveCart();
	}

	// Clear cart
	obj.clearCart = function() {
		cart = [];
		saveCart();
	}

	// Count cart
	obj.totalCount = function() {
		var totalCount = 0;
		for(var item in cart) {
			totalCount += cart[item].count;
		}
		return totalCount;
	}

	// Total cart
	obj.totalCart = function() {
		var totalCart = 0;
		for(var item in cart) {
			totalCart += cart[item].price * cart[item].count;
		}
		return Number(totalCart.toFixed(2));
	}

	// List cart
	obj.listCart = function() {
		var cartCopy = [];
		for(i in cart) {
			item = cart[i];
			itemCopy = {};
			for(p in item) {
				itemCopy[p] = item[p];

			}
			itemCopy.total = Number(item.price * item.count).toFixed(2);
			cartCopy.push(itemCopy)
		}
		return cartCopy;
	}

	// cart : Array
	// Item : Object/Class
	// addItemToCart : Function
	// removeItemFromCart : Function
	// removeItemFromCartAll : Function
	// clearCart : Function
	// countCart : Function
	// totalCart : Function
	// listCart : Function
	// saveCart : Function
	// loadCart : Function
	return obj;
})();


// *****************************************
// Triggers / Events
// *****************************************
// Add item
$('.add-to-cart').click(function(event) {
	event.preventDefault();
	var proID= $(this).attr('data-id');
	alert(proID);
	var name = $(this).data('name');
	var price = Number($(this).data('price'));
	shoppingCart.addItemToCart(name, price, 1);
	displayCart();
});

// Clear items
$('.clear-cart').click(function() {
	shoppingCart.clearCart();
	displayCart();
});


function displayCart() {
	var cartArray = shoppingCart.listCart();
	var output = "";
	for(var i in cartArray) {

		output += "<tr>"
		+ "<td><div class='input-group cart-bdy row'><span class='plus-item col-md-12 text-success fas fa-angle-up input-group-addon' data-name=" + cartArray[i].name + "></span>"
		+ "<span type='text' class='col-md-12 mx-auto item-count form-control' data-name='" + cartArray[i].name + "'>" + cartArray[i].count +"</span>"
		+ "<span class='col-md-12 minus-item input-group-addon text-danger fas fa-angle-down' data-name=" + cartArray[i].name + "></span></div></td>"
		+"<td><img class='img-fluid cart-product-image' src='img/baby-wipes.jpg'></td>"
		+ "<td><div class='row'><div class='col-md-12'><strong>" + cartArray[i].name + "</strong></div>"
		+ " <div class='col-md-12'><small>"+ cartArray[i].price +"</small>/<small>200 gm</small></div></div></td>"
		+ "<td class='font-weight-bold'>" + cartArray[i].total + "৳</td>"
		+ "<td><button class='delete-item btn' data-name=" + cartArray[i].name + "><i class='fas fa-trash-alt text-danger'></i></button></td>"
		+  "</tr>";
	}
	$('.show-cart').html(output);
	$('.total-cart').html(shoppingCart.totalCart());
	$('.total-count').html(shoppingCart.totalCount());
}

// Delete item button

$('.show-cart').on("click", ".delete-item", function(event) {
	var name = $(this).data('name')
	shoppingCart.removeItemFromCartAll(name);
	displayCart();
});



// -1
$('.show-cart').on("click", ".minus-item", function(event) {
	var name = $(this).data('name')
	shoppingCart.removeItemFromCart(name);
	displayCart();
});

$('.shopping-card').on("click", ".minus-item", function(event) {
	var name = $(this).data('name')
	shoppingCart.removeItemFromCart(name);
	displayCart();
});

// +1
$('.show-cart').on("click", ".plus-item", function(event) {
	var name = $(this).data('name')
	shoppingCart.addItemToCart(name);
	displayCart();
});

$('.shopping-card').on("click", ".plus-item", function(event) {
	var name = $(this).data('name')
	shoppingCart.addItemToCart(name);
	displayCart();
});

// Item count input
$('.show-cart').on("change", ".item-count", function(event) {
	var name = $(this).data('name');
	var count = Number($(this).val());
	shoppingCart.setCountForItem(name, count);
	displayCart();
});

displayCart();

var urls = [
'img/baby-wipes.jpg',
'img/baby-wipes.jpg',
'img/baby-wipes.jpg'
];
$('#myGallery').zoomy(urls, {
	width: 600,
	height: 450,

});
/*
function w3_open() {
	document.getElementById("main").style.marginRight = "30%";
	document.getElementById("mySidebar").style.width = "30%";
	document.getElementById("mySidebar").style.display = "block";
	document.getElementById("openNav").style.display = 'none';
	document.getElementById("home").style.marginRight = "10%";
}
function w3_close() {
	document.getElementById("main").style.marginRight = "0%";
	document.getElementById("mySidebar").style.display = "none";
	document.getElementById("openNav").style.display = "inline-block";
	document.getElementById("home").style.marginRight = "0%";
}
*/
$(window).resize(function() {
	if ($(window).width() > 480) {
        $('#cartBag').click(function(){
			$('.product-page .col-md-3').addClass('offset-md-1');
		});
		$('#cartClose').click(function(){
			$('.product-page .col-md-3').removeClass('offset-md-1');
		});
    }
    if ($(window).width() < 480) {
        $(".collapse").removeClass('show');
    } else {
        $(".collapse").addClass('show');
    }
}).resize();
