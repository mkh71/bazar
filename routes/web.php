<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Illuminate\Support\Facades\Artisan;

/* Front Website All Route Start */





Route::get("reboot",function(){
    Artisan::call("view:clear");
    Artisan::call("route:clear");
    Artisan::call("config:cache");
    Artisan::call("cache:clear");
    Artisan::call("key:generate");
});

Route::get('migrate',function (){
    Artisan::call('migrate');
});



Route::get('dynamic-web','frontend\WebController@index');
Route::get('assign','frontend\WebController@assign');


Route::get('/','frontend\BasicController@index')->name('/');

Route::get('visit-category/{id}','frontend\ProductViewController@CategoryVisit')->name('visit.category');
Route::get('product-visit/{id}','frontend\ProductViewController@ProductVisit')->name('visit.product');
Route::get('order-product','frontend\CartController@order_product')->name('order.product');

/* Cart Start */

Route::namespace('frontend')->name('cart.')->group(function(){
    Route::post("/save-cart-item","CartController@store")->name('store');
    Route::post('/update-cart-item','CartController@updateCart')->name('update');
    Route::get('/carts','CartController@carts')->name('items');
    Route::post('/remove-cart-item','CartController@removeItem')->name('destroy');


    Route::post('/location-store','CartController@locationStore')->name('orderLocation');
    Route::get('/payment-option','CartController@paymentPage')->name('payment');
    Route::post('/confirm-order','CartController@confirmOrder')->name('complete-order');
    Route::get('/place-order','CartController@placeOrder')->name('placeorder');

});

/* Cart End*/

/*
Route::get('visit-category/{id}','frontend\BasicController@CategoryProductVisit')->name('visit.category');
Route::get('visit-category/{name}/{id}','frontend\BasicController@CategoryProductVisit')->name('productcategory.visit');*/

Route::get("single-product/{id}","frontend\BasicController@categoryProducts")->name('product.singlevisit');


Route::prefix('/CustomerRequest')->group(function(){
    Route::get('/visitor-messages','frontend\BasicController@Messages')->name('customer.message');
});

/*visitor text end */
/* Front Website All Route End */



Route::get('/about','frontend\BasicController@AboutMtr')->name('about');
Route::get('/contact','frontend\BasicController@Contact')->name('contact');
Route::get('/products','frontend\BasicController@product')->name('products');
Route::get('/service/{id}','frontend\BasicController@singleservice')->name('service.browse');



Auth::routes();

Route::middleware(['CheckPermit','auth'])->group(function(){

    Route::prefix('UserManagement')->group(function(){
        Route::get('/user-create','backend\UserController@index')->name('user.create');
        Route::post('/user-store','backend\UserController@store')->name('user.store');
        Route::get('/edit/{id}','backend\UserController@edit')->name('user.edit');
        Route::post('/erase','backend\UserController@destroy')->name('user.destroy');
        Route::post('/update-user/{id}','backend\UserController@update')->name('user.update');
        Route::get('/role-create','backend\RoleController@index')->name('role.create');
        Route::get('/role-view','backend\RoleController@view')->name('role.view');
        Route::post('/role-store','backend\RoleController@store')->name('role.store');
        Route::get('/role/edit/{id}','backend\RoleController@edit')->name('role.edit');
        Route::patch('/role/{id}/update','backend\RoleController@update')->name('role.update');
    });




    Route::prefix('SliderManagement')->group(function (){
        Route::get("Add-Slider","backend\SliderController@index")->name('slider.add');
        Route::post("store-Slider","backend\SliderController@store")->name('slider.store');
        Route::get("edit-Slider/{id}","backend\SliderController@edit")->name('slider.edit');
        Route::post("update-Slider/{id}","backend\SliderController@update")->name('slider.update');
        Route::post("destroy-Slider","backend\SliderController@destroy")->name('slider.destroy');
    });

    Route::prefix('Company')->group(function (){
        Route::get("edit-company/{id}","backend\SettingController@edit")->name('company.edit');
        Route::post("update-company/{id}","backend\SettingController@update")->name('company.update');
    });

    Route::prefix('dashboard')->group(function(){
        Route::get('/home', 'backend\AdminController@index')->name('home');

    });

    Route::prefix('ProductManagement')->group(function (){
        Route::get('/brand', 'backend\BrandController@index')->name('brand.add');
        Route::post('/brand/add', 'backend\BrandController@store')->name('brand.store');
        Route::get('/brand/{id}/Show', 'backend\BrandController@show')->name('brand.edit');
        Route::post('/erase', 'backend\BrandController@destroy')->name('brand.destroy');
        Route::post('/brand/update', 'backend\BrandController@update')->name('brand.update');

        /* Category Start */

        Route::get('/category', 'backend\CategoryController@index')->name('category.add');
        Route::post('/category/add', 'backend\CategoryController@store')->name('category.store');
        Route::get('/category/{id}/Show', 'backend\CategoryController@show')->name('category.edit');
        Route::post('/Category/erase', 'backend\CategoryController@destroy')->name('category.destroy');
        Route::post('/category/update', 'backend\CategoryController@update')->name('category.update');

        /* Category End */

        /* Sub-Category Start */

        Route::get('/sub-category', 'backend\SubCategoryController@index')->name('sub-category.add');
        Route::post('/sub-category/add', 'backend\SubCategoryController@store')->name('sub-category.store');
        Route::get('/sub-category/{id}/Edit', 'backend\SubCategoryController@show')->name('sub-category.edit');
        Route::post('/sub-Category/erase', 'backend\SubCategoryController@destroy')->name('sub-category.destroy');
        Route::post('/sub-category/update', 'backend\SubCategoryController@update')->name('sub-category.update');

        /* Sub-Category End */

        /* Product Add */

        Route::get('/product', 'backend\ProductController@index')->name('product.add');
        Route::get('/product/view', 'backend\ProductController@viewproduct')->name('product.view');
        Route::post('product/add', 'backend\ProductController@store')->name('product.store');
        Route::get('/product/{id}/Show', 'backend\ProductController@show')->name('product.edit');
        Route::post('/Product/erase', 'backend\ProductController@destroy')->name('product.destroy');
        Route::post('/product/update', 'backend\ProductController@update')->name('product.update');
        Route::post('/status-update', 'backend\ProductController@status_update')->name('product.statusupdate');

        Route::post('/search-category', 'backend\ProductController@searchsubcategory')->name('purchase.subcategory');


        /* Product End */

        /* Product Size Start */

        Route::get('/size', 'backend\SizeController@index')->name('size.add');
        Route::post('/add', 'backend\SizeController@store')->name('size.store');
        Route::get('/size/{id}/Show', 'backend\SizeController@show')->name('size.edit');
        Route::post('/Size/erase', 'backend\SizeController@destroy')->name('size.destroy');
        Route::post('/size/update', 'backend\SizeController@update')->name('size.update');


    });

    Route::prefix('Offer')->group(function (){
        Route::get('view-offer', 'backend\OfferController@index')->name('offer.index');
        Route::post('store-offer', 'backend\OfferController@store')->name('offer.store');
        Route::get('edit-offer/{id}', 'backend\OfferController@edit')->name('offer.edit');
        Route::post('update-offer/', 'backend\OfferController@update')->name('offer.update');
        Route::post('delete-offer', 'backend\OfferController@destroy')->name('offer.delete');

        Route::get('manage-offer', 'backend\OfferController@manage')->name('offer.manage');
        Route::post('load-product', 'backend\OfferController@load_product')->name('load.product');
        Route::post('create-offer', 'backend\OfferController@create_offer')->name('create.offer');
        Route::post('Offer/edit-offer-product/{id}', 'backend\OfferController@offerProduct_edit')->name('offer_product.edit');
        Route::post('Offer/delete-offer-product', 'backend\OfferController@offerProduct_delete')->name('offer_product.delete');

        Route::get('coupon', 'backend\OfferController@coupon')->name('coupon');
        Route::post('coupon', 'backend\OfferController@coupon')->name('coupon.store');
    });


    Route::prefix('WebManagement')->group(function () {

        /* Team Start */

        Route::get("/add-team","backend\TeamController@index")->name('team.add');
        Route::get("/edit/{id}","backend\TeamController@show")->name('team.edit');
        Route::post("/save-team","backend\TeamController@store")->name('team.store');
        Route::post("/update-team","backend\TeamController@update")->name('team.update');
        Route::post("/erase","backend\TeamController@destroy")->name('team.destroy');

        /* service Start */

        Route::get("/add-service","backend\ServiceController@index")->name('service.add');
        Route::get("/edit-service/{id}","backend\ServiceController@show")->name('service.edit');
        Route::post("/save-service","backend\ServiceController@store")->name('service.store');
        Route::post("/update-service/{id}","backend\ServiceController@update")->name('service.update');
        Route::post("/erase-service","backend\ServiceController@destroy")->name('service.destroy');

        /* Speech  Start */

        Route::get("/add-speech","backend\SpeechController@index")->name('speech.add');
        Route::get("/edit-speech/{id}","backend\SpeechController@show")->name('speech.edit');
        Route::post("/save-speech","backend\SpeechController@store")->name('speech.store');
        Route::post("/update-speech","backend\SpeechController@update")->name('speech.update');
        Route::post("/erase-speech","backend\SpeechController@destroy")->name('speech.destroy');

        /* About */

        Route::get("/add-about","backend\MtrController@index")->name('about.add');
        Route::get("/edit-about/{id}","backend\MtrController@show")->name('about.edit');
        Route::post("/save-about","backend\MtrController@store")->name('about.store');
        Route::post("/update-about","backend\MtrController@update")->name('about.update');
        Route::post("/erase-about","backend\MtrController@destroy")->name('about.destroy');
    });

        /* Title  */
    Route::prefix('title')->group(function(){
        Route::get("/add-title","backend\TitleController@index")->name('title.add');
        Route::get("/edit-title/{id}","backend\TitleController@show")->name('title.edit');
        Route::post("/save-title","backend\TitleController@store")->name('title.store');
        Route::post("/update-title/{id}","backend\TitleController@update")->name('title.update');
        Route::post("/erase-title","backend\TitleController@destroy")->name('title.destroy');
    });

    /* Faq Manage Start */
    Route::prefix('faq')->group(function(){
        Route::get("/add-faq","backend\FaqController@index")->name('faq.add');
        Route::get("/edit-faq/{id}","backend\FaqController@show")->name('faq.edit');
        Route::post("/save-faq","backend\FaqController@store")->name('faq.store');
        Route::post("/update-faq/{id}","backend\FaqController@update")->name('faq.update');
        Route::post("/erase-faq","backend\FaqController@destroy")->name('faq.destroy');
    });

});
