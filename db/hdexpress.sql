-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Apr 05, 2020 at 05:10 PM
-- Server version: 5.7.29-0ubuntu0.18.04.1
-- PHP Version: 7.2.24-0ubuntu0.18.04.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hdexpress`
--

-- --------------------------------------------------------

--
-- Table structure for table `abouts`
--

CREATE TABLE `abouts` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `description` varchar(5000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `abouts`
--

INSERT INTO `abouts` (`id`, `name`, `image`, `type`, `description`, `created_at`, `updated_at`) VALUES
(1, ';lk;ljk.', '2020_92b474.jpg', 4, '<p>hkjhkhjfytthyiuk,mhnbhj jgjhkjhkjghhukjk,ujkm<br></p>', '2020-04-03 05:44:07', '2020-04-03 05:44:07');

-- --------------------------------------------------------

--
-- Table structure for table `brands`
--

CREATE TABLE `brands` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `IsActive` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `advertise_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `image`, `description`, `advertise_image`, `created_at`, `updated_at`) VALUES
(1, 'Category', '2020_5dc523.jpg', 'Demo', '2020_5dc523.jpg', '2020-03-20 07:46:58', '2020-03-20 07:46:58'),
(2, 'Baby Care', '2020_b95990.jpg', NULL, '2020_b95990.jpg', '2020-03-31 02:20:31', '2020-03-31 02:20:31');

-- --------------------------------------------------------

--
-- Table structure for table `companies`
--

CREATE TABLE `companies` (
  `id` int(10) UNSIGNED NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mail` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `facebook` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitter` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `instagram` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `linkedin` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gmail` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `whatsapp` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `logo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `favicon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile_logo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `footer_logo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `about` longtext COLLATE utf8mb4_unicode_ci,
  `download` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `companies`
--

INSERT INTO `companies` (`id`, `phone`, `phone1`, `mail`, `address`, `facebook`, `twitter`, `instagram`, `linkedin`, `gmail`, `whatsapp`, `logo`, `favicon`, `mobile_logo`, `footer_logo`, `name`, `about`, `download`, `created_at`, `updated_at`) VALUES
(1, '+880123456789', '+880123456789', 'hdexpress@gmail.com', '2No gate, CDA Avenue, East Nasirabad, Chattogram.', 'https://facebook.com/hdexpress', 'https://facebook.com/hdexpress', 'https://facebook.com/hdexpress', 'https://facebook.com/hdexpress', 'hdexpress@gmail.com', NULL, '2020_46e6031.png', NULL, NULL, NULL, 'HD Express', NULL, NULL, '2020-03-20 07:42:16', '2020-03-31 01:08:17');

-- --------------------------------------------------------

--
-- Table structure for table `coupons`
--

CREATE TABLE `coupons` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `amount` int(11) NOT NULL,
  `expire` datetime NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `faqs`
--

CREATE TABLE `faqs` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `image_products`
--

CREATE TABLE `image_products` (
  `id` int(10) UNSIGNED NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `image_products`
--

INSERT INTO `image_products` (`id`, `image`, `product_id`, `created_at`, `updated_at`) VALUES
(1, '2020_b0fb7.jpg', 1, '2020-03-20 08:52:50', '2020-03-20 08:52:50'),
(2, '2020_b0fb7.jpg', 1, '2020-03-20 08:52:51', '2020-03-20 08:52:51'),
(3, '2020_04ddb.jpg', 1, '2020-03-20 08:52:51', '2020-03-20 08:52:51'),
(4, '2020_04ddb.jpg', 1, '2020-03-20 08:52:52', '2020-03-20 08:52:52'),
(5, '2020_a01ea.png', 2, '2020-03-22 02:33:02', '2020-03-22 02:33:02'),
(6, '2020_b0b84.jpg', 3, '2020-03-22 02:33:32', '2020-03-22 02:33:32'),
(7, '2020_c838a.jpg', 4, '2020-03-31 02:22:49', '2020-03-31 02:22:49');

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE `menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `prefix` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `route` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `path` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `name`, `prefix`, `route`, `path`, `icon`, `status`, `created_at`, `updated_at`) VALUES
(1, 'customer message', 'CustomerRequest', 'customer.message', 'CustomerRequest/visitor-messages', NULL, 1, '2020-04-05 09:08:30', '2020-04-05 09:08:30'),
(2, 'slider add', 'SliderManagement', 'slider.add', 'SliderManagement/Add-Slider', NULL, 1, '2020-04-05 09:08:30', '2020-04-05 09:08:30'),
(3, 'slider store', 'SliderManagement', 'slider.store', 'SliderManagement/store-Slider', NULL, 1, '2020-04-05 09:08:30', '2020-04-05 09:08:30'),
(4, 'slider edit', 'SliderManagement', 'slider.edit', 'SliderManagement/edit-Slider/{id}', NULL, 1, '2020-04-05 09:08:30', '2020-04-05 09:08:30'),
(5, 'slider update', 'SliderManagement', 'slider.update', 'SliderManagement/update-Slider/{id}', NULL, 1, '2020-04-05 09:08:30', '2020-04-05 09:08:30'),
(6, 'slider destroy', 'SliderManagement', 'slider.destroy', 'SliderManagement/destroy-Slider', NULL, 1, '2020-04-05 09:08:30', '2020-04-05 09:08:30'),
(7, 'company edit', 'Company', 'company.edit', 'Company/edit-company/{id}', NULL, 1, '2020-04-05 09:08:30', '2020-04-05 09:08:30'),
(8, 'company update', 'Company', 'company.update', 'Company/update-company/{id}', NULL, 1, '2020-04-05 09:08:30', '2020-04-05 09:08:30'),
(9, 'home', 'dashboard', 'home', 'dashboard/home', NULL, 1, '2020-04-05 09:08:30', '2020-04-05 09:08:30'),
(10, 'brand add', 'ProductManagement', 'brand.add', 'ProductManagement/brand', NULL, 1, '2020-04-05 09:08:30', '2020-04-05 09:08:30'),
(11, 'brand store', 'ProductManagement', 'brand.store', 'ProductManagement/brand/add', NULL, 1, '2020-04-05 09:08:30', '2020-04-05 09:08:30'),
(12, 'brand edit', 'ProductManagement', 'brand.edit', 'ProductManagement/brand/{id}/Show', NULL, 1, '2020-04-05 09:08:30', '2020-04-05 09:08:30'),
(13, 'brand destroy', 'ProductManagement', 'brand.destroy', 'ProductManagement/erase', NULL, 1, '2020-04-05 09:08:30', '2020-04-05 09:08:30'),
(14, 'brand update', 'ProductManagement', 'brand.update', 'ProductManagement/brand/update', NULL, 1, '2020-04-05 09:08:30', '2020-04-05 09:08:30'),
(15, 'category add', 'ProductManagement', 'category.add', 'ProductManagement/category', NULL, 1, '2020-04-05 09:08:30', '2020-04-05 09:08:30'),
(16, 'category store', 'ProductManagement', 'category.store', 'ProductManagement/category/add', NULL, 1, '2020-04-05 09:08:30', '2020-04-05 09:08:30'),
(17, 'category edit', 'ProductManagement', 'category.edit', 'ProductManagement/category/{id}/Show', NULL, 1, '2020-04-05 09:08:30', '2020-04-05 09:08:30'),
(18, 'category destroy', 'ProductManagement', 'category.destroy', 'ProductManagement/Category/erase', NULL, 1, '2020-04-05 09:08:30', '2020-04-05 09:08:30'),
(19, 'category update', 'ProductManagement', 'category.update', 'ProductManagement/category/update', NULL, 1, '2020-04-05 09:08:30', '2020-04-05 09:08:30'),
(20, 'sub-category add', 'ProductManagement', 'sub-category.add', 'ProductManagement/sub-category', NULL, 1, '2020-04-05 09:08:30', '2020-04-05 09:08:30'),
(21, 'sub-category store', 'ProductManagement', 'sub-category.store', 'ProductManagement/sub-category/add', NULL, 1, '2020-04-05 09:08:30', '2020-04-05 09:08:30'),
(22, 'sub-category edit', 'ProductManagement', 'sub-category.edit', 'ProductManagement/sub-category/{id}/Edit', NULL, 1, '2020-04-05 09:08:30', '2020-04-05 09:08:30'),
(23, 'sub-category destroy', 'ProductManagement', 'sub-category.destroy', 'ProductManagement/sub-Category/erase', NULL, 1, '2020-04-05 09:08:30', '2020-04-05 09:08:30'),
(24, 'sub-category update', 'ProductManagement', 'sub-category.update', 'ProductManagement/sub-category/update', NULL, 1, '2020-04-05 09:08:30', '2020-04-05 09:08:30'),
(25, 'product add', 'ProductManagement', 'product.add', 'ProductManagement/product', NULL, 1, '2020-04-05 09:08:30', '2020-04-05 09:08:30'),
(26, 'product view', 'ProductManagement', 'product.view', 'ProductManagement/product/view', NULL, 1, '2020-04-05 09:08:30', '2020-04-05 09:08:30'),
(27, 'product store', 'ProductManagement', 'product.store', 'ProductManagement/product/add', NULL, 1, '2020-04-05 09:08:30', '2020-04-05 09:08:30'),
(28, 'product edit', 'ProductManagement', 'product.edit', 'ProductManagement/product/{id}/Show', NULL, 1, '2020-04-05 09:08:30', '2020-04-05 09:08:30'),
(29, 'product destroy', 'ProductManagement', 'product.destroy', 'ProductManagement/Product/erase', NULL, 1, '2020-04-05 09:08:30', '2020-04-05 09:08:30'),
(30, 'product update', 'ProductManagement', 'product.update', 'ProductManagement/product/update', NULL, 1, '2020-04-05 09:08:30', '2020-04-05 09:08:30'),
(31, 'product statusupdate', 'ProductManagement', 'product.statusupdate', 'ProductManagement/status-update', NULL, 1, '2020-04-05 09:08:30', '2020-04-05 09:08:30'),
(32, 'purchase subcategory', 'ProductManagement', 'purchase.subcategory', 'ProductManagement/search-category', NULL, 1, '2020-04-05 09:08:30', '2020-04-05 09:08:30'),
(33, 'size add', 'ProductManagement', 'size.add', 'ProductManagement/size', NULL, 1, '2020-04-05 09:08:30', '2020-04-05 09:08:30'),
(34, 'size store', 'ProductManagement', 'size.store', 'ProductManagement/add', NULL, 1, '2020-04-05 09:08:30', '2020-04-05 09:08:30'),
(35, 'size edit', 'ProductManagement', 'size.edit', 'ProductManagement/size/{id}/Show', NULL, 1, '2020-04-05 09:08:30', '2020-04-05 09:08:30'),
(36, 'size destroy', 'ProductManagement', 'size.destroy', 'ProductManagement/Size/erase', NULL, 1, '2020-04-05 09:08:30', '2020-04-05 09:08:30'),
(37, 'size update', 'ProductManagement', 'size.update', 'ProductManagement/size/update', NULL, 1, '2020-04-05 09:08:30', '2020-04-05 09:08:30'),
(38, 'offer index', 'Offer', 'offer.index', 'Offer/view-offer', NULL, 1, '2020-04-05 09:08:30', '2020-04-05 09:08:30'),
(39, 'offer store', 'Offer', 'offer.store', 'Offer/store-offer', NULL, 1, '2020-04-05 09:08:30', '2020-04-05 09:08:30'),
(40, 'offer edit', 'Offer', 'offer.edit', 'Offer/edit-offer/{id}', NULL, 1, '2020-04-05 09:08:30', '2020-04-05 09:08:30'),
(41, 'offer update', 'Offer', 'offer.update', 'Offer/update-offer', NULL, 1, '2020-04-05 09:08:30', '2020-04-05 09:08:30'),
(42, 'offer delete', 'Offer', 'offer.delete', 'Offer/delete-offer', NULL, 1, '2020-04-05 09:08:30', '2020-04-05 09:08:30'),
(43, 'offer manage', 'Offer', 'offer.manage', 'Offer/manage-offer', NULL, 1, '2020-04-05 09:08:30', '2020-04-05 09:08:30'),
(44, 'load product', 'Offer', 'load.product', 'Offer/load-product', NULL, 1, '2020-04-05 09:08:30', '2020-04-05 09:08:30'),
(45, 'create offer', 'Offer', 'create.offer', 'Offer/create-offer', NULL, 1, '2020-04-05 09:08:30', '2020-04-05 09:08:30'),
(46, 'offer_product edit', 'Offer', 'offer_product.edit', 'Offer/Offer/edit-offer-product/{id}', NULL, 1, '2020-04-05 09:08:30', '2020-04-05 09:08:30'),
(47, 'offer_product delete', 'Offer', 'offer_product.delete', 'Offer/Offer/delete-offer-product', NULL, 1, '2020-04-05 09:08:30', '2020-04-05 09:08:30'),
(48, 'coupon', 'Offer', 'coupon', 'Offer/coupon', NULL, 1, '2020-04-05 09:08:30', '2020-04-05 09:08:30'),
(49, 'coupon store', 'Offer', 'coupon.store', 'Offer/coupon', NULL, 1, '2020-04-05 09:08:30', '2020-04-05 09:08:30'),
(50, 'team add', 'WebManagement', 'team.add', 'WebManagement/add-team', NULL, 1, '2020-04-05 09:08:30', '2020-04-05 09:08:30'),
(51, 'team edit', 'WebManagement', 'team.edit', 'WebManagement/edit/{id}', NULL, 1, '2020-04-05 09:08:31', '2020-04-05 09:08:31'),
(52, 'team store', 'WebManagement', 'team.store', 'WebManagement/save-team', NULL, 1, '2020-04-05 09:08:31', '2020-04-05 09:08:31'),
(53, 'team update', 'WebManagement', 'team.update', 'WebManagement/update-team', NULL, 1, '2020-04-05 09:08:31', '2020-04-05 09:08:31'),
(54, 'team destroy', 'WebManagement', 'team.destroy', 'WebManagement/erase', NULL, 1, '2020-04-05 09:08:31', '2020-04-05 09:08:31'),
(55, 'service add', 'WebManagement', 'service.add', 'WebManagement/add-service', NULL, 1, '2020-04-05 09:08:31', '2020-04-05 09:08:31'),
(56, 'service edit', 'WebManagement', 'service.edit', 'WebManagement/edit-service/{id}', NULL, 1, '2020-04-05 09:08:31', '2020-04-05 09:08:31'),
(57, 'service store', 'WebManagement', 'service.store', 'WebManagement/save-service', NULL, 1, '2020-04-05 09:08:31', '2020-04-05 09:08:31'),
(58, 'service update', 'WebManagement', 'service.update', 'WebManagement/update-service/{id}', NULL, 1, '2020-04-05 09:08:31', '2020-04-05 09:08:31'),
(59, 'service destroy', 'WebManagement', 'service.destroy', 'WebManagement/erase-service', NULL, 1, '2020-04-05 09:08:31', '2020-04-05 09:08:31'),
(60, 'speech add', 'WebManagement', 'speech.add', 'WebManagement/add-speech', NULL, 1, '2020-04-05 09:08:31', '2020-04-05 09:08:31'),
(61, 'speech edit', 'WebManagement', 'speech.edit', 'WebManagement/edit-speech/{id}', NULL, 1, '2020-04-05 09:08:31', '2020-04-05 09:08:31'),
(62, 'speech store', 'WebManagement', 'speech.store', 'WebManagement/save-speech', NULL, 1, '2020-04-05 09:08:31', '2020-04-05 09:08:31'),
(63, 'speech update', 'WebManagement', 'speech.update', 'WebManagement/update-speech', NULL, 1, '2020-04-05 09:08:31', '2020-04-05 09:08:31'),
(64, 'speech destroy', 'WebManagement', 'speech.destroy', 'WebManagement/erase-speech', NULL, 1, '2020-04-05 09:08:31', '2020-04-05 09:08:31'),
(65, 'about add', 'WebManagement', 'about.add', 'WebManagement/add-about', NULL, 1, '2020-04-05 09:08:31', '2020-04-05 09:08:31'),
(66, 'about edit', 'WebManagement', 'about.edit', 'WebManagement/edit-about/{id}', NULL, 1, '2020-04-05 09:08:31', '2020-04-05 09:08:31'),
(67, 'about store', 'WebManagement', 'about.store', 'WebManagement/save-about', NULL, 1, '2020-04-05 09:08:31', '2020-04-05 09:08:31'),
(68, 'about update', 'WebManagement', 'about.update', 'WebManagement/update-about', NULL, 1, '2020-04-05 09:08:31', '2020-04-05 09:08:31'),
(69, 'about destroy', 'WebManagement', 'about.destroy', 'WebManagement/erase-about', NULL, 1, '2020-04-05 09:08:31', '2020-04-05 09:08:31'),
(70, 'title add', 'title', 'title.add', 'title/add-title', NULL, 1, '2020-04-05 09:08:31', '2020-04-05 09:08:31'),
(71, 'title edit', 'title', 'title.edit', 'title/edit-title/{id}', NULL, 1, '2020-04-05 09:08:31', '2020-04-05 09:08:31'),
(72, 'title store', 'title', 'title.store', 'title/save-title', NULL, 1, '2020-04-05 09:08:31', '2020-04-05 09:08:31'),
(73, 'title update', 'title', 'title.update', 'title/update-title/{id}', NULL, 1, '2020-04-05 09:08:31', '2020-04-05 09:08:31'),
(74, 'title destroy', 'title', 'title.destroy', 'title/erase-title', NULL, 1, '2020-04-05 09:08:31', '2020-04-05 09:08:31'),
(75, 'faq add', 'faq', 'faq.add', 'faq/add-faq', NULL, 1, '2020-04-05 09:08:31', '2020-04-05 09:08:31'),
(76, 'faq edit', 'faq', 'faq.edit', 'faq/edit-faq/{id}', NULL, 1, '2020-04-05 09:08:31', '2020-04-05 09:08:31'),
(77, 'faq store', 'faq', 'faq.store', 'faq/save-faq', NULL, 1, '2020-04-05 09:08:31', '2020-04-05 09:08:31'),
(78, 'faq update', 'faq', 'faq.update', 'faq/update-faq/{id}', NULL, 1, '2020-04-05 09:08:31', '2020-04-05 09:08:31'),
(79, 'faq destroy', 'faq', 'faq.destroy', 'faq/erase-faq', NULL, 1, '2020-04-05 09:08:31', '2020-04-05 09:08:31'),
(80, 'user create', 'UserManagement', 'user.create', 'UserManagement/user-create', NULL, 1, '2020-04-05 09:50:29', '2020-04-05 09:50:29'),
(81, 'user store', 'UserManagement', 'user.store', 'UserManagement/user-store', NULL, 1, '2020-04-05 09:50:29', '2020-04-05 09:50:29'),
(82, 'user edit', 'UserManagement', 'user.edit', 'UserManagement/edit/{id}', NULL, 1, '2020-04-05 09:50:29', '2020-04-05 09:50:29'),
(83, 'user destroy', 'UserManagement', 'user.destroy', 'UserManagement/erase', NULL, 1, '2020-04-05 09:50:29', '2020-04-05 09:50:29'),
(84, 'user update', 'UserManagement', 'user.update', 'UserManagement/update-user/{id}', NULL, 1, '2020-04-05 09:50:29', '2020-04-05 09:50:29'),
(85, 'role create', 'UserManagement', 'role.create', 'UserManagement/role-create', NULL, 1, '2020-04-05 09:50:29', '2020-04-05 09:50:29'),
(86, 'role view', 'UserManagement', 'role.view', 'UserManagement/role-view', NULL, 1, '2020-04-05 09:50:29', '2020-04-05 09:50:29'),
(87, 'role store', 'UserManagement', 'role.store', 'UserManagement/role-store', NULL, 1, '2020-04-05 09:50:29', '2020-04-05 09:50:29'),
(88, 'role edit', 'UserManagement', 'role.edit', 'UserManagement/role/edit/{id}', NULL, 1, '2020-04-05 09:50:29', '2020-04-05 09:50:29'),
(89, 'role update', 'UserManagement', 'role.update', 'UserManagement/role/{id}/update', NULL, 1, '2020-04-05 09:50:29', '2020-04-05 09:50:29');

-- --------------------------------------------------------

--
-- Table structure for table `menu_roles`
--

CREATE TABLE `menu_roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `menu_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menu_roles`
--

INSERT INTO `menu_roles` (`id`, `menu_id`, `role_id`, `created_at`, `updated_at`) VALUES
(2, 1, 1, '2020-04-05 09:15:43', '2020-04-05 09:15:43'),
(3, 2, 1, '2020-04-05 09:15:43', '2020-04-05 09:15:43'),
(4, 3, 1, '2020-04-05 09:15:43', '2020-04-05 09:15:43'),
(5, 4, 1, '2020-04-05 09:15:43', '2020-04-05 09:15:43'),
(6, 5, 1, '2020-04-05 09:15:43', '2020-04-05 09:15:43'),
(7, 6, 1, '2020-04-05 09:15:43', '2020-04-05 09:15:43'),
(8, 7, 1, '2020-04-05 09:15:43', '2020-04-05 09:15:43'),
(9, 8, 1, '2020-04-05 09:15:43', '2020-04-05 09:15:43'),
(10, 9, 1, '2020-04-05 09:15:43', '2020-04-05 09:15:43'),
(11, 10, 1, '2020-04-05 09:15:43', '2020-04-05 09:15:43'),
(12, 11, 1, '2020-04-05 09:15:43', '2020-04-05 09:15:43'),
(13, 12, 1, '2020-04-05 09:15:43', '2020-04-05 09:15:43'),
(14, 13, 1, '2020-04-05 09:15:43', '2020-04-05 09:15:43'),
(15, 14, 1, '2020-04-05 09:15:43', '2020-04-05 09:15:43'),
(16, 15, 1, '2020-04-05 09:15:43', '2020-04-05 09:15:43'),
(17, 16, 1, '2020-04-05 09:15:43', '2020-04-05 09:15:43'),
(18, 17, 1, '2020-04-05 09:15:43', '2020-04-05 09:15:43'),
(19, 18, 1, '2020-04-05 09:15:43', '2020-04-05 09:15:43'),
(20, 19, 1, '2020-04-05 09:15:43', '2020-04-05 09:15:43'),
(21, 20, 1, '2020-04-05 09:15:43', '2020-04-05 09:15:43'),
(22, 21, 1, '2020-04-05 09:15:43', '2020-04-05 09:15:43'),
(23, 22, 1, '2020-04-05 09:15:43', '2020-04-05 09:15:43'),
(24, 23, 1, '2020-04-05 09:15:43', '2020-04-05 09:15:43'),
(25, 24, 1, '2020-04-05 09:15:43', '2020-04-05 09:15:43'),
(26, 25, 1, '2020-04-05 09:15:43', '2020-04-05 09:15:43'),
(27, 26, 1, '2020-04-05 09:15:43', '2020-04-05 09:15:43'),
(28, 27, 1, '2020-04-05 09:15:43', '2020-04-05 09:15:43'),
(29, 28, 1, '2020-04-05 09:15:43', '2020-04-05 09:15:43'),
(30, 29, 1, '2020-04-05 09:15:43', '2020-04-05 09:15:43'),
(31, 30, 1, '2020-04-05 09:15:43', '2020-04-05 09:15:43'),
(32, 31, 1, '2020-04-05 09:15:43', '2020-04-05 09:15:43'),
(33, 32, 1, '2020-04-05 09:15:43', '2020-04-05 09:15:43'),
(34, 33, 1, '2020-04-05 09:15:43', '2020-04-05 09:15:43'),
(35, 34, 1, '2020-04-05 09:15:43', '2020-04-05 09:15:43'),
(36, 35, 1, '2020-04-05 09:15:43', '2020-04-05 09:15:43'),
(37, 36, 1, '2020-04-05 09:15:43', '2020-04-05 09:15:43'),
(38, 37, 1, '2020-04-05 09:15:43', '2020-04-05 09:15:43'),
(39, 38, 1, '2020-04-05 09:15:43', '2020-04-05 09:15:43'),
(40, 39, 1, '2020-04-05 09:15:43', '2020-04-05 09:15:43'),
(41, 40, 1, '2020-04-05 09:15:43', '2020-04-05 09:15:43'),
(42, 41, 1, '2020-04-05 09:15:43', '2020-04-05 09:15:43'),
(43, 42, 1, '2020-04-05 09:15:43', '2020-04-05 09:15:43'),
(44, 43, 1, '2020-04-05 09:15:43', '2020-04-05 09:15:43'),
(45, 44, 1, '2020-04-05 09:15:43', '2020-04-05 09:15:43'),
(46, 45, 1, '2020-04-05 09:15:43', '2020-04-05 09:15:43'),
(47, 46, 1, '2020-04-05 09:15:43', '2020-04-05 09:15:43'),
(48, 47, 1, '2020-04-05 09:15:43', '2020-04-05 09:15:43'),
(49, 48, 1, '2020-04-05 09:15:43', '2020-04-05 09:15:43'),
(50, 49, 1, '2020-04-05 09:15:43', '2020-04-05 09:15:43'),
(51, 50, 1, '2020-04-05 09:15:43', '2020-04-05 09:15:43'),
(52, 51, 1, '2020-04-05 09:15:43', '2020-04-05 09:15:43'),
(53, 52, 1, '2020-04-05 09:15:43', '2020-04-05 09:15:43'),
(54, 53, 1, '2020-04-05 09:15:43', '2020-04-05 09:15:43'),
(55, 54, 1, '2020-04-05 09:15:43', '2020-04-05 09:15:43'),
(56, 55, 1, '2020-04-05 09:15:43', '2020-04-05 09:15:43'),
(57, 56, 1, '2020-04-05 09:15:43', '2020-04-05 09:15:43'),
(58, 57, 1, '2020-04-05 09:15:43', '2020-04-05 09:15:43'),
(59, 58, 1, '2020-04-05 09:15:43', '2020-04-05 09:15:43'),
(60, 59, 1, '2020-04-05 09:15:43', '2020-04-05 09:15:43'),
(61, 60, 1, '2020-04-05 09:15:43', '2020-04-05 09:15:43'),
(62, 61, 1, '2020-04-05 09:15:43', '2020-04-05 09:15:43'),
(63, 62, 1, '2020-04-05 09:15:43', '2020-04-05 09:15:43'),
(64, 63, 1, '2020-04-05 09:15:43', '2020-04-05 09:15:43'),
(65, 64, 1, '2020-04-05 09:15:43', '2020-04-05 09:15:43'),
(66, 65, 1, '2020-04-05 09:15:43', '2020-04-05 09:15:43'),
(67, 66, 1, '2020-04-05 09:15:43', '2020-04-05 09:15:43'),
(68, 67, 1, '2020-04-05 09:15:43', '2020-04-05 09:15:43'),
(69, 68, 1, '2020-04-05 09:15:43', '2020-04-05 09:15:43'),
(70, 69, 1, '2020-04-05 09:15:43', '2020-04-05 09:15:43'),
(71, 70, 1, '2020-04-05 09:15:43', '2020-04-05 09:15:43'),
(72, 71, 1, '2020-04-05 09:15:43', '2020-04-05 09:15:43'),
(73, 72, 1, '2020-04-05 09:15:43', '2020-04-05 09:15:43'),
(74, 73, 1, '2020-04-05 09:15:43', '2020-04-05 09:15:43'),
(75, 74, 1, '2020-04-05 09:15:43', '2020-04-05 09:15:43'),
(76, 75, 1, '2020-04-05 09:15:43', '2020-04-05 09:15:43'),
(77, 76, 1, '2020-04-05 09:15:43', '2020-04-05 09:15:43'),
(78, 77, 1, '2020-04-05 09:15:43', '2020-04-05 09:15:43'),
(79, 78, 1, '2020-04-05 09:15:43', '2020-04-05 09:15:43'),
(80, 79, 1, '2020-04-05 09:15:43', '2020-04-05 09:15:43'),
(81, 80, 1, NULL, NULL),
(82, 81, 1, NULL, NULL),
(83, 82, 1, NULL, NULL),
(84, 83, 1, NULL, NULL),
(85, 84, 1, NULL, NULL),
(86, 85, 1, NULL, NULL),
(87, 86, 1, NULL, NULL),
(88, 87, 1, NULL, NULL),
(89, 88, 1, NULL, NULL),
(90, 89, 1, NULL, NULL),
(91, 7, 3, '2020-04-05 10:35:42', '2020-04-05 10:35:42'),
(92, 8, 3, '2020-04-05 10:35:42', '2020-04-05 10:35:42'),
(93, 10, 3, '2020-04-05 10:35:42', '2020-04-05 10:35:42'),
(94, 11, 3, '2020-04-05 10:35:42', '2020-04-05 10:35:42'),
(95, 12, 3, '2020-04-05 10:35:42', '2020-04-05 10:35:42'),
(96, 13, 3, '2020-04-05 10:35:42', '2020-04-05 10:35:42'),
(97, 14, 3, '2020-04-05 10:35:42', '2020-04-05 10:35:42'),
(98, 15, 3, '2020-04-05 10:35:42', '2020-04-05 10:35:42'),
(99, 16, 3, '2020-04-05 10:35:42', '2020-04-05 10:35:42'),
(100, 17, 3, '2020-04-05 10:35:42', '2020-04-05 10:35:42'),
(101, 18, 3, '2020-04-05 10:35:42', '2020-04-05 10:35:42'),
(102, 19, 3, '2020-04-05 10:35:42', '2020-04-05 10:35:42'),
(103, 20, 3, '2020-04-05 10:35:42', '2020-04-05 10:35:42'),
(104, 21, 3, '2020-04-05 10:35:42', '2020-04-05 10:35:42'),
(105, 22, 3, '2020-04-05 10:35:42', '2020-04-05 10:35:42'),
(106, 23, 3, '2020-04-05 10:35:42', '2020-04-05 10:35:42'),
(107, 24, 3, '2020-04-05 10:35:42', '2020-04-05 10:35:42'),
(108, 25, 3, '2020-04-05 10:35:42', '2020-04-05 10:35:42'),
(109, 26, 3, '2020-04-05 10:35:42', '2020-04-05 10:35:42'),
(110, 27, 3, '2020-04-05 10:35:42', '2020-04-05 10:35:42'),
(111, 28, 3, '2020-04-05 10:35:42', '2020-04-05 10:35:42'),
(112, 29, 3, '2020-04-05 10:35:42', '2020-04-05 10:35:42'),
(113, 30, 3, '2020-04-05 10:35:42', '2020-04-05 10:35:42'),
(114, 31, 3, '2020-04-05 10:35:42', '2020-04-05 10:35:42'),
(115, 32, 3, '2020-04-05 10:35:42', '2020-04-05 10:35:42'),
(116, 33, 3, '2020-04-05 10:35:42', '2020-04-05 10:35:42'),
(117, 34, 3, '2020-04-05 10:35:42', '2020-04-05 10:35:42'),
(118, 35, 3, '2020-04-05 10:35:42', '2020-04-05 10:35:42'),
(119, 36, 3, '2020-04-05 10:35:42', '2020-04-05 10:35:42'),
(120, 37, 3, '2020-04-05 10:35:42', '2020-04-05 10:35:42');

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `details` varchar(5000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_11_23_075041_create_brands_table', 1),
(4, '2018_11_23_191629_create_categories_table', 1),
(5, '2018_11_23_191832_create_sub_categories_table', 1),
(6, '2018_11_23_191921_foreign_subcategories_categories', 1),
(7, '2018_11_25_222540_create_sizes_table', 1),
(8, '2018_11_26_164447_create_products_table', 1),
(9, '2018_11_26_164714_create_product_sizes_table', 1),
(10, '2018_11_26_165416_foreign_productsize', 1),
(11, '2018_12_07_131730_create_services_table', 1),
(12, '2018_12_07_141421_create_abouts_table', 1),
(13, '2018_12_28_043253_create_image_products_table', 1),
(14, '2018_12_28_043429_foreign_image_products', 1),
(15, '2019_01_21_224154_create_messages_table', 1),
(16, '2019_04_01_060842_create_socials_table', 1),
(17, '2019_05_24_190748_create_sliders_table', 1),
(18, '2020_01_09_191545_create_titles_table', 1),
(19, '2020_01_17_064956_create_faqs_table', 1),
(20, '2020_01_17_070724_create_companies_table', 1),
(21, '2020_01_17_081145_seed_companies_table_companies', 1),
(22, '2020_01_17_170236_about_at_companies', 1),
(23, '2020_03_19_093102_create_subcat_children_table', 1),
(24, '2020_03_19_095737_foreign_subcat_subcat_child', 1),
(25, '2020_03_19_100412_foreign_category_subcat_child', 1),
(32, '2020_03_20_163654_create_offers_table', 2),
(33, '2020_03_21_093111_create_offer_products_table', 2),
(34, '2020_03_21_094000_foreign_offer_product', 2),
(35, '2020_03_24_095432_create_coupons_table', 3),
(36, '2020_04_04_110528_create_roles_table', 3),
(40, '2020_04_04_211544_create_orders_table', 4),
(41, '2020_04_04_213322_foreign_user_id_at_table_orders', 5),
(43, '2020_04_04_213936_create_order_products_table', 6),
(44, '2020_04_04_214317_foreign_order_id_product_id_at_table_order_products', 6),
(45, '2020_04_04_214817_create_order_ships_table', 7),
(46, '2020_04_04_215027_foreign_order_id_user_id_at_table_order_ships', 7),
(47, '2020_04_05_145751_create_user_roles_table', 8),
(48, '2020_04_05_145759_create_menus_table', 8),
(49, '2020_04_05_145804_create_menu_roles_table', 8),
(50, '2020_04_05_150006_foreign_user_role', 8),
(51, '2020_04_05_150131_foreign_menu_role', 8),
(52, '2020_04_05_151318_seed_roles_and_users', 9);

-- --------------------------------------------------------

--
-- Table structure for table `offers`
--

CREATE TABLE `offers` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `offer` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` int(11) NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `offers`
--

INSERT INTO `offers` (`id`, `name`, `offer`, `type`, `image`, `description`, `status`, `created_at`, `updated_at`) VALUES
(2, 'Summer Offer', '10', 2, '1585033639jpg', NULL, NULL, '2020-03-24 01:07:19', '2020-03-24 01:07:19');

-- --------------------------------------------------------

--
-- Table structure for table `offer_products`
--

CREATE TABLE `offer_products` (
  `id` int(10) UNSIGNED NOT NULL,
  `offer_id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `offer_price` double(8,2) NOT NULL,
  `image` varchar(199) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `start` datetime DEFAULT NULL,
  `expire` datetime DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `offer_products`
--

INSERT INTO `offer_products` (`id`, `offer_id`, `product_id`, `offer_price`, `image`, `start`, `expire`, `status`, `created_at`, `updated_at`) VALUES
(4, 2, 1, 180.00, '1585416691jpg', NULL, NULL, 1, '2020-03-28 11:31:31', '2020-03-28 11:31:31'),
(5, 2, 2, 450.00, '', NULL, NULL, 1, '2020-03-28 11:31:31', '2020-03-28 11:31:31'),
(6, 2, 3, 270.00, '1585416692jpg', NULL, NULL, 1, '2020-03-28 11:31:32', '2020-03-28 11:31:32'),
(7, 2, 1, 180.00, '1585468679jpg', '2020-03-19 09:00:00', '2020-03-10 02:00:00', 1, '2020-03-29 01:57:59', '2020-03-29 01:57:59');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `order_no` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `total` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `qty` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `discount` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `isDelivered` int(11) NOT NULL DEFAULT '1',
  `isReady` int(11) NOT NULL DEFAULT '0',
  `isCancel` int(11) NOT NULL DEFAULT '0',
  `delivered_date` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ready_date` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cancel_date` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `order_no`, `user_id`, `total`, `qty`, `discount`, `isDelivered`, `isReady`, `isCancel`, `delivered_date`, `ready_date`, `cancel_date`, `created_at`, `updated_at`) VALUES
(1, 'SHO-7ab8bf44afc07190b5b373641b', 1, '1000', '3', NULL, 1, 0, 0, NULL, NULL, NULL, '2020-04-04 16:15:59', '2020-04-04 16:15:59'),
(2, 'SHO-40d058ee15c34cea5d8ad5d739', 1, '1000', '3', NULL, 1, 0, 0, NULL, NULL, NULL, '2020-04-04 16:17:13', '2020-04-04 16:17:13'),
(3, 'SHO-ae8a6b880dd9e9e165bf2b8ea0', 1, '2200', '9', NULL, 1, 0, 0, NULL, NULL, NULL, '2020-04-04 16:21:12', '2020-04-04 16:21:12');

-- --------------------------------------------------------

--
-- Table structure for table `order_products`
--

CREATE TABLE `order_products` (
  `id` int(10) UNSIGNED NOT NULL,
  `order_id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `qty` int(11) NOT NULL,
  `price` double(8,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `order_products`
--

INSERT INTO `order_products` (`id`, `order_id`, `product_id`, `qty`, `price`, `created_at`, `updated_at`) VALUES
(1, 3, 1, 3, 200.00, '2020-04-04 16:21:12', '2020-04-04 16:21:12'),
(2, 3, 2, 2, 500.00, '2020-04-04 16:21:12', '2020-04-04 16:21:12'),
(3, 3, 3, 2, 300.00, '2020-04-04 16:21:12', '2020-04-04 16:21:12');

-- --------------------------------------------------------

--
-- Table structure for table `order_ships`
--

CREATE TABLE `order_ships` (
  `id` int(10) UNSIGNED NOT NULL,
  `order_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `mobile` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `district` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `thana` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `order_ships`
--

INSERT INTO `order_ships` (`id`, `order_id`, `user_id`, `mobile`, `district`, `thana`, `address`, `created_at`, `updated_at`) VALUES
(1, 3, 1, '12345678912', 'Chittagong', 'Chadgaon', 'Repellendus Tempori', '2020-04-04 16:21:12', '2020-04-04 16:21:12');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(5000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category_id` int(10) UNSIGNED NOT NULL,
  `subcategory_id` int(10) UNSIGNED DEFAULT NULL,
  `subcat_children_id` int(10) UNSIGNED DEFAULT NULL,
  `brand_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `code`, `name`, `price`, `image`, `description`, `category_id`, `subcategory_id`, `subcat_children_id`, `brand_id`, `created_at`, `updated_at`) VALUES
(1, 'fgsdf', 'sfdgfd', '200', NULL, '<p>vvbvc<br></p>', 1, 2, NULL, NULL, '2020-03-20 08:52:49', '2020-03-20 08:52:49'),
(2, 'kjlk;sd', 'jaklsdklkad', '500', NULL, NULL, 1, 2, NULL, NULL, '2020-03-22 02:32:59', '2020-03-22 02:32:59'),
(3, NULL, 'asldjf;lkd.s,', '300', NULL, NULL, 1, 2, NULL, NULL, '2020-03-22 02:33:32', '2020-03-22 02:33:32'),
(4, 'P-7', 'Diaper', '110', NULL, '<p>jkasldk;fj;lsdakfjdask.fads&nbsp; adsf&nbsp; adsf<br></p>', 2, NULL, NULL, NULL, '2020-03-31 02:22:47', '2020-03-31 02:22:47');

-- --------------------------------------------------------

--
-- Table structure for table `product_size`
--

CREATE TABLE `product_size` (
  `id` int(10) UNSIGNED NOT NULL,
  `size_id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Admin', '2020-04-05 09:15:36', '2020-04-05 09:15:36'),
(2, 'User', '2020-04-05 09:15:36', '2020-04-05 09:15:36'),
(3, 'customer2', '2020-04-05 10:35:42', '2020-04-05 10:35:42');

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sizes`
--

CREATE TABLE `sizes` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sliders`
--

CREATE TABLE `sliders` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sliders`
--

INSERT INTO `sliders` (`id`, `title`, `description`, `image`, `created_at`, `updated_at`) VALUES
(3, 'Slider', NULL, '2020_faffc.jpg', '2020-03-24 07:45:30', '2020-03-24 07:45:30');

-- --------------------------------------------------------

--
-- Table structure for table `socials`
--

CREATE TABLE `socials` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` text COLLATE utf8mb4_unicode_ci,
  `type` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `subcategories`
--

CREATE TABLE `subcategories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `advertise_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `subcategories`
--

INSERT INTO `subcategories` (`id`, `name`, `category_id`, `image`, `description`, `advertise_image`, `created_at`, `updated_at`) VALUES
(2, 'sub cat1', 1, '2020_179936.jpg', 'kl;admsklfm', '2020_179936.jpg', '2020-03-20 07:47:25', '2020-03-20 08:05:09');

-- --------------------------------------------------------

--
-- Table structure for table `subcat_children`
--

CREATE TABLE `subcat_children` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL,
  `subcategory_id` int(10) UNSIGNED NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `advertise_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `titles`
--

CREATE TABLE `titles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `page` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `usertype` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'user',
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `username`, `usertype`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'HD Express', 'hdexpr', 'admin', 'admin@gmail.com', '$2y$12$V5Zksg50RGox8VvJMEYthunYrhdl5MtM0NqE1m/zCbYbDowEaW2ue', 'xFoIkcNsEXUsS9sUisLSYsGOr9nT5X8nPu6yl2KjpYSt6PE9EfOhH1XtJmtL', '2020-03-20 07:42:06', '2020-03-20 07:42:06'),
(3, 'Ripon Uddin', '', 'user', 'rislam252@gmail.com', '$2y$10$AOoqDbhDjfqNuoPf5ayMeuOaCk5BJYvt3O9G96BA3Y2pbjTo.Mozi', 'DBCfRz44tiAn2uqW6ORpON5pPQL7557waqQjqvIDAblZ3yyvOasvGQIzVPqp', '2020-04-05 10:41:19', '2020-04-05 10:41:19'),
(6, 'Arman', NULL, 'user', 'arman@gmail.com', '$2y$10$kGZO7esUhf7YCuUhDPiO3u/X9uBKLBO6cKSsSf122b9UKmXcsfsS6', NULL, '2020-04-05 11:05:47', '2020-04-05 11:05:47');

-- --------------------------------------------------------

--
-- Table structure for table `user_roles`
--

CREATE TABLE `user_roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_roles`
--

INSERT INTO `user_roles` (`id`, `user_id`, `role_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '2020-04-05 09:15:36', '2020-04-05 09:15:36'),
(3, 3, 1, NULL, NULL),
(4, 6, 1, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `abouts`
--
ALTER TABLE `abouts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `brands`
--
ALTER TABLE `brands`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `companies`
--
ALTER TABLE `companies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `coupons`
--
ALTER TABLE `coupons`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `coupons_name_unique` (`name`),
  ADD KEY `coupons_user_id_foreign` (`user_id`);

--
-- Indexes for table `faqs`
--
ALTER TABLE `faqs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `image_products`
--
ALTER TABLE `image_products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `image_products_product_id_foreign` (`product_id`);

--
-- Indexes for table `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu_roles`
--
ALTER TABLE `menu_roles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `menu_roles_menu_id_foreign` (`menu_id`),
  ADD KEY `menu_roles_role_id_foreign` (`role_id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `offers`
--
ALTER TABLE `offers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `offer_products`
--
ALTER TABLE `offer_products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `offer_products_offer_id_foreign` (`offer_id`),
  ADD KEY `offer_products_product_id_foreign` (`product_id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `orders_user_id_foreign` (`user_id`);

--
-- Indexes for table `order_products`
--
ALTER TABLE `order_products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_products_order_id_foreign` (`order_id`),
  ADD KEY `order_products_product_id_foreign` (`product_id`);

--
-- Indexes for table `order_ships`
--
ALTER TABLE `order_ships`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_ships_order_id_foreign` (`order_id`),
  ADD KEY `order_ships_user_id_foreign` (`user_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `products_category_id_foreign` (`category_id`),
  ADD KEY `products_subcategory_id_foreign` (`subcategory_id`),
  ADD KEY `products_brand_id_foreign` (`brand_id`);

--
-- Indexes for table `product_size`
--
ALTER TABLE `product_size`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_size_product_id_foreign` (`product_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sizes`
--
ALTER TABLE `sizes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sliders`
--
ALTER TABLE `sliders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `socials`
--
ALTER TABLE `socials`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subcategories`
--
ALTER TABLE `subcategories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `subcategories_category_id_foreign` (`category_id`);

--
-- Indexes for table `subcat_children`
--
ALTER TABLE `subcat_children`
  ADD PRIMARY KEY (`id`),
  ADD KEY `subcat_children_subcategory_id_foreign` (`subcategory_id`),
  ADD KEY `subcat_children_category_id_foreign` (`category_id`);

--
-- Indexes for table `titles`
--
ALTER TABLE `titles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD UNIQUE KEY `users_username_unique` (`username`);

--
-- Indexes for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_roles_user_id_foreign` (`user_id`),
  ADD KEY `user_roles_role_id_foreign` (`role_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `abouts`
--
ALTER TABLE `abouts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `brands`
--
ALTER TABLE `brands`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `companies`
--
ALTER TABLE `companies`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `coupons`
--
ALTER TABLE `coupons`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `faqs`
--
ALTER TABLE `faqs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `image_products`
--
ALTER TABLE `image_products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=90;
--
-- AUTO_INCREMENT for table `menu_roles`
--
ALTER TABLE `menu_roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=121;
--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;
--
-- AUTO_INCREMENT for table `offers`
--
ALTER TABLE `offers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `offer_products`
--
ALTER TABLE `offer_products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `order_products`
--
ALTER TABLE `order_products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `order_ships`
--
ALTER TABLE `order_ships`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `product_size`
--
ALTER TABLE `product_size`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sizes`
--
ALTER TABLE `sizes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sliders`
--
ALTER TABLE `sliders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `socials`
--
ALTER TABLE `socials`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `subcategories`
--
ALTER TABLE `subcategories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `subcat_children`
--
ALTER TABLE `subcat_children`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `titles`
--
ALTER TABLE `titles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `user_roles`
--
ALTER TABLE `user_roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `coupons`
--
ALTER TABLE `coupons`
  ADD CONSTRAINT `coupons_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `image_products`
--
ALTER TABLE `image_products`
  ADD CONSTRAINT `image_products_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`);

--
-- Constraints for table `menu_roles`
--
ALTER TABLE `menu_roles`
  ADD CONSTRAINT `menu_roles_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`),
  ADD CONSTRAINT `menu_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);

--
-- Constraints for table `offer_products`
--
ALTER TABLE `offer_products`
  ADD CONSTRAINT `offer_products_offer_id_foreign` FOREIGN KEY (`offer_id`) REFERENCES `offers` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `offer_products_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `order_products`
--
ALTER TABLE `order_products`
  ADD CONSTRAINT `order_products_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`),
  ADD CONSTRAINT `order_products_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`);

--
-- Constraints for table `order_ships`
--
ALTER TABLE `order_ships`
  ADD CONSTRAINT `order_ships_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`),
  ADD CONSTRAINT `order_ships_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_brand_id_foreign` FOREIGN KEY (`brand_id`) REFERENCES `brands` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `products_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `products_subcategory_id_foreign` FOREIGN KEY (`subcategory_id`) REFERENCES `subcategories` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `product_size`
--
ALTER TABLE `product_size`
  ADD CONSTRAINT `product_size_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`);

--
-- Constraints for table `subcategories`
--
ALTER TABLE `subcategories`
  ADD CONSTRAINT `subcategories_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`);

--
-- Constraints for table `subcat_children`
--
ALTER TABLE `subcat_children`
  ADD CONSTRAINT `subcat_children_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `subcat_children_subcategory_id_foreign` FOREIGN KEY (`subcategory_id`) REFERENCES `subcategories` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD CONSTRAINT `user_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`),
  ADD CONSTRAINT `user_roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
